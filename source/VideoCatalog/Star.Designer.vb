﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Star
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StartPic = New System.Windows.Forms.PictureBox()
        Me.StarRatingMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StarRatingMenuItemClear = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.StartPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StarRatingMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'StartPic
        '
        Me.StartPic.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.StartPic.ContextMenuStrip = Me.StarRatingMenu
        Me.StartPic.Dock = System.Windows.Forms.DockStyle.Fill
        Me.StartPic.Location = New System.Drawing.Point(0, 0)
        Me.StartPic.Name = "StartPic"
        Me.StartPic.Size = New System.Drawing.Size(80, 16)
        Me.StartPic.TabIndex = 0
        Me.StartPic.TabStop = False
        '
        'StarRatingMenu
        '
        Me.StarRatingMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StarRatingMenuItemClear})
        Me.StarRatingMenu.Name = "StarRatingMenu"
        Me.StarRatingMenu.ShowImageMargin = False
        Me.StarRatingMenu.Size = New System.Drawing.Size(111, 26)
        '
        'StarRatingMenuItemClear
        '
        Me.StarRatingMenuItemClear.Name = "StarRatingMenuItemClear"
        Me.StarRatingMenuItemClear.Size = New System.Drawing.Size(110, 22)
        Me.StarRatingMenuItemClear.Text = "Clear rating"
        '
        'Star
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.Controls.Add(Me.StartPic)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.MaximumSize = New System.Drawing.Size(100, 20)
        Me.Name = "Star"
        Me.Size = New System.Drawing.Size(80, 16)
        CType(Me.StartPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StarRatingMenu.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents StartPic As PictureBox
    Friend WithEvents StarRatingMenu As ContextMenuStrip
    Friend WithEvents StarRatingMenuItemClear As ToolStripMenuItem
End Class
