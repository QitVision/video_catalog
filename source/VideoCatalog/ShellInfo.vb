﻿Friend Class ShellInfo
    Public Property Name As String
    Public Property Value As String

    Public Sub New(_name As String, _value As String)
        Name = _name
        Value = _value
    End Sub

    Public Overrides Function ToString() As String
        Return Name

    End Function
End Class
