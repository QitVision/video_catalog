﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

''' <summary>
''' This class handles the sorting of search result video lists.
''' </summary>
Public Class VideoSorter

    ''' <summary>
    ''' Sort by rating
    ''' </summary>
    ''' <param name="result_list">initial list of video indexes to be sorted</param>
    ''' <param name="videoList">copy of the list of video files</param>
    ''' <returns></returns>
    Public Function rating(ByVal result_list As List(Of Integer), ByVal videoList As List(Of VideoFile)) As IOrderedEnumerable(Of Integer)
        Return result_list.OrderBy(Function(x) videoList(x).getRating())
    End Function

    ''' <summary>
    ''' Sort by video length
    ''' </summary>
    ''' <param name="result_list">initial list of video indexes to be sorted</param>
    ''' <param name="videoList">copy of the list of video files</param>
    ''' <returns></returns>
    Public Function length(ByVal result_list As List(Of Integer), ByVal videoList As List(Of VideoFile)) As IOrderedEnumerable(Of Integer)
        Return result_list.OrderBy(Function(x) videoList(x).getLength())
    End Function

    ''' <summary>
    ''' Sort by date created.
    ''' </summary>
    ''' <param name="result_list">initial list of video indexes to be sorted</param>
    ''' <param name="videoList">copy of the list of video files</param>
    ''' <returns></returns>
    Public Function date_created(ByVal result_list As List(Of Integer), ByVal videoList As List(Of VideoFile)) As IOrderedEnumerable(Of Integer)
        Return result_list.OrderBy(Function(x) videoList(x).getCreatedLong())
    End Function

    ''' <summary>
    ''' Sort randomly
    ''' </summary>
    ''' <param name="result_list">initial list of video indexes to be sorted</param>
    ''' <param name="videoList">copy of the list of video files</param>
    ''' <returns></returns>
    Public Function random(ByVal result_list As List(Of Integer), ByVal videoList As List(Of VideoFile)) As IOrderedEnumerable(Of Integer)
        Dim seed As Long = DateTime.Now.Millisecond + Cursor.Position.X + Cursor.Position.Y
        Dim Generator As System.Random = New System.Random(seed)
        Return result_list.OrderBy(Function(x) Generator.Next)
    End Function

End Class
