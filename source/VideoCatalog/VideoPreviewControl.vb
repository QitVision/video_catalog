﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.IO

Public Class VideoPreviewControl

    Private vid As VideoFile                    'videoFile that is loaded to this control
    Private previewDir As String                'preview directory path that is passed straight from the Form1
    Private thisPreviewDir As String
    Private PicSize As Integer = 1              'Default = 1, 0 = small (160x95), 1 = regular (216x126), 2 = large (320x190), 3 = very large (576x342)
    Private ShowRating As Boolean = False       'star rating control
    Private starRating As Star = Nothing        'holder for the control
    Private showtagMenu As Boolean = True
    Private TagMenuPic As PictureBox            'holder for tag menu
    Private _TagMenu As TagMenu                 'holder for tagMenu
    Private previewChangeSpeed As Integer = 1   '0 = slow (1100 ms), 1 = default/medium (700 ms), 2 = fast (500 ms), 3 = very fast (350 ms)
    Private autoCompleteTags As AutoCompleteStringCollection

    'video previews
    Private preview_file_paths As New List(Of String)
    Private preview_images As New List(Of Image)
    Private default_image As Image
    Private carousel_index As Integer = 0
    Private page_changing As Boolean = False
    Private Inactive_brush As Brush = New SolidBrush(Color.FromArgb(150, Color.FromArgb(80, 80, 85)))
    Private text_background_brush As Brush = New SolidBrush(Color.FromArgb(220, Color.FromArgb(80, 80, 85)))
    Private lengthFont As Font = New Font("Arial", 12)
    Private carousel_is_default As Boolean = True
    Private preview_frame As RectangleF = New Rectangle(0, 0, 0, 0) 'This is the area on the picturebox that has content
    Private preview_box_aspect As Single = 1.0

    'tag menu
    Private tag_rectangle As Rectangle
    Private hoverOverTag As Boolean = False

    'other info
    Private lengthStr As String = ""

    'Initialize with a reference of the videoFile and pass the appData directory that holds the previews
    Public Sub New(ByRef _vid As VideoFile, ByVal _PreviewFileDirectory As String, ByVal _PicSize As Integer, ByVal _ShowRating As Boolean, ByVal _previewChangeSpeed As Integer, ByVal _showTagMenu As Boolean, ByRef _autoCompleteTags As AutoCompleteStringCollection)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        vid = _vid
        previewDir = _PreviewFileDirectory
        PicSize = _PicSize
        ShowRating = _ShowRating
        previewChangeSpeed = _previewChangeSpeed
        showtagMenu = _showTagMenu
        autoCompleteTags = _autoCompleteTags
    End Sub

    Private Sub VideoPreviewControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        're-set sizes
        Select Case PicSize
            Case 0  'SMALL
                PreviewVideo.Size = New Size(160, 95)
                Name_lbl.MaximumSize = New Size(155, 40)
            Case 1  'MEDIUM0
                'default
            Case 2  'LARGE
                PreviewVideo.Size = New Size(320, 190)
                Name_lbl.MaximumSize = New Size(310, 40)
            Case 3  'VERY LARGE
                PreviewVideo.Size = New Size(576, 342) 'New Size(640, 380)
                Name_lbl.MaximumSize = New Size(566, 40) 'New Size(630, 40)
        End Select

        preview_box_aspect = PreviewVideo.Width / PreviewVideo.Height

        If (ShowRating) Then
            starRating = New Star()
            FlowLayoutPanel1.Controls.Add(starRating)
            FlowLayoutPanel1.Controls.SetChildIndex(starRating, 1)
        End If

        If (showtagMenu) Then

            'set tag rect area
            'Tag rect is part of the previewVideo picture. Its width and height is 24
            Dim tag_offset As Integer = 4
            tag_rectangle = New Rectangle(PreviewVideo.Width - 20 - tag_offset, PreviewVideo.Height - 20 - 20 - tag_offset, 20, 20)

        End If

        'set preview change speed
        Select Case previewChangeSpeed
            Case 0
                Timer1.Interval = 1100
            Case 1
                Timer1.Interval = 700
            Case 2
                Timer1.Interval = 500
            Case 3
                Timer1.Interval = 350
        End Select

        'if not emty, load
        If (vid IsNot Nothing) Then
            LoadContent(vid, previewDir)
        Else
            'empty
            emptyContent()
        End If

    End Sub

    Public Sub setPageChange()
        page_changing = True
        If (starRating IsNot Nothing) Then
            starRating.setRating(Nothing)
        End If
        PreviewVideo.Invalidate()
    End Sub

    Public Sub emptyContent()
        page_changing = False
        vid = Nothing
        PreviewVideo.Image = Nothing
        'load name
        Name_lbl.Text = ""
        preview_file_paths.Clear()
        preview_images.Clear()
        default_image = Nothing
        ToolTip1.SetToolTip(Name_lbl, "")
        lengthStr = ""
        If (starRating IsNot Nothing) Then
            starRating.setRating(Nothing)
        End If
    End Sub

    Public Sub LoadContent(ByRef _vid As VideoFile, ByVal _PreviewFileDirectory As String)
        page_changing = False
        vid = _vid
        previewDir = _PreviewFileDirectory
        preview_file_paths.Clear()
        preview_images.Clear()

        thisPreviewDir = _PreviewFileDirectory + vid.getId() + "\"
        'load all preview file paths to list
        If (Directory.Exists(thisPreviewDir)) Then
            For Each file In IO.Directory.GetFiles(thisPreviewDir, "*.jpg*", SearchOption.TopDirectoryOnly)
                preview_file_paths.Add(file)
            Next
        End If

        'load only default image
        If (preview_file_paths.Count > 0) Then
            Dim relevantImageIndex As Integer = vid.getPreviewIndex
            If ((relevantImageIndex = -1) Or relevantImageIndex >= preview_file_paths.Count) Then
                relevantImageIndex = preview_file_paths.Count / 2
            End If

            Try
                default_image = SafeImageFromFile(preview_file_paths(relevantImageIndex))
            Catch ex As Exception
                default_image = My.Resources.no_preview
            End Try

            PreviewVideo.Image = default_image

                'Calculate content area
                Dim imgAspect As Single = default_image.Width / default_image.Height
                If (imgAspect < preview_box_aspect) Then
                    'height is the same
                    preview_frame.Height = PreviewVideo.Height - 2
                    preview_frame.Width = PreviewVideo.Height * imgAspect
                    preview_frame.Y = 1
                    preview_frame.X = ((PreviewVideo.Width - (preview_frame.Width)) / 2)
                Else
                    'width is the same
                    preview_frame.Width = PreviewVideo.Width - 2
                    preview_frame.Height = PreviewVideo.Width / imgAspect
                    preview_frame.X = 1
                    preview_frame.Y = ((PreviewVideo.Height - (preview_frame.Height)) / 2)
                End If
            Else
                default_image = My.Resources.no_preview
            'default_image = Nothing
            PreviewVideo.Image = default_image

            'preview_frame.Height = PreviewVideo.Height - 2
            'preview_frame.Width = PreviewVideo.Width - 2
            'preview_frame.Y = 1
            'preview_frame.X = 1

            'Calculate content area
            Dim imgAspect As Single = default_image.Width / default_image.Height
            If (imgAspect < preview_box_aspect) Then
                'height is the same
                preview_frame.Height = PreviewVideo.Height - 2
                preview_frame.Width = PreviewVideo.Height * imgAspect
                preview_frame.Y = 1
                preview_frame.X = ((PreviewVideo.Width - (preview_frame.Width)) / 2)
            Else
                'width is the same
                preview_frame.Width = PreviewVideo.Width - 2
                preview_frame.Height = PreviewVideo.Width / imgAspect
                preview_frame.X = 1
                preview_frame.Y = ((PreviewVideo.Height - (preview_frame.Height)) / 2)
            End If

        End If

        'calculate tag position
        Dim tag_offset As Integer = 4
        tag_rectangle = New Rectangle(preview_frame.X + preview_frame.Width - 20 - tag_offset, preview_frame.Y + preview_frame.Height - 20 - 20 - tag_offset, 20, 20)

        'load name
        Name_lbl.Text = vid.getName
        lengthStr = vid.getLengthStr

        'load tooltips
        ToolTip1.SetToolTip(Name_lbl, vid.getPath)

        'load rating
        If (starRating IsNot Nothing) Then
            starRating.setRating(vid)
        End If

    End Sub

    Private Sub PreviewVideo_MouseEnter(sender As Object, e As EventArgs) Handles PreviewVideo.MouseEnter
        If (preview_file_paths.Count > 1) Then
            carousel_index = preview_file_paths.Count - 1   'this makes the carousel start at zero
            Timer1.Start()
            'launch first event immediately
            carousel_image(-1)
        End If
    End Sub

    Private Sub PreviewVideo_MouseLeave(sender As Object, e As EventArgs) Handles PreviewVideo.MouseLeave
        Timer1.Stop()
        carousel_is_default = True
        PreviewVideo.Image = default_image
        'hide tag menu if mouse leaves
        If (hoverOverTag) Then toggleTagMenuVisibility(False)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        carousel_image(-1)
    End Sub

    Private Sub carousel_image(ByVal index As Integer)
        carousel_is_default = False
        If (preview_file_paths.Count = 0) Then Return
        If (preview_images.Count = 0) Then loadAllPreviews()
        If (index > preview_images.Count - 1) Then Return
        If (index = -1) Then
            carousel_index += 1
            If (carousel_index > preview_images.Count - 1) Then carousel_index = 0
            PreviewVideo.Image = preview_images(carousel_index)
        Else
            PreviewVideo.Image = preview_images(index)
        End If
    End Sub

    Private Sub loadAllPreviews()
        For Each _file In preview_file_paths
            Try
                preview_images.Add(SafeImageFromFile(_file))
            Catch ex As Exception
                'unable to load preview
            End Try
        Next
    End Sub

    Private Function SafeImageFromFile(path As String) As Image
        Using fs As New FileStream(path, FileMode.Open, FileAccess.Read)
            Dim img = Image.FromStream(fs)
            Return img
        End Using
    End Function

#Region "Video ContextMenu"

    Private Sub PlayToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PlayToolStripMenuItem.Click
        playVideo()
    End Sub

    Private Sub playVideo()
        'Play the video
        If (vid IsNot Nothing) Then
            If (File.Exists(vid.getPath)) Then
                Process.Start(vid.getPath)
            End If
        End If
    End Sub

    Private Sub PreviewVideo_DoubleClick(sender As Object, e As EventArgs) Handles PreviewVideo.DoubleClick
        playVideo()
    End Sub

    Private Sub ViewInFolderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewInFolderToolStripMenuItem.Click
        If (vid IsNot Nothing) Then
            If (File.Exists(vid.getPath)) Then
                Process.Start("explorer.exe", "/select,""" + vid.getPath + """")
            End If
        End If
    End Sub


#End Region

    'Paint event on the preview
    Private Sub PreviewVideo_Paint(sender As Object, e As PaintEventArgs) Handles PreviewVideo.Paint
        Try
            Dim g As Graphics = e.Graphics
            'draw wait frame
            If (vid IsNot Nothing) Then
                g.DrawRectangle(New Pen(Brushes.Black, 2), preview_frame.X, preview_frame.Y, preview_frame.Width, preview_frame.Height)
                If (page_changing) Then
                    g.FillRectangle(Inactive_brush, New RectangleF(preview_frame.X, preview_frame.Y, preview_frame.Width, preview_frame.Height))
                Else
                    If ((preview_images.Count > 0) And Not carousel_is_default) Then
                        Dim singleW As Integer = (preview_frame.Width - 5) / (preview_images.Count - 1)
                        Dim currentW = preview_frame.X + 1 + carousel_index * singleW + 5
                        g.DrawLine(New Pen(Brushes.IndianRed, 3), preview_frame.X + 1, preview_frame.Y + 2, currentW, preview_frame.Y + 2)
                    End If
                    Dim strLength As SizeF = g.MeasureString(lengthStr, lengthFont)
                    'draw box
                    g.FillRectangle(text_background_brush, New Rectangle(preview_frame.X + preview_frame.Width - strLength.Width - 3, preview_frame.Y + preview_frame.Height - strLength.Height - 3, strLength.Width + 2, strLength.Height + 2))
                    'draw length text
                    g.DrawString(lengthStr, lengthFont, Brushes.WhiteSmoke, preview_frame.X + preview_frame.Width - strLength.Width - 1, preview_frame.Y + preview_frame.Height - strLength.Height - 1)
                    'draw tag_menu image if tag menu is set visible
                    If (showtagMenu) Then
                        g.DrawImage(My.Resources.tag_small_bw, tag_rectangle.X, tag_rectangle.Y, tag_rectangle.Width, tag_rectangle.Height)
                    End If
                End If
            End If
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

#Region "Tag menu"

    Private Sub PreviewVideo_MouseMove(sender As Object, e As MouseEventArgs) Handles PreviewVideo.MouseMove
        'check if the mouse is on the tag 
        If ((e.X > tag_rectangle.X) And (e.X < tag_rectangle.X + (tag_rectangle.Width)) And (e.Y > tag_rectangle.Y) And (e.Y < tag_rectangle.Y + tag_rectangle.Height)) Then
            'mouse is over the tag
            toggleTagMenuVisibility(True)
        Else
            'mouse is not on the tag
            toggleTagMenuVisibility(False)
        End If
    End Sub

    Private Sub toggleTagMenuVisibility(ByVal mouseHoverOver As Boolean)
        If Not (showtagMenu) Then Return
        If (hoverOverTag <> mouseHoverOver) Then
            hoverOverTag = mouseHoverOver

            'state change
            If (mouseHoverOver) Then
                tagMenuTimer.Stop()
                tagMenuTimer.Start()
            Else
                'hide tag menu
                If (_TagMenu IsNot Nothing) Then
                    _TagMenu.closeTagMenu()
                    _TagMenu = Nothing
                End If
            End If

        End If
    End Sub

    Private Sub tagMenuTimer_Tick(sender As Object, e As EventArgs) Handles tagMenuTimer.Tick
        If (hoverOverTag) Then
            'mouse enters
            If (_TagMenu Is Nothing) Then
                Console.WriteLine("onMouseEnter show")
                _TagMenu = New TagMenu(vid, autoCompleteTags)
                _TagMenu.Show()
                Dim pos As Size = Cursor.Position
                pos.Width += 1
                pos.Height += 1
                _TagMenu.Location = pos
                _TagMenu.TopMost = True
            End If
        End If
        tagMenuTimer.Enabled = False 'fire once
    End Sub

#End Region

End Class
