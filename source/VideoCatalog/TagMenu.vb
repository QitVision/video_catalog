﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.Runtime.InteropServices

Public Class TagMenu

#Region "Textbox hints"
    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByVal lParam As String) As Int32
    End Function
#End Region

    Private onFocus As Boolean = False
    Private onFocusTextBox As Boolean = False
    Private writingTags As Boolean = False
    Private panelFocus As Boolean = False
    Private listFocus As Boolean = False
    Private allowedToClose As Boolean = False

    Private vid As VideoFile

    Public Sub New(ByRef _vid As VideoFile, ByRef autoCompleteTags As AutoCompleteStringCollection)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        vid = _vid

        TagBox.AutoCompleteCustomSource = autoCompleteTags
    End Sub

    Private Sub TagMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ActiveControl = Me.FlowLayoutPanel1
        'show hint at tag box
        SendMessage(Me.TagBox.Handle, &H1501, 0, "Add tags")

        'load tags to list
        Dim tagArray As String() = vid.getTags
        Dim aiTags As String() = vid.getAITags

        For Each _tag In tagArray
            tagList.Items.Add(_tag)
        Next

        ai_tags_txt.Text = "" ' "AI tags: "
        For Each _tag In aiTags
            'skip * mark that indicates that this file has been through AI tag generation
            If (_tag.Equals("*")) Then Continue For
            ai_tags_txt.Text = ai_tags_txt.Text + _tag + vbCrLf '", "
        Next

    End Sub

    Public Sub closeTagMenu()
        allowedToClose = True
        If (Not onFocus) Then
            CloseTimer.Stop()
            CloseTimer.Start()
        End If
    End Sub

    Private Sub CloseTimer_Tick(sender As Object, e As EventArgs) Handles CloseTimer.Tick
        If (Not onFocus And allowedToClose And Not onFocusTextBox And Not writingTags And Not listFocus And Not panelFocus) Then
            'check cursor position is inside the form
            'controls mouse leave and next controls mouse enter events can leave a gap and cause form closing
            If Not (Cursor.Position.X > Me.Location.X And Cursor.Position.X < (Me.Location.X + Me.Width) And Cursor.Position.Y > Me.Location.Y And Cursor.Position.Y < (Me.Location.Y + Me.Width)) Then
                'before closing, check if the tag array has stayed the same and save changes if tags are altered
                CheckNewTags()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub CheckNewTags()
        'check if the tag array has stayed the same and save changes if tags are altered
        Dim newTagList As New List(Of String)
        Dim hasChanges As Boolean = False
        Dim oldTags As String() = vid.getTags

        Dim tagCounter As Integer = 0
        For Each _tag In tagList.Items
            If (tagCounter >= oldTags.Count) Then
                hasChanges = True
            Else
                If Not (oldTags(tagCounter).Equals(_tag)) Then
                    hasChanges = True
                End If
            End If
            newTagList.Add(_tag)
            tagCounter += 1
        Next

        If (tagList.Items.Count < oldTags.Count) Then
            hasChanges = True
        End If

        If (hasChanges) Then
            vid.setTag(newTagList.ToArray())
        End If

    End Sub

#Region "Focus events"

    Private Sub TagMenu_MouseEnter(sender As Object, e As EventArgs) Handles Me.MouseEnter
        onFocus = True
        Console.WriteLine("tagMenu mouseEnter")
    End Sub

    Private Sub TagMenu_MouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
        onFocus = False
        'leaves the form
        Console.WriteLine("tagMenu mouseLeave")
        If (allowedToClose) Then CloseTimer.Stop() : CloseTimer.Start()
    End Sub

    Private Sub TagBox_MouseEnter(sender As Object, e As EventArgs) Handles TagBox.MouseEnter
        onFocusTextBox = True
        Console.WriteLine("tagBox mouseEnter")
    End Sub

    Private Sub TagBox_MouseLeave(sender As Object, e As EventArgs) Handles TagBox.MouseLeave
        onFocusTextBox = False
        Console.WriteLine("tagBox mouseLeave")
    End Sub

    Private Sub TagBox_GotFocus(sender As Object, e As EventArgs) Handles TagBox.GotFocus
        writingTags = True
        Console.WriteLine("tagBox GotFocus")
    End Sub

    Private Sub TagBox_LostFocus(sender As Object, e As EventArgs) Handles TagBox.LostFocus
        writingTags = False
        If (allowedToClose) Then CloseTimer.Stop() : CloseTimer.Start()
        Console.WriteLine("tagBox lostFocus")
    End Sub

    Private Sub FlowLayoutPanel1_MouseEnter(sender As Object, e As EventArgs) Handles FlowLayoutPanel1.MouseEnter
        panelFocus = True
        Console.WriteLine("panel mouseEnter")
    End Sub

    Private Sub FlowLayoutPanel1_MouseLeave(sender As Object, e As EventArgs) Handles FlowLayoutPanel1.MouseLeave
        panelFocus = False
        Console.WriteLine("panel mouseLeave")
    End Sub

    Private Sub tagList_MouseEnter(sender As Object, e As EventArgs) Handles tagList.MouseEnter
        listFocus = True
    End Sub

    Private Sub tagList_MouseLeave(sender As Object, e As EventArgs) Handles tagList.MouseLeave
        listFocus = False
    End Sub

#End Region

#Region "Tag input and edit"

    Private Sub TagBox_KeyDown(sender As Object, e As KeyEventArgs) Handles TagBox.KeyDown
        'add tag on enter
        If (e.KeyCode = Keys.Enter) Then
            'validate
            Dim NewTag As String = TagBox.Text
            If ((NewTag.Length > 0) And Not tagList.Items.Contains(NewTag)) Then
                tagList.Items.Add(NewTag)
                If Not (TagBox.AutoCompleteCustomSource.Contains(NewTag)) Then
                    TagBox.AutoCompleteCustomSource.Add(NewTag)
                End If
                TagBox.Text = "" 'clear
                    'set focus elsewhere
                    Me.ActiveControl = Me.FlowLayoutPanel1
                End If
            End If
    End Sub

    Private Sub tagList_KeyDown(sender As Object, e As KeyEventArgs) Handles tagList.KeyDown
        'remove tags on delete
        If (e.KeyCode = Keys.Delete And tagList.Items.Count > 0) Then
            tagList.Items.RemoveAt(tagList.SelectedIndex)
        End If
    End Sub

    Private Sub FlowLayoutPanel1_KeyDown(sender As Object, e As KeyEventArgs) Handles FlowLayoutPanel1.KeyDown
        'This panel is on focus by default and if the user is typing the tag while this is active, forward focus to tagbox
        If (e.KeyCode <> Keys.Enter And e.KeyCode <> Keys.Shift And e.KeyCode <> Keys.Alt And e.KeyCode <> Keys.Escape And e.KeyCode <> Keys.Delete) Then
            Me.ActiveControl() = Me.TagBox
        End If
    End Sub

#End Region

End Class