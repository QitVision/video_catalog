﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.IO
Imports System.Windows.Media
Imports System.ComponentModel
Imports System.Runtime.InteropServices

'PROJECT COLORS (r,g,b)
'MAIN DARK = 14,54,123

Public Class Form1

#Region "Load and declarations"

#Region "Textbox hints"
    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.LPWStr)> ByVal lParam As String) As Int32
    End Function
#End Region

    'Directories and filepaths
    Dim _ProgramFolder As String                                    'the path to the AppData folder. This is where the settings and file lists are stored
    Dim SettingsFile As String                                      'full path to the persistent settings file
    Dim _IndexFilePath As String                                    'full path to a file that contains updated list of video files and their metadata
    Dim _OldIndexFilePath As String                                 'full path to a file that contains previous list of video files and their metadata. Recovery file
    Dim _PreviewFileDirectory As String                             'full path to a directory that contains video preview images
    Dim ErrorFile As String                                         'full path to error log file. This is created on first exception and appended after that
    Dim _ModelFolder As String                                      'Inception model files. Used as a local image classifier for generating AI search tags
    Dim previewVersionFile As String = "preview_version.txt"

    Dim ROOT_DIRECTORIES As New List(Of String)                     'Video library directories. Videos are searched from these folders and their subfolders
    Dim VidList As New List(Of VideoFile)                           'IndexFile is loaded to this list at run time

    'these are almost all Windows Media Player supported video formats
    'except .mkv format is not natively supported
    Dim VALID_EXTENSIONS As String() = {".wma", ".wmv", ".asf", ".wm", ".dvr-ms", ".avi", ".mpg", ".mpeg", ".m1v", ".mp2", ".mpe", ".mov", ".mp4", ".m4v", ".m2ts", ".mkv", ".flv"}
    Dim WMP_UNSUPPORTED_EXTENSIONS As String() = {".mkv"}

    'Localized names for extended property field names
    'These are used to find correct key-value property fields when reading extended file properties
    'Field names are found by reading properties from a dummy video file with known resolution, fps and duration. This is done on first open.
    Dim FIELD_FRAME_WIDTH As String
    Dim FIELD_FRAME_HEIGHT As String
    Dim FIELD_FPS As String
    Dim FIELD_LENGTH As String

    'number of screenshots to take from each video
    Dim SCREENSHOT_COUNT As Integer = 14                             'cannot be smaller than 1
    Dim screenCaptureClass As PreviewPlayer = Nothing                're-use the same screencaptureclass when capturing screenshots

    'Keep record of files that are not found anymore, could be disconneced network drive
    Dim active_indexes As New List(Of Integer)
    'While refreshing the list, Update buffer list and after finished, copy to original list
    Dim active_indexes_buffer As New List(Of Integer)

    'Video list refresh parameters
    Dim silent_refresh As Boolean = False       'This is false if the refresh is done as user action, silent mode is used when updating on the background after opening
    Dim fix_previews As Boolean = False         'This is true if previously failed previews are being retried.
    Dim refresh_has_changes As Boolean = False  'The visible videos are updated after silent refresh if this is true
    Dim previews_need_refresh As Boolean = False    'If the file refresh is silent, previews aren't generated but the user is noted that the previews are missing

    'Video search backgroundworker
    Dim bw As BackgroundWorker = New BackgroundWorker
    Dim background_status As String = ""                            'this is displayed on the statusLabel
    'Video categorizing backgroundworker
    Dim bw_AI As BackgroundWorker = New BackgroundWorker

    'Dialog triggers
    Dim first_open As Boolean = False

    'Error details
    Public ErrorDescription As String = ""                          'If an exception is fired, this variable holds the details and can be retrieved by double clicking the statuslabel error title

    'Navigator and videolist
    Dim MAX_VISIBLE_VIDEOS As Integer = 30                          'Maximum video items shown on the videolayout
    Dim visible_indexes As New List(Of Integer)                     'this is a subgroup of active indexes that is edited by seacrh
    Dim nav_page As Integer = 0                                     'current navigation page
    Dim last_page As Integer = 0                                    '0-based last page. Math.ceiling(visible_indexes.count / MAX_VISIBLE_VIDEOS) - 1
    Dim page_changing As Boolean = False                            'flag that indicates to navigatorTextBox when the index is changed by system functions
    Dim videoHolders As New List(Of VideoPreviewControl)
    Dim PREVIEW_PIC_SIZE As Integer = 2                               'Default = 1, 0 = small (160x95), 1 = regular (216x126), 2 = large (320x190), 3 = very large (576x342)
    Dim populate_layout_busy As Boolean = False
    Dim cancel_populate_layout As Boolean = False
    Dim SHOW_RATING As Boolean = True                               'show star rating control on videos
    Dim previewChangeSpeed As Integer = 1                           '0 = slow (1100 ms), 1 = default/medium (700 ms), 2 = fast (500 ms)
    Dim SHOW_TAG_MENU As Boolean = True


    'inactivate controls when loading settings
    Dim loading_settings As Boolean = False

    'search
    Dim DurationTrackBarMouseDown As Boolean = False
    Dim duration_filter_under As Integer = Integer.MaxValue                       'limit search results by duration. return everything under, Integer.MaxValue = disable
    Dim duration_filter_over As Integer = Integer.MinValue                        'limit search results by duration. return everything over, Integer.MinValue = disable
    Dim rating_filter_under As Integer = Integer.MaxValue                       'limit search results by duration. return everything under, Integer.MaxValue = disable
    Dim rating_filter_over As Integer = Integer.MinValue                        'limit search results by duration. return everything over, Integer.MinValue = disable
    Dim autoCompleteList As New AutoCompleteStringCollection
    Dim autoCompleteListCounts As New Dictionary(Of String, Integer)
    Dim lastAutoCompleteGeneration As DateTime = Nothing
    Dim sortBy As String = "relevance"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'get AppData dir
        _ProgramFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\VideoCatalog\"

        'create AppData dir on first open or if it has been deleted
        If Directory.Exists(_ProgramFolder) = False Then
            Directory.CreateDirectory(_ProgramFolder)
            first_open = True
        End If

        'get file paths
        _IndexFilePath = _ProgramFolder + "indexFile.txt"
        _OldIndexFilePath = _ProgramFolder + "indexFile_old.txt"
        _PreviewFileDirectory = _ProgramFolder + "previews\"
        SettingsFile = _ProgramFolder + "VideoCatalog_settings.txt"
        ErrorFile = _ProgramFolder + "ErrorLog.txt"
        _ModelFolder = _ProgramFolder + "models\"

        'hide advanced search panel
        AdvancedSearchPanel.Visible = False

        'show version at the version label
        Versionlbl.Text = "Version: " + Application.ProductVersion

        'Add black borders
        SearchTableLayout.BorderStyle = FormBorderStyle.FixedSingle
        NavigatorTable.BorderStyle = FormBorderStyle.FixedSingle
        VideoFlowLayoutPanel.BorderStyle = FormBorderStyle.FixedSingle

        If (ElementHost1.Child Is Nothing) Then
            ElementHost1.Child = New PreviewPlayer()
        End If

    End Sub

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        'Shown event is fired only once after the form has completely loaded

        'This is done only once to get environment specific names for the required extended property fields such as 'frame width'
        If (first_open) Then calibrate_extended_property_fields()

        If File.Exists(SettingsFile) Then                                                       'See if the settings file exist
            Load_settings()                                                                     'Load the setting from the .txt file
            If (PREVIEW_PIC_SIZE = 3) Then
                Me.Size = New Size(1250, 800)
            End If
        Else
                Create_settings()                                                                   'Create a default settings file
        End If

        init_database()

        init_bg_worker()

        'if there are no root directories set
        If (first_open Or ROOT_DIRECTORIES.Count = 0) Then Dialog_first_open()

        create_videoHolders()

        populate_videolayout()

        GenerateAutoCompleteList()

        'Start background refresh
        If (active_indexes.Count > 0) Then
            refresh_files(True)
        End If

    End Sub

#End Region

#Region "Backgroundworkers"

    Private Sub init_bg_worker()
        bw.WorkerReportsProgress = True
        bw.WorkerSupportsCancellation = True
        AddHandler bw.DoWork, AddressOf bw_DoWork
        AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
        AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted


        bw_AI.WorkerSupportsCancellation = True
        AddHandler bw_AI.DoWork, AddressOf bw_AI_DoWork
        AddHandler bw_AI.RunWorkerCompleted, AddressOf bw_AI_RunWorkerCompleted

    End Sub

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        Try

            'activate progressbar
            bw_invoke_progress("Searching for videos...", 10)

            active_indexes_buffer.Clear()  'rebuild active indexes
            refresh_has_changes = False     'reset

            'file spider
            Dim listOfFilePaths As New List(Of String)

            Dim root_count As Integer = ROOT_DIRECTORIES.Count
            Dim running_index As Integer = 0
            Dim progress_per_directory As Integer = 40 / root_count

            'go through all root folders
            For Each root In ROOT_DIRECTORIES
                Dim file_count As Integer = IO.Directory.GetFiles(root, "*.*", SearchOption.AllDirectories).Count
                Dim file_index As Integer = 0

                For Each _filename As String In IO.Directory.GetFiles(root, "*.*", SearchOption.AllDirectories)
                    If (isValidFile(_filename)) Then

                        'Check file fingerprint to avoid duplicates
                        Dim fingerprint As String = Checksum.getFingerprint(_filename) ' getCRC32Fingerprint(_filename) 'getFingerprint(_filename)
                        If fingerprint <> "" Then
                            If (vidListIndexOf(fingerprint) = -1) Then              'check if the file is already on the list
                                'Search properties from extended property fields only when the format is not natively supported by WMP
                                'shell queries need UI thread and it is slower but provide potentially more metadata than MWP, e.g. video fps
                                Dim shell_query As Boolean = False
                                For Each non_supported_extension In WMP_UNSUPPORTED_EXTENSIONS
                                    If (_filename.EndsWith(non_supported_extension)) Then shell_query = True
                                Next
                                If (shell_query) Then
                                    Me.Invoke(New addVideoToList_delegate(AddressOf addVideoToList), New Object() {_filename, fingerprint, shell_query})
                                Else
                                    addVideoToList(_filename, fingerprint, shell_query)
                                End If
                                If (Not active_indexes_buffer.Contains(VidList.Count - 1)) Then
                                    active_indexes_buffer.Add(VidList.Count - 1)   'add last index to active indexes
                                End If
                                refresh_has_changes = True                      'mark that something has changed
                            Else
                                If (Not active_indexes_buffer.Contains(vidListIndexOf(fingerprint))) Then
                                    active_indexes_buffer.Add(vidListIndexOf(fingerprint))   'mark index as active still
                                End If
                                'update path if it has changed
                                If (Not _filename.Equals(VidList(vidListIndexOf(fingerprint)).getPath)) Then
                                    VidList(vidListIndexOf(fingerprint)).setPath(_filename)
                                End If
                            End If
                        End If

                    End If

                    If (bw.CancellationPending) Then e.Cancel = True : Return

                    'report progress
                    file_index += 1
                    bw_invoke_progress("Searching for videos...", Calculate_progress(10 + (running_index * progress_per_directory), 10 + (running_index * progress_per_directory) + progress_per_directory, file_index, file_count))

                Next

                'report progress
                running_index += 1
                bw_invoke_progress("Searching for videos...", Calculate_progress(10, 50, running_index, root_count))

            Next

            'report progress
            bw_invoke_progress("Generating preview thumbnails...", 50)

            'refresh previews
            refresh_previews(silent_refresh)

        Catch ex As Exception
            'Notify user
            _Error("Exception during file refresh. Please try again.", ex.ToString)
        End Try
    End Sub

    Private Sub bw_invoke_progress(ByVal status As String, ByVal progress As Integer)
        'activate progressbar
        background_status = status
        bw.ReportProgress(progress)
    End Sub

    'Returns a value between allowed_min and allowed_max in the relation of jobs_done / jobs_total
    'Return value range is 0 to 100
    Private Function Calculate_progress(ByVal allowed_min As Integer, ByVal allowed_max As Integer, ByVal jobs_done As Integer, ByVal jobs_total As Integer) As Integer
        Dim progress As Integer
        'avoid division with zero exceptions
        If (jobs_total <= 0) Then
            progress = allowed_max
        Else
            'calculate how much is done
            Dim percentage_done As Double = jobs_done / jobs_total

            progress = allowed_min + Math.Round(percentage_done * (allowed_max - allowed_min))
        End If

        'check bounds
        progress = Math.Min(100, progress)
        progress = Math.Max(0, progress)

        Return progress

    End Function

    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        If (silent_refresh) Then Return
        statusLabel.Text = background_status
        BottomProgressBar.Value = e.ProgressPercentage
    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)

        Console.WriteLine("Finish file refresh")

        If Not silent_refresh Then
            Me.Cursor = Cursors.Default
            'reset status and progress
            statusLabel.Text = ""
            BottomProgressBar.Value = 0

            'hide cancel button
            CancelLoadingBtn.Visible = False

        End If

        'hide screenshots if the progress was cancelled
        'destroy videoclass
        If (e.Cancelled And screenCaptureClass IsNot Nothing) Then
            screenCaptureClass.unload()
        End If
        'hide label from preview screens
        preview_screen_lbl.Visible = False

        'Check if the active indexes have changed
        For Each ind In active_indexes_buffer
            If Not active_indexes.Contains(ind) Then
                refresh_has_changes = True
                Exit For
            End If
        Next
        For Each ind In active_indexes
            If Not active_indexes_buffer.Contains(ind) Then
                refresh_has_changes = True
                Exit For
            End If
        Next

        'copy active list buffer to active list
        If (refresh_has_changes And e.Cancelled = False) Then
            active_indexes.Clear()
            active_indexes.AddRange(active_indexes_buffer)
        End If

        Console.WriteLine("Has changes = " + refresh_has_changes.ToString)

        'File previews are missing, ask the user to do a non-silent refresh
        If (previews_need_refresh And silent_refresh) Then
            Dialog_missing_previews()
            Return
        End If

        'Start AI tagging
        If (e.Cancelled = False) Then
            startAIClassifier()
        End If

        'skip if silent refresh found no changes
        If (silent_refresh And Not refresh_has_changes) Then Return

        'save to file
        save_indexFile()

        'update settings to include active indexes
        Create_settings()

        populate_videolayout()

        'message how many videos were indexed
        If (Not silent_refresh) Then
            MsgBox(active_indexes.Count.ToString + " videos indexed.", MsgBoxStyle.OkOnly, "Finished")
        End If

    End Sub

#Region "Image classifier backgroundworker"

    Private Sub startAIClassifier()
        If (Not bw_AI.IsBusy) Then
            Console.WriteLine("Start AI tagging")
            bw_AI.RunWorkerAsync()
        End If
    End Sub

    Private Sub bw_AI_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        'Start AI tag check
        Dim TF As New TF_Inception
        Try
            TF.AITagVideos(VidList, _ModelFolder, _PreviewFileDirectory, active_indexes, bw_AI)
        Catch ex As Exception
            Console.WriteLine("AI exception")
        End Try
    End Sub

    Private Sub bw_AI_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Console.WriteLine("AI tagging finished")
        'recreate autocomplete lists

        GenerateAutoCompleteList()
    End Sub

#End Region

#End Region

#Region "Settings"

    Private Sub Load_settings()
        loading_settings = True
        Try
            Dim _Settings() As String = File.ReadAllLines(SettingsFile)
            If _Settings.Length > 0 Then
                Dim parts() As String
                For i As Integer = 0 To _Settings.Count - 1
                    parts = _Settings(i).Split(New Char() {"="c})
                    Select Case parts(0)
                        Case "ROOT_DIRS"
                            Dim _dirs() As String = parts(1).Split(New Char() {";"c}, StringSplitOptions.RemoveEmptyEntries)
                            For u As Integer = 0 To _dirs.Count - 1
                                ROOT_DIRECTORIES.Add(_dirs(u))
                            Next
                        Case "FIELD_FRAME_WIDTH"
                            FIELD_FRAME_WIDTH = parts(1)
                        Case "FIELD_FRAME_HEIGHT"
                            FIELD_FRAME_HEIGHT = parts(1)
                        Case "FIELD_FPS"
                            FIELD_FPS = parts(1)
                        Case "FIELD_LENGTH"
                            FIELD_LENGTH = parts(1)
                        Case "ACTIVE_INDEXES"
                            Dim _indexes() As String = parts(1).Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
                            active_indexes.Clear()
                            For Each ind In _indexes
                                active_indexes.Add(Integer.Parse(ind))
                            Next
                        Case "MAX_VISIBLE_VIDEOS"
                            Dim MAX_VISIBLE_VIDEOS_BUFFER As Integer = 30
                            If (Integer.TryParse(parts(1), MAX_VISIBLE_VIDEOS_BUFFER)) Then
                                MAX_VISIBLE_VIDEOS = MAX_VISIBLE_VIDEOS_BUFFER
                            Else
                                MAX_VISIBLE_VIDEOS = 30 'default
                            End If
                            'update UI
                            MaxVisibleVideosNumeric.Value = MAX_VISIBLE_VIDEOS
                        Case "PREVIEW_PIC_SIZE"
                            Dim PREVIEW_PIC_SIZE_BUFFER As Integer = 2
                            If (Integer.TryParse(parts(1), PREVIEW_PIC_SIZE_BUFFER)) Then
                                PREVIEW_PIC_SIZE = PREVIEW_PIC_SIZE_BUFFER
                            Else
                                PREVIEW_PIC_SIZE = 2 'default
                            End If
                            'update UI
                            Select Case PREVIEW_PIC_SIZE
                                Case 0
                                    PreviewSizeRadio_Small.Checked = True
                                Case 1
                                    PreviewSizeRadio_Medium.Checked = True
                                Case 2
                                    PreviewSizeRadio_Large.Checked = True
                                Case 3
                                    PreviewSizeRadio_Verylarge.Checked = True
                            End Select
                        Case "SHOW_RATING"
                            If (parts(1).Equals("true")) Then
                                SHOW_RATING = True
                            Else
                                SHOW_RATING = False
                            End If
                            'update UI
                            ShowRatingCb.Checked = SHOW_RATING
                        Case "PREVIEW_CHANGE_SPEED"
                            Dim previewChangeSpeedBuffer As Integer = 1
                            If (Integer.TryParse(parts(1), previewChangeSpeedBuffer)) Then
                                previewChangeSpeed = previewChangeSpeedBuffer
                            Else
                                previewChangeSpeed = 1  'default
                            End If
                            Select Case previewChangeSpeed
                                Case 0  'slow
                                    PreviewSpeedRadio_slow.Checked = True
                                Case 1  'default
                                    PreviewSpeedRadio_default.Checked = True
                                Case 2  'fast
                                    PreviewSpeedRadio_fast.Checked = True
                                Case 3  'very fast
                                    PreviewSpeedRadio_veryfast.Checked = True
                            End Select
                        Case "SHOW_TAG_MENU"
                            If (parts(1).Equals("true")) Then
                                SHOW_TAG_MENU = True
                            Else
                                SHOW_TAG_MENU = False
                            End If
                            'update UI
                            TagMenuCb.Checked = SHOW_TAG_MENU
                    End Select
                Next
            End If
        Catch ex As Exception
            _Error("Exception on loading settings", ex.ToString)
        End Try
        loading_settings = False
    End Sub

    Private Sub Create_settings()
        Try
            Using _SW As StreamWriter = File.CreateText(SettingsFile)
                Dim root_dir_string = "ROOT_DIRS="
                For Each Dir As String In ROOT_DIRECTORIES
                    root_dir_string += Dir + ";"
                Next
                _SW.WriteLine(root_dir_string)

                _SW.WriteLine("FIELD_FRAME_WIDTH=" + FIELD_FRAME_WIDTH)
                _SW.WriteLine("FIELD_FRAME_HEIGHT=" + FIELD_FRAME_HEIGHT)
                _SW.WriteLine("FIELD_FPS=" + FIELD_FPS)
                _SW.WriteLine("FIELD_LENGTH=" + FIELD_LENGTH)
                Dim indexList As String = "ACTIVE_INDEXES="
                For Each ind In active_indexes
                    indexList += ind.ToString + ","
                Next
                _SW.WriteLine(indexList)
                _SW.WriteLine("MAX_VISIBLE_VIDEOS=" + MAX_VISIBLE_VIDEOS.ToString)
                _SW.WriteLine("PREVIEW_PIC_SIZE=" + PREVIEW_PIC_SIZE.ToString)
                If (SHOW_RATING) Then
                    _SW.WriteLine("SHOW_RATING=true")
                Else
                    _SW.WriteLine("SHOW_RATING=false")
                End If
                _SW.WriteLine("PREVIEW_CHANGE_SPEED=" + previewChangeSpeed.ToString)
                If (SHOW_TAG_MENU) Then
                    _SW.WriteLine("SHOW_TAG_MENU=true")
                Else
                    _SW.WriteLine("SHOW_TAG_MENU=false")
                End If

            End Using
        Catch ex As Exception
            _Error("Exception on creating settings", ex.ToString)
        End Try
    End Sub

#End Region

#Region "Dialogs"

    'This dialog guides through setup
    Private Sub Dialog_first_open()
        Dim result As Integer = MessageBox.Show("VideoCatalog can organize and help you search for your videos. Start by selecting the folders you want to include in this catalog." + vbCrLf + "You can modify the folders from the settings at any time.", "Welcome!", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If result = DialogResult.OK Then

            'open root folder selection
            Dim root_setup As Integer = Root_setup_dialog()

            'if the roots were setup, ask to search for the files
            If (root_setup = DialogResult.OK) Then

                result = MessageBox.Show("Now you can refresh the list of your videos." + vbCrLf + "You can refresh the list of videos from the settings at any time.", "Refresh lists?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

                'Start refresh
                If result = DialogResult.OK Then
                    refresh_files(False)
                End If

            End If

        End If

    End Sub

    Private Sub Dialog_missing_previews()
        Dim result As Integer = MessageBox.Show("The contents of your library have changed. Refresh now?", "Refresh library?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        If result = DialogResult.OK Then
            'change tab to settings tab - preview frame box has to be initialized in order to generate previews
            MainTabControl.SelectTab(MainTabControl.TabPages.IndexOf(MainTabSettings))

            'Start refresh
            If result = DialogResult.OK Then
                refresh_files(False)
            End If

        End If

    End Sub

#End Region

#Region "Database and videolist"

    'First thing to do. On the first run, create necessary files and folders and otherwise just read them
    Private Sub init_database()
        'Check
        check_db_exist()

        'read existing db
        load_indexFile()

    End Sub

    Private Sub check_db_exist()
        'index file
        If (Not File.Exists(_IndexFilePath)) Then
            createIndexFile()
        End If

        'preview folder
        If (Not Directory.Exists(_PreviewFileDirectory)) Then
            Directory.CreateDirectory(_PreviewFileDirectory)
        End If

        'model folder
        If (Not Directory.Exists(_ModelFolder)) Then
            Directory.CreateDirectory(_ModelFolder)
        End If

    End Sub

    Private Sub load_indexFile()

        'clear videolist
        VidList.Clear()

        'Each line except the first represents a video file with all its meta data
        Dim _Indexes() As String = File.ReadAllLines(_IndexFilePath)
        If _Indexes.Length > 1 Then 'First line is always 'date created or updated line'
            Dim parts() As String
            For i As Integer = 1 To _Indexes.Count - 1
                parts = _Indexes(i).Split(New Char() {";"c})

                'create empty fields for placeholding
                Dim _path As String = ""
                Dim _id As String = ""
                Dim _extension As String = ""
                Dim _tags As String() = {}
                Dim _length As Long = 0
                Dim _resolution As Size = New Size(0, 0)
                Dim _size As Long = 0
                Dim _created As String = ""
                Dim _edited As String = ""
                Dim _fps As String = ""
                Dim _path_tags As String() = {}
                Dim _AI_tags As String() = {}
                Dim _rating As Integer = -1
                Dim _preview_index As Integer = -1


                'go through every field
                For Each field In parts
                    'Each field is additionally split to key-value pairs "key=value"
                    Dim keyval() As String
                    If (field.Length > 0) Then
                        keyval = field.Split(New Char() {"="c})

                        Select Case keyval(0)
                            Case "PATH"
                                _path = keyval(1)
                            Case "ID"
                                _id = keyval(1)
                            Case "EXTENSION"
                                _extension = keyval(1)
                            Case "TAGS"
                                'Split tags with comma
                                Dim tagParts() As String
                                tagParts = keyval(1).Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
                                _tags = tagParts
                            Case "LENGTH"
                                _length = Long.Parse(keyval(1))
                            Case "RESOLUTION"
                                Dim resolutionParts() As String = keyval(1).Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
                                _resolution = New Size(Integer.Parse(resolutionParts(0)), Integer.Parse(resolutionParts(1)))
                            Case "SIZE"
                                _size = Long.Parse(keyval(1))
                            Case "CREATED"
                                _created = keyval(1)
                            Case "EDITED"
                                _edited = keyval(1)
                            Case "FPS"
                                _fps = keyval(1)
                            Case "PATH_TAGS"
                                'Split tags with comma
                                Dim tagParts() As String
                                tagParts = keyval(1).Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
                                _path_tags = tagParts
                            Case "AI_TAGS"
                                'Split tags with comma
                                Dim tagParts() As String
                                tagParts = keyval(1).Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
                                _AI_tags = tagParts
                            Case "RATING"
                                _rating = Integer.Parse(keyval(1))
                            Case "PREVIEW_INDEX"
                                _preview_index = Integer.Parse(keyval(1))
                        End Select
                    End If
                Next

                'add video file to the list
                VidList.Add(New VideoFile(_path, _id, _extension, _tags, _length, _resolution, _size, _created, _edited, _fps, _path_tags, _AI_tags, _rating, _preview_index))
            Next
        End If
    End Sub

    Private Sub save_indexFile()
        'rename old indexfile
        If (File.Exists(_OldIndexFilePath)) Then
            File.Delete(_OldIndexFilePath)
        End If
        'rename
        File.Move(_IndexFilePath, _OldIndexFilePath)

        'then, create a new indexfile
        createIndexFile()

        'clear flags
        For Each vid In VidList
            vid.clearDBChanges()
        Next
    End Sub

    Private Sub createIndexFile()
        'create
        Using _SW As StreamWriter = File.CreateText(_IndexFilePath)
            Dim Datestr As String = "CREATED:"
            Datestr += DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss")
            _SW.WriteLine(Datestr)

            If (VidList.Count > 0) Then
                For Each vid In VidList
                    'parse to line
                    Dim line As String = "PATH=" + vid.getPath + ";ID=" + vid.getId + ";EXTENSION=" + vid.getExtension
                    Dim tags As String() = vid.getTags
                    Dim tagStr As String = ""
                    For Each _tag In tags
                        tagStr += _tag + ","
                    Next
                    line += ";TAGS=" + tagStr
                    line += ";LENGTH=" + vid.getLength().ToString + ";RESOLUTION=" + vid.getresolution().Width.ToString + "," + vid.getresolution().Height.ToString + ";SIZE=" + vid.getSize().ToString
                    line += ";CREATED=" + vid.getCreated + ";EDITED=" + vid.getEdited + ";FPS=" + vid.getFPS

                    Dim pathTags As String() = vid.getPathTags
                    Dim pathTagStr As String = ""
                    For Each _path_tag In pathTags
                        pathTagStr += _path_tag + ","
                    Next
                    line += ";PATH_TAGS=" + pathTagStr

                    Dim AITags As String() = vid.getAITags
                    Dim AITagStr As String = ""
                    For Each _AI_tag In AITags
                        AITagStr += _AI_tag + ","
                    Next
                    line += ";AI_TAGS=" + AITagStr
                    line += ";RATING=" + vid.getRating().ToString
                    line += ";PREVIEW_INDEX=" + vid.getPreviewIndex().ToString

                    _SW.WriteLine(line)
                Next
            End If

        End Using

    End Sub

#End Region

#Region "VidList search"

    Private Function vidListIndexOf(ByVal _id As String) As Integer
        If (VidList.Count = 0) Then Return -1
        For i As Integer = 0 To VidList.Count - 1
            If (VidList(i).getId().Equals(_id)) Then
                Return i
            End If
        Next
        Return -1
    End Function

#End Region

#Region "refresh"

    Private Sub refresh_files(ByVal isSilent As Boolean, Optional isFix As Boolean = False)
        If (Not bw.IsBusy) Then
            silent_refresh = isSilent
            fix_previews = isFix
            If Not (silent_refresh) Then
                Me.Cursor = Cursors.WaitCursor
                CancelLoadingBtn.Visible = True              'show cancel button
            End If
            'cancel background tagging
            If (bw_AI.IsBusy) Then
                bw_AI.CancelAsync()
            End If
            Console.WriteLine("Start file refresh")
            bw.RunWorkerAsync()
        Else
            Dim result As Integer = MessageBox.Show("Cancel refresh that is already running.", "Cancel current refresh?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If (result = MsgBoxResult.Yes) Then
                bw.CancelAsync()
            End If
        End If
    End Sub

    Private Function isValidFile(ByVal path As String) As Boolean
        For Each extension In VALID_EXTENSIONS
            If (path.EndsWith(extension, StringComparison.OrdinalIgnoreCase)) Then Return True
        Next
        Return False
    End Function

    'Shell32 which is a COM component requires an STA thread
    Private Delegate Sub addVideoToList_delegate(<[ParamArray]()> ByVal path As String, ByVal md5 As String, ByVal shell_query As Boolean)

    Private Sub addVideoToList(ByVal path As String, ByVal md5 As String, ByVal shell_query As Boolean)
        'process file and get all available info
        Dim _id As String = md5
        If (_id = "") Then _id = Checksum.getFingerprint(path)
        Dim _created As String = File.GetCreationTime(path).ToString("yyyy.MM.dd-HH.mm")
        Dim _edited As String = File.GetLastWriteTime(path).ToString("yyyy.MM.dd-HH.mm")
        Dim _extension As String = path.Substring(path.LastIndexOf(".") + 1, path.Length - path.LastIndexOf(".") - 1)
        Dim _size As Long = New FileInfo(path).Length
        Dim _tags As String() = {}
        Dim _path_tags As String() = {}
        Dim _AI_tags As String() = {}
        Dim _rating As Integer = -1
        Dim _preview_index As Integer = -1

        Dim _resolution As Size = New Size(0, 0)
        Dim _fps As String = ""
        Dim _length As Long = 0
        'use dummy video with known properties to search for the correct names
        'indexes and names vary by os, installed apps and system language
        If (shell_query) Then
            Dim xtd As List(Of ShellInfo) = GetXtdShellInfo(path)
            For Each s As ShellInfo In xtd
                Try
                    Select Case s.Name
                        Case FIELD_FRAME_WIDTH
                            Dim _widthStr As String = s.Value.ToString()
                            If (_widthStr.Length > 0) Then
                                Dim _widthInt As Integer
                                If (Integer.TryParse(_widthStr, _widthInt)) Then
                                    _resolution.Width = _widthInt
                                End If
                            End If
                        Case FIELD_FRAME_HEIGHT
                            Dim _heightStr As String = s.Value.ToString()
                            If (_heightStr.Length > 0) Then
                                Dim _heightInt As Integer
                                If (Integer.TryParse(_heightStr, _heightInt)) Then
                                    _resolution.Height = _heightInt
                                End If
                            End If
                        Case FIELD_FPS
                            _fps = s.Value.ToString
                        Case FIELD_LENGTH
                            Dim _lengthstr As String = s.Value.ToString 'format is HH:mm:ss
                            Dim _lengthParts As String() = _lengthstr.Split(New Char() {":"c})
                            If (_lengthParts.Length = 3) Then
                                _length = Long.Parse(_lengthParts(2)) + 60 * Long.Parse(_lengthParts(1)) + 3600 * Long.Parse(_lengthParts(0))
                            End If
                    End Select
                Catch ex As Exception
                    _Error("Exception on reading file properties", ex.ToString)
                End Try
            Next
        End If

        'add video file to the list
        VidList.Add(New VideoFile(path, _id, _extension, _tags, _length, _resolution, _size, _created, _edited, _fps, _path_tags, _AI_tags, _rating, _preview_index))
    End Sub

#End Region

#Region "VideoLayout"

    'Call this before filling the videolayout and after MAX_VISIBLE_VIDEOS is changed
    Private Sub create_videoHolders()
        'clear possible views from the videolayout
        While VideoFlowLayoutPanel.Controls.Count > 0
            VideoFlowLayoutPanel.Controls.RemoveAt(0)
        End While

        'dispose holders
        videoHolders.Clear()

        'create new holders
        For i As Integer = 0 To MAX_VISIBLE_VIDEOS - 1
            videoHolders.Add(New VideoPreviewControl(Nothing, _PreviewFileDirectory, PREVIEW_PIC_SIZE, SHOW_RATING, previewChangeSpeed, SHOW_TAG_MENU, autoCompleteList))
        Next

        'fill layout with holders
        For i As Integer = 0 To MAX_VISIBLE_VIDEOS - 1
            VideoFlowLayoutPanel.Controls.Add(videoHolders(i))
        Next

    End Sub

    'Refresh visible videos
    Private Sub populate_videolayout()

        If (VidList.Count = 0) Then Return

        If (populate_layout_busy) Then
            'invoke stop
            cancel_populate_layout = True
            Return
        Else
            populate_layout_busy = True

            'get visible indexes
            update_visible_indexes()

            'check for cancel
            If (cancel_populate_layout) Then
                populate_layout_busy = False
                populate_videolayout_done()
                Return
            End If

            'reset page index
            nav_page = 0
            last_page = Math.Ceiling(visible_indexes.Count / MAX_VISIBLE_VIDEOS) - 1

            'update UI
            update_nav_buttons()

            'check for cancel
            If (cancel_populate_layout) Then
                populate_layout_busy = False
                populate_videolayout_done()
                Return
            End If

            update_videoLayout()

            'done
            populate_layout_busy = False

            populate_videolayout_done()
        End If
    End Sub

    Private Sub populate_videolayout_done()
        If (cancel_populate_layout) Then
            'reset
            cancel_populate_layout = False
            'restart
            populate_videolayout()
        End If
    End Sub

    Private Sub update_visible_indexes()
        'get search text
        Dim searchStr As String = SearchTextBox.Text

        'clear list
        visible_indexes.Clear()

        visible_indexes = Search.GetMatchingVideos(VidList, active_indexes, searchStr, duration_filter_under, duration_filter_over, rating_filter_under, rating_filter_over, sortBy, SortOrderCb.Checked)

    End Sub

    'call this on page change and after populate_layout
    Private Sub update_videoLayout()
        'return if empty list
        If (visible_indexes.Count = 0) Then
            'clear videos
            For i As Integer = 0 To MAX_VISIBLE_VIDEOS - 1
                videoHolders(i).emptyContent()
            Next
            Return
        End If

        'return if there are no videos
        If (VidList.Count = 0) Then
            Return
        End If

        'get items on current page
        Dim first_index As Integer = nav_page * MAX_VISIBLE_VIDEOS
        Dim last_index As Integer = Math.Min(visible_indexes.Count - 1, first_index + MAX_VISIBLE_VIDEOS - 1)
        If (visible_indexes.Count = 0) Then last_index = 0

        'set each holder to page change mode
        For i As Integer = 0 To MAX_VISIBLE_VIDEOS - 1
            videoHolders(i).setPageChange()
        Next
        System.Windows.Forms.Application.DoEvents()

        'change page items
        For i As Integer = 0 To MAX_VISIBLE_VIDEOS - 1
            If ((i <= (last_index - first_index)) And (last_index > -1)) Then
                videoHolders(i).LoadContent(VidList(visible_indexes(first_index + i)), _PreviewFileDirectory)
            Else
                videoHolders(i).emptyContent()
            End If

            'let ui update once a while
            If ((i > 0) And (i Mod 10 = 0)) Then
                If (cancel_populate_layout) Then Return   'exit if cancellation is pending
                System.Windows.Forms.Application.DoEvents()
            End If
        Next

        System.Windows.Forms.Application.DoEvents()
        GC.Collect()

    End Sub

#End Region

#Region "Page navigation"

    Private Sub nav_next()
        If (nav_page >= last_page) Then Return
        nav_page += 1
        update_nav_buttons()
        update_videoLayout()
    End Sub

    Private Sub nav_prev()
        If (nav_page <= 0) Then Return
        nav_page -= 1
        update_nav_buttons()
        update_videoLayout()
    End Sub

    Private Sub update_nav_buttons()
        'update labels
        page_changing = True
        If (visible_indexes.Count = 0) Then
            NavigatorTextBox.Text = "0"
            navigator_lbl.Text = "/ 0"
            videoCountlbl.Text = "0 videos"
        Else
            NavigatorTextBox.Text = (nav_page + 1).ToString
            navigator_lbl.Text = "/ " + (last_page + 1).ToString
            If (visible_indexes.Count = 1) Then
                videoCountlbl.Text = visible_indexes.Count.ToString + " video"
            Else
                videoCountlbl.Text = visible_indexes.Count.ToString + " videos"
            End If
            If (nav_page = 0) Then
                prev_btn.Enabled = False
                prev_btn.Image = My.Resources.prev_n
            End If
            If (nav_page = last_page) Then
                next_btn.Enabled = False
                next_btn.Image = My.Resources.next_n
            End If
            If (nav_page > 0) Then
                prev_btn.Enabled = True
            End If
            If (nav_page < last_page) Then
                next_btn.Enabled = True
            End If
        End If
        page_changing = False
        'let the ui update
        System.Windows.Forms.Application.DoEvents()
    End Sub

    Private Sub NavigatorTextBox_TextChanged(sender As Object, e As EventArgs) Handles NavigatorTextBox.TextChanged
        If Not (page_changing) Then
            Dim pageBuffer As Integer
            If (Integer.TryParse(NavigatorTextBox.Text, pageBuffer)) Then
                pageBuffer -= 1     'zero based number
                'check limits
                If (pageBuffer < 0) Then
                    pageBuffer = 0
                ElseIf (pageBuffer > last_page) Then
                    pageBuffer = last_page
                End If
            Else
                pageBuffer = nav_page
            End If
            If (NavigatorTextBox.Text <> (pageBuffer + 1).ToString) Then
                page_changing = True
                NavigatorTextBox.Text = (pageBuffer + 1).ToString
                page_changing = False
            End If
            'apply page
            If (pageBuffer <> nav_page) Then
                nav_page = pageBuffer
                update_nav_buttons()
                update_videoLayout()
            End If
        End If
    End Sub

#Region "Navigation button events"

    'Prev btn
    Private Sub prev_btn_MouseEnter(sender As Object, e As EventArgs) Handles prev_btn.MouseEnter
        If (prev_btn.Enabled) Then
            prev_btn.Image = My.Resources.prev_h
        End If
    End Sub

    Private Sub prev_btn_MouseLeave(sender As Object, e As EventArgs) Handles prev_btn.MouseLeave
        If (prev_btn.Enabled) Then
            prev_btn.Image = My.Resources.prev_n
        End If
    End Sub

    Private Sub prev_btn_MouseDown(sender As Object, e As MouseEventArgs) Handles prev_btn.MouseDown
        If (prev_btn.Enabled) Then
            prev_btn.Image = My.Resources.prev_p
            nav_prev()
        End If
    End Sub

    Private Sub prev_btn_MouseUp(sender As Object, e As MouseEventArgs) Handles prev_btn.MouseUp
        If (prev_btn.Enabled) Then
            prev_btn.Image = My.Resources.prev_h
        Else
            prev_btn.Image = My.Resources.prev_n
        End If
    End Sub

    'Next btn
    Private Sub next_btn_MouseEnter(sender As Object, e As EventArgs) Handles next_btn.MouseEnter
        If (next_btn.Enabled) Then
            next_btn.Image = My.Resources.next_h
        End If
    End Sub

    Private Sub next_btn_MouseLeave(sender As Object, e As EventArgs) Handles next_btn.MouseLeave
        If (next_btn.Enabled) Then
            next_btn.Image = My.Resources.next_n
        End If
    End Sub

    Private Sub next_btn_MouseDown(sender As Object, e As MouseEventArgs) Handles next_btn.MouseDown
        If (next_btn.Enabled) Then
            next_btn.Image = My.Resources.next_p
            nav_next()
        End If
    End Sub

    Private Sub next_btn_MouseUp(sender As Object, e As MouseEventArgs) Handles next_btn.MouseUp
        If (next_btn.Enabled) Then
            next_btn.Image = My.Resources.next_h
        Else
            next_btn.Image = My.Resources.next_n
        End If
    End Sub

#End Region

#End Region

#Region "Extended file properties"

    Private Function GetXtdShellInfo(filepath As String) As List(Of ShellInfo)
        Dim xtd As New List(Of ShellInfo)

        Try
            Dim shell As New Shell32.Shell
            Dim shFolder As Shell32.Folder
            shFolder = shell.NameSpace(Path.GetDirectoryName(filepath))

            ' its com so iterate to find what we want -
            ' or modify to return a dictionary of lists for all the items
            Dim key As String

            For Each s In shFolder.Items
                ' look for the one we are after
                If shFolder.GetDetailsOf(s, 0).ToLowerInvariant = Path.GetFileName(filepath).ToLowerInvariant Then

                    Dim ndx As Int32 = 0
                    key = shFolder.GetDetailsOf(shFolder.Items, ndx)

                    ' there are a varying number of entries depending on the OS
                    ' 34 min, W7=290, W8=309 with some blanks

                    ' this should get up to 310 non blank elements

                    Do Until String.IsNullOrEmpty(key) AndAlso ndx > 310
                        If String.IsNullOrEmpty(key) = False Then
                            xtd.Add(New ShellInfo(key,
                                              shFolder.GetDetailsOf(s, ndx)))
                        End If
                        ndx += 1
                        key = shFolder.GetDetailsOf(shFolder.Items, ndx)
                    Loop

                    ' we got what we came for
                    Exit For
                End If
            Next
        Catch ex_cast As InvalidCastException
            'shell32 failed to cast
        Catch ex As Exception
            _Error("Exception on getting file properties", ex.ToString)
        End Try
        Return xtd
    End Function

    'extended file property indexes and names vary by os, installed apps and system language
    'so this function uses a dummy video file With known properties to find the localized names for the wanted properties
    Private Sub calibrate_extended_property_fields()
        'write a dummy video with known properties to disk
        Try
            'dummy file
            Dim dummyPath As String = _ProgramFolder + "dummy.avi"

            'if it already exists, recreate it to make sure it is the right file
            If (File.Exists(dummyPath)) Then
                File.Delete(dummyPath)
            End If

            'write the file
            Dim b As Byte() = My.Resources.dummy
            My.Computer.FileSystem.WriteAllBytes(dummyPath, b, False)

            'The dummy video file has Resolution of 640x480 and framerate of 24.0
            Dim xtd As List(Of ShellInfo) = GetXtdShellInfo(dummyPath)
            For Each s As ShellInfo In xtd
                If (s.Name Is Nothing) Then Continue For
                Select Case s.Value.ToString
                    Case "640"
                        FIELD_FRAME_WIDTH = s.Name
                    Case "480"
                        FIELD_FRAME_HEIGHT = s.Name
                    Case "00:00:01"
                        FIELD_LENGTH = s.Name
                End Select

                If (s.Value.ToString.Contains("24.00") Or s.Value.ToString.Contains("24,00")) Then
                    FIELD_FPS = s.Name
                End If
            Next

            'Dummy file is no longer needed
            File.Delete(dummyPath)

        Catch ex As Exception
            _Error("Exception on calibrating extended property fields", ex.ToString)
        End Try
    End Sub

#End Region

#Region "Previews"

    Private Delegate Sub refresh_previews_delegate(ByVal isSilent As Boolean)

    Private Sub refresh_previews(ByVal isSilent As Boolean)

        previews_need_refresh = False   'reset

        If ElementHost1.InvokeRequired Then
            ElementHost1.Invoke(New refresh_previews_delegate(AddressOf refresh_previews), New Object() {isSilent})
        Else

            'first, remove all not in the list
            Dim previewDirs As String() = Directory.GetDirectories(_PreviewFileDirectory)
            For Each previewDir In previewDirs
                Dim onTheList As Boolean = False
                For Each vid In VidList
                    If (previewDir.Contains(vid.getId())) Then
                        onTheList = True
                        Exit For
                    End If
                Next

                'remove previewdir if it's not on the list anymore
                If (Not onTheList) Then
                    Try
                        If (Directory.Exists(previewDir)) Then
                            Directory.Delete(previewDir, True)
                        End If
                    Catch ex As Exception
                        'directory might be in use
                        _Error("Exception on deleting old preview directory", ex.ToString)
                    End Try
                End If
            Next

            'next, get all files that are not yet previewed
            Dim previewMissingList As New List(Of VideoFile)
            For Each vid In VidList
                Dim onTheList As Boolean = False
                For Each previewDir In previewDirs
                    If (previewDir.Contains(vid.getId())) Then
                        'check preview generation version
                        If (File.Exists(previewDir + "\" + previewVersionFile)) Then
                            'check for preview file count. If it matches the set SCREENSHOT_COUNT, skip it
                            Dim previewFiles As String() = IO.Directory.GetFiles(previewDir, "*.jpg*", SearchOption.TopDirectoryOnly)
                            If (previewFiles.Count = SCREENSHOT_COUNT) Then
                                onTheList = True
                            Else
                                'check if the folder is marked with no_preview.jpg. This means that the preview files cannot be generated
                                If (Not fix_previews) Then
                                    For Each _file In previewFiles
                                        If (_file.Contains("no_preview.jpg")) Then
                                            onTheList = True
                                            Exit For
                                        End If
                                    Next
                                End If
                            End If
                        End If
                        Exit For
                    End If
                Next

                If (Not onTheList) Then
                    'Check that the file is currently active and available
                    If (active_indexes_buffer.Contains(vidListIndexOf(vid.getId()))) Then
                        previewMissingList.Add(vid)
                    End If
                End If
            Next

            'exit if user has cancelled
            If (bw.CancellationPending) Then Return

            'create previews
            Dim totalcount As Integer = previewMissingList.Count

            'Do not update previews if the refresh is silent. It would interrupt UI too much
            'Just notify the user about missing preview screenshots
            If (isSilent) Then
                previews_need_refresh = totalcount > 0
                Return
            End If

            Dim running_count As Integer = 0

            'show label on preview screens
            preview_screen_lbl.Visible = totalcount > 0

            For Each vid In previewMissingList
                'possible errors: file access exceptions, unable to generate a preview, etc
                Try
                    If (WMP_UNSUPPORTED_EXTENSIONS.Contains("." + vid.getExtension)) Then
                        'cannot use WMP here
                        'TODO get previews some other way
                        'add_preview(vid, True)

                        'if the codecs are installed, this will work
                        add_preview(vid, False)
                    Else
                        add_preview(vid, False)
                    End If
                Catch ex As Exception
                    _Error("Exception on creating previews", ex.ToString)
                End Try

                'exit if user has cancelled
                If (bw.CancellationPending) Then Return

                'report progress
                running_count += 1

                bw_invoke_progress("Generating preview thumbnails...", Calculate_progress(50, 100, running_count, totalcount))
            Next

            'destroy videoclass
            If (screenCaptureClass IsNot Nothing) Then
                screenCaptureClass.unload()
            End If

        End If
    End Sub

    Private Sub add_preview(ByRef vid As VideoFile, ByVal unsupportedFormat As Boolean)
        'create a directory for the previews
        Dim previewDir As String = _PreviewFileDirectory + vid.getId() + "\"
        If (Not Directory.Exists(previewDir)) Then
            Directory.CreateDirectory(previewDir)
        Else
            'new previews, delete old ones
            Try
                Directory.Delete(previewDir, True)
                Directory.CreateDirectory(previewDir)
            Catch ex As Exception
                Console.WriteLine("Could not delete preview directory. " + ex.ToString)
            End Try
        End If

        'Check if the file format is not supported, then just save preview not available
        If (unsupportedFormat) Then
            Try
                Dim filepath = _PreviewFileDirectory + vid.getId() + "\no_preview.jpg"
                'write the file
                Dim img As Image = My.Resources.no_preview
                img.Save(filepath)
            Catch ex As Exception
                Console.WriteLine(ex.ToString)
            End Try
            Return
        End If

        'get the MediaPlayer WPF control
        If (screenCaptureClass Is Nothing) Then
            screenCaptureClass = ElementHost1.Child
        End If

        'save screenshots
        screenCaptureClass.captureScreenshots(vid, previewDir, SCREENSHOT_COUNT, bw)

        'unload class
        screenCaptureClass.unload()

        'compile gif out of the screenshots
        'create_gif(previewDir)

    End Sub

    Private Sub create_gif(ByRef previewDir As String)
        'list of bitmaps
        Dim bitmaps As New List(Of Bitmap)
        Dim previewFiles As String() = IO.Directory.GetFiles(previewDir, "*.jpg*", SearchOption.TopDirectoryOnly)
        For Each prevFile In previewFiles
            bitmaps.Add(Image.FromFile(prevFile))
        Next

        'create gif encoder
        Dim encoder As New System.Windows.Media.Imaging.GifBitmapEncoder()
        For Each bmp In previewFiles
            encoder.Frames.Add(Imaging.BitmapFrame.Create(New Uri(bmp)))
        Next

        'Save gif to previewDir
        encoder.Save(New FileStream(previewDir + "animation.gif", FileMode.Create))

    End Sub

#End Region

#Region "AutoCompleteList"

    Private Sub GenerateAutoCompleteList()

        'Check last time autocomplete was generated. If it was two seconds ago, do not douple fire. This can happen at the app launch:
        'Launch -> generate autocomplete -> generate AI tags -> update autocomplete
        If (lastAutoCompleteGeneration <> Nothing) Then
            Dim dateNow As DateTime = DateTime.Now
            Dim span = lastAutoCompleteGeneration - dateNow
            If (span.Seconds < 2) Then
                Return
            End If
        End If

        'by clearing linked list the searchbox doesn't flicker during updates
        SearchTextBox.AutoCompleteCustomSource = Nothing

        Console.WriteLine("generatin autocomplete list")

        autoCompleteList.Clear()
        autoCompleteListCounts.Clear()

        For Each vid In VidList
            'check path tags
            For Each _tag In vid.getPathTags
                If Not (autoCompleteList.Contains(_tag)) Then
                    autoCompleteList.Add(_tag)
                End If
                If Not (autoCompleteListCounts.ContainsKey(_tag)) Then
                    autoCompleteListCounts.Add(_tag, 1)
                Else
                    autoCompleteListCounts(_tag) += 1
                End If
            Next
            'check user tags
            For Each _tag In vid.getTags
                If Not (autoCompleteList.Contains(_tag)) Then
                    autoCompleteList.Add(_tag)
                End If
                If Not (autoCompleteListCounts.ContainsKey(_tag)) Then
                    autoCompleteListCounts.Add(_tag, 1)
                Else
                    autoCompleteListCounts(_tag) += 1
                End If
            Next
            'check AI tags
            'check path tags
            For Each _tag In vid.getAITags
                'skip * indicator that marks if the file has been through the AI process
                If (_tag.Equals("*")) Then Continue For
                If Not (autoCompleteList.Contains(_tag)) Then
                    autoCompleteList.Add(_tag)
                End If
                If Not (autoCompleteListCounts.ContainsKey(_tag)) Then
                    autoCompleteListCounts.Add(_tag, 1)
                Else
                    autoCompleteListCounts(_tag) += 1
                End If
            Next
        Next

        SearchTextBox.AutoCompleteCustomSource = autoCompleteList

        'update last time generation was done
        lastAutoCompleteGeneration = DateTime.Now

        'Send random message
        Try
            'get list of values
            Dim values As List(Of Integer) = autoCompleteListCounts.Values.ToList
            'and sort from small to large
            values.Sort()
            'get random large-ish number from the values
            Dim rand As Random = New Random(DateTime.Now.Millisecond)
            Dim occurences As Integer = values(rand.Next(2 * values.Count / 3, values.Count - 1))

            'get any correspnding key that matches the value
            Dim random_key As String = ""
            For i As Integer = 0 To (autoCompleteListCounts.Count - 1)
                If (autoCompleteListCounts.ElementAt(i).Value = occurences) Then
                    random_key = autoCompleteListCounts.ElementAt(i).Key
                    'additional random
                    If (rand.Next(0, 12) > 9) Then
                        Exit For
                    End If
                End If
            Next

            SendMessage(Me.SearchTextBox.Handle, &H1501, 0, "Search. e.g. " + random_key + " (" + occurences.ToString + " occurences)")
        Catch ex As Exception
            SendMessage(Me.SearchTextBox.Handle, &H1501, 0, "Search")
        End Try

    End Sub



#End Region

#Region "Root directories"

    Private Function Root_setup_dialog() As Integer
        'Open root folder manager
        Dim root_manager As SetRoots = New SetRoots(ROOT_DIRECTORIES)
        Dim root_dialog_result As Integer = root_manager.ShowDialog
        If root_dialog_result = DialogResult.OK Then
            'update settings
            Create_settings()

            'return OK dialogresult
            Return DialogResult.OK
        ElseIf (root_dialog_result = DialogResult.No) Then
            'the list is empty but OK was pressed
            'update settings
            Create_settings()
            Return DialogResult.No
        End If
        'No root folders changed, return cancel
        Return DialogResult.Cancel
    End Function

#End Region

#Region "UI actions"

#Region "Search"

    Private Sub SearchTextBox_TextChanged(sender As Object, e As EventArgs) Handles SearchTextBox.TextChanged
        populate_videolayout()
    End Sub

    Private Sub AdvancedSearchBtn_Click(sender As Object, e As EventArgs) Handles AdvancedSearchBtn.Click
        'toggle advanced search
        AdvancedSearchPanel.Visible = Not AdvancedSearchPanel.Visible
    End Sub

#Region "Duration filter"

    Private Sub DurationFilterRadio_none_CheckedChanged(sender As Object, e As EventArgs) Handles DurationFilterRadio_none.CheckedChanged
        If (DurationFilterRadio_none.Checked) Then
            DurationFilterTrackbar.Enabled = False
            duration_filter_under = Integer.MaxValue
            duration_filter_over = Integer.MinValue
            populate_videolayout()
        End If
    End Sub

    Private Sub DurationFilterRadio_over_CheckedChanged(sender As Object, e As EventArgs) Handles DurationFilterRadio_over.CheckedChanged
        If (DurationFilterRadio_over.Checked) Then
            DurationFilterTrackbar.Enabled = True
            duration_filter_under = Integer.MaxValue
            duration_filter_over = DurationFilterTrackbar.Value
            populate_videolayout()
        End If
    End Sub

    Private Sub DurationFilterRadio_under_CheckedChanged(sender As Object, e As EventArgs) Handles DurationFilterRadio_under.CheckedChanged
        If (DurationFilterRadio_under.Checked) Then
            DurationFilterTrackbar.Enabled = True
            duration_filter_under = DurationFilterTrackbar.Value
            duration_filter_over = Integer.MinValue
            populate_videolayout()
        End If
    End Sub

    Private Sub DurationFilterTrackbar_Scroll(sender As Object, e As EventArgs) Handles DurationFilterTrackbar.Scroll
        'update UI
        durationFilterLbl.Text = getDurationString(DurationFilterTrackbar.Value)
    End Sub

    Private Sub DurationFilterTrackbar_ValueChanged(sender As Object, e As EventArgs) Handles DurationFilterTrackbar.ValueChanged
        'change values
        If (DurationFilterRadio_under.Checked) Then
            duration_filter_under = DurationFilterTrackbar.Value
            duration_filter_over = Integer.MinValue
        ElseIf (DurationFilterRadio_over.Checked) Then
            duration_filter_under = Integer.MaxValue
            duration_filter_over = DurationFilterTrackbar.Value
        ElseIf DurationFilterRadio_none.Checked Then
            duration_filter_under = Integer.MaxValue
            duration_filter_over = Integer.MinValue
        End If

        'invoke search
        If Not DurationTrackBarMouseDown Then populate_videolayout()
    End Sub

    Private Sub DurationFilterTrackbar_MouseDown(sender As Object, e As MouseEventArgs) Handles DurationFilterTrackbar.MouseDown
        DurationTrackBarMouseDown = True
    End Sub

    Private Sub DurationFilterTrackbar_MouseUp(sender As Object, e As MouseEventArgs) Handles DurationFilterTrackbar.MouseUp
        DurationTrackBarMouseDown = False
        populate_videolayout()
    End Sub

    Private Function getDurationString(ByVal dur As Integer)
        'get hours minutes and seconds
        Dim hours As Integer = Math.Floor(dur / 3600)
        Dim minutes As Integer = Math.Floor((dur - (3600 * hours)) / 60)
        Dim seconds As Integer = dur - (3600 * hours) - (60 * minutes)
        'string format
        Dim hourStr As String = hours.ToString
        If (hourStr.Length = 1) Then hourStr = "0" + hourStr
        Dim minutesStr As String = minutes.ToString
        If (minutesStr.Length = 1) Then minutesStr = "0" + minutesStr
        Dim secondsStr As String = seconds.ToString
        If (secondsStr.Length = 1) Then secondsStr = "0" + secondsStr

        Return "Duration: " + hourStr + ":" + minutesStr + ":" + secondsStr
    End Function



#End Region

#Region "Rating filter"

    Private Sub RatingFilterRadio_none_CheckedChanged(sender As Object, e As EventArgs) Handles RatingFilterRadio_none.CheckedChanged
        If (RatingFilterRadio_none.Checked) Then
            ratingFilterNumeric.Enabled = False
            rating_filter_under = Integer.MaxValue
            rating_filter_over = Integer.MinValue
            populate_videolayout()
        End If
    End Sub

    Private Sub RatingFilterRadio_over_CheckedChanged(sender As Object, e As EventArgs) Handles RatingFilterRadio_over.CheckedChanged
        If (RatingFilterRadio_over.Checked) Then
            ratingFilterNumeric.Enabled = True
            rating_filter_under = Integer.MaxValue
            rating_filter_over = ratingFilterNumeric.Value * 10
            populate_videolayout()
        End If
    End Sub

    Private Sub RatingFilterRadio_under_CheckedChanged(sender As Object, e As EventArgs) Handles RatingFilterRadio_under.CheckedChanged
        If (RatingFilterRadio_under.Checked) Then
            ratingFilterNumeric.Enabled = True
            rating_filter_under = ratingFilterNumeric.Value * 10
            rating_filter_over = Integer.MinValue
            populate_videolayout()
        End If
    End Sub

    Private Sub ratingFilterNumeric_ValueChanged(sender As Object, e As EventArgs) Handles ratingFilterNumeric.ValueChanged
        'change values
        If (RatingFilterRadio_under.Checked) Then
            rating_filter_under = ratingFilterNumeric.Value * 10
            rating_filter_over = Integer.MinValue
        ElseIf (RatingFilterRadio_over.Checked) Then
            rating_filter_under = Integer.MaxValue
            rating_filter_over = ratingFilterNumeric.Value * 10
        ElseIf RatingFilterRadio_none.Checked Then
            rating_filter_under = Integer.MaxValue
            rating_filter_over = Integer.MinValue
        End If
        populate_videolayout()
    End Sub


    Private Sub RatingFilterRadio_notrated_CheckedChanged(sender As Object, e As EventArgs) Handles RatingFilterRadio_notrated.CheckedChanged
        If (RatingFilterRadio_notrated.Checked) Then
            ratingFilterNumeric.Enabled = False
            rating_filter_under = 0
            rating_filter_over = Integer.MinValue
            populate_videolayout()
        End If
    End Sub

#End Region

#Region "sortbox"

    Private Sub SortByRelevanceCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortByRelevanceCb.CheckedChanged
        If (SortByRelevanceCb.Checked) Then
            sortBy = "relevance"
            populate_videolayout()
        End If
    End Sub

    Private Sub SortByDateCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortByDateCb.CheckedChanged
        If (SortByDateCb.Checked) Then
            sortBy = "date_created"
            populate_videolayout()
        End If
    End Sub

    Private Sub SortByLengthCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortByLengthCb.CheckedChanged
        If (SortByLengthCb.Checked) Then
            sortBy = "length"
            populate_videolayout()
        End If
    End Sub

    Private Sub SortByRatingCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortByRatingCb.CheckedChanged
        If (SortByRatingCb.Checked) Then
            sortBy = "rating"
            populate_videolayout()
        End If
    End Sub

    Private Sub SortByRandomCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortByRandomCb.CheckedChanged
        If (SortByRandomCb.Checked) Then
            sortBy = "random"
            populate_videolayout()
        End If
    End Sub

    Private Sub SortOrderCb_CheckedChanged(sender As Object, e As EventArgs) Handles SortOrderCb.CheckedChanged
        populate_videolayout()
    End Sub

#End Region

#End Region

#Region "Video library settings"

    Private Sub btn_select_folders_Click(sender As Object, e As EventArgs) Handles btn_select_folders.Click
        Dim root_setup As Integer = Root_setup_dialog()

        'if the roots were setup, ask to search for the files
        If (root_setup = DialogResult.OK) Then

            Dim result As Integer = MessageBox.Show("Refresh the list of your videos?" + vbCrLf + "You can refresh the list of videos from the settings at any time.", "Refresh lists?", MessageBoxButtons.OKCancel)

            'Start refresh
            If result = DialogResult.OK Then
                refresh_files(False)
            End If

        End If
    End Sub

    Private Sub btn_refresh_Click(sender As Object, e As EventArgs) Handles btn_refresh.Click
        refresh_files(False)
    End Sub

    Private Sub Fix_previews_btn_Click(sender As Object, e As EventArgs) Handles Fix_previews_btn.Click
        refresh_files(False, True)
    End Sub

    Private Sub Regenerate_AI_tags_btn_Click(sender As Object, e As EventArgs) Handles Regenerate_AI_tags_btn.Click
        'delete model version file
        If (File.Exists(_ModelFolder + "model_version.txt")) Then
            Try
                File.Delete(_ModelFolder + "model_version.txt")
            Catch ex As Exception
                Return
            End Try
        End If
        If (Not bw.IsBusy And Not bw_AI.IsBusy) Then
            MessageBox.Show("AI tags will be re-created at the background. You can continue to use this library normally.", "AI", MessageBoxButtons.OK, MessageBoxIcon.Information)
            startAIClassifier()
        Else
            If (bw.IsBusy) Then
                MsgBox("File refresh is running. New AI tags will be generated after.")
            Else
                MessageBox.Show("AI tagging is already running", "Action cancelled", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub CancelLoadingBtn_Click(sender As Object, e As EventArgs) Handles CancelLoadingBtn.Click
        If (bw.IsBusy) Then
            'cancel backgroundprogress
            bw.CancelAsync()
        End If
    End Sub

#End Region

#Region "General settings"

    Private Sub GeneralSettingsApplyBtn_Click(sender As Object, e As EventArgs) Handles GeneralSettingsApplyBtn.Click
        Me.Cursor = Cursors.WaitCursor
        Dim changes As Boolean = False

        'check if max video count has changed
        If (MaxVisibleVideosNumeric.Value <> MAX_VISIBLE_VIDEOS) Then
            MAX_VISIBLE_VIDEOS = MaxVisibleVideosNumeric.Value  'update value
            changes = True
        End If

        Dim PREVIEW_PIC_SIZE_BUFFER As Integer = 2
        If (PreviewSizeRadio_Small.Checked) Then
            PREVIEW_PIC_SIZE_BUFFER = 0
        ElseIf (PreviewSizeRadio_Medium.Checked) Then
            PREVIEW_PIC_SIZE_BUFFER = 1
        ElseIf (PreviewSizeRadio_Large.Checked) Then
            PREVIEW_PIC_SIZE_BUFFER = 2
        ElseIf (PreviewSizeRadio_Verylarge.Checked) Then
            PREVIEW_PIC_SIZE_BUFFER = 3
        End If

        'check if preview size has changed
        If (PREVIEW_PIC_SIZE_BUFFER <> PREVIEW_PIC_SIZE) Then
            PREVIEW_PIC_SIZE = PREVIEW_PIC_SIZE_BUFFER  'update value
            changes = True
        End If

        'check if star rating visibility has changed
        If (SHOW_RATING <> ShowRatingCb.Checked) Then
            SHOW_RATING = ShowRatingCb.Checked
            changes = True
        End If

        'check if preview change speed has changed
        Dim previewChangeSpeedBuffer As Integer = 1
        If (PreviewSpeedRadio_slow.Checked) Then
            previewChangeSpeedBuffer = 0
        ElseIf (PreviewSpeedRadio_default.Checked) Then
            previewChangeSpeedBuffer = 1
        ElseIf (PreviewSpeedRadio_fast.Checked) Then
            previewChangeSpeedBuffer = 2
        ElseIf (PreviewSpeedRadio_veryfast.Checked) Then
            previewChangeSpeedBuffer = 3
        End If
        If (previewChangeSpeedBuffer <> previewChangeSpeed) Then
            previewChangeSpeed = previewChangeSpeedBuffer
            changes = True
        End If

        'check if tag menu visibility has changed
        If (SHOW_TAG_MENU <> TagMenuCb.Checked) Then
            SHOW_TAG_MENU = TagMenuCb.Checked
            changes = True
        End If

        If (changes) Then
            Create_settings()   'update settings
            create_videoHolders()  'update page holders
            populate_videolayout()  'update views
        End If

        Me.Cursor = Cursors.Default
    End Sub

#End Region

#Region "About"

    'show copyright and license info
    Private Sub Licenselbl_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles Licenselbl.LinkClicked
        Process.Start("https://opensource.org/licenses/MIT")
    End Sub

#End Region

#End Region

#Region "Exception handling"

    'Thread safe function to call when catching exceptions
    Private Delegate Sub _Error_delegate(<[ParamArray]()> ByVal _Title As String, ByVal _details As String)

    'Title: This is visible to the user at the statuslbl. Should describe where the error happened.
    'Description: usually exception details (ex.toString)
    Public Sub _Error(ByVal _Title As String, ByVal _details As String)
        If StatusStrip1.InvokeRequired Then
            StatusStrip1.Invoke(New _Error_delegate(AddressOf _Error), New Object() {_Title, _details})
        Else
            ErrorDescription = _details
            statusLabel.Text = DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss ") + _Title            'specify error date and time
            'Print exception to console
            Console.WriteLine(_Title)
            Console.WriteLine(_details)
            Try
                'write or append
                If File.Exists(ErrorFile) Then
                    Using sw As New StreamWriter(ErrorFile, FileMode.Open)
                        sw.WriteLine(DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss --") + _Title + vbCrLf + _details + vbCrLf)
                    End Using
                Else
                    Using sw As New StreamWriter(File.Open(ErrorFile, FileMode.Create))
                        sw.WriteLine(DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss --") + _Title + vbCrLf + _details + vbCrLf)
                    End Using
                End If
                'Flash red statusbar
                statusLabel.ForeColor = System.Drawing.Color.White
                StatusStrip1.BackColor = System.Drawing.Color.Firebrick
                ErrorFlashTimer.Start()
            Catch ex As Exception
                'Error on error
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub

    'Fired once to return from a flashed statuslabel
    Private Sub ErrorFlashTimer_Tick(sender As Object, e As EventArgs) Handles ErrorFlashTimer.Tick
        ErrorFlashTimer.Enabled = False 'disable
        StatusStrip1.BackColor = SystemColors.Control
        statusLabel.ForeColor = System.Drawing.Color.Black
    End Sub

    'By doubleclicking the statuslabel after an error, the error description is shown in a messagebox
    Private Sub statusLabel_DoubleClick(sender As Object, e As EventArgs) Handles statusLabel.DoubleClick
        If ErrorDescription <> "" Then MsgBox(ErrorDescription)
    End Sub

    Private Sub Form1_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        If (e.Button = MouseButtons.XButton1) Then
            Console.WriteLine("XButton1")
        ElseIf (e.Button = MouseButtons.XButton2) Then
            Console.WriteLine("XButton2")
        End If
    End Sub

#End Region

#Region "Closing"

    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        Dim hasChanges As Boolean = False
        For Each vid In VidList
            If (vid.hasChanges) Then hasChanges = True : Exit For
        Next

        If (hasChanges) Then
            Me.Cursor = Cursors.WaitCursor
            'save before closing
            save_indexFile()
            Me.Cursor = Cursors.Default
        End If

        If (bw.IsBusy) Then
            'cancel backgroundprogress
            bw.CancelAsync()
        End If

        If (bw_AI.IsBusy) Then
            'cancel backgroundprogress
            bw_AI.CancelAsync()
        End If

    End Sub

#End Region

End Class
