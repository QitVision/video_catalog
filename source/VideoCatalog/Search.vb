﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

''' <summary>
''' Search module includes search sub-routines
''' Usage: Call GetMatchingVideos to search from a list of VideoFile
''' </summary>
Module Search

    ''' <summary>
    ''' Returns a list of video indexes that match the search query criteria
    ''' </summary>
    ''' <param name="vids">List of VideoFile to be searched from</param>
    ''' <param name="active_indexes">Indexes of the vids that should be included in the search</param>
    ''' <param name="searchStr">Search string</param>
    ''' <param name="duration_filter_under">Optional. Remove search results that are over this many seconds (-1 means this filter is not active)</param>
    ''' <param name="duration_filter_over">Optional. Remove search results that are under this many seconds (-1 means this filter is not active)</param>
    ''' <param name="rating_filter_under">Optional. Remove search results that are over this many stars (-1 means this filter is not active). Range is from 0 to 50.</param>
    ''' <param name="rating_filter_over">Optional. Remove search results that are under this many stars (-1 means this filter is not active). Range is from 0 to 50.</param>
    ''' <param name="sortBy">Optional. Default = "relevance". Sort by. Accepts 'rating', 'length' or 'date_created'</param>
    ''' <param name="sortOrder">Optional. Default = True. False for inverse return order</param>
    ''' <returns></returns>
    Public Function GetMatchingVideos(ByRef vids As List(Of VideoFile), ByRef active_indexes As List(Of Integer), ByVal searchStr As String, Optional ByVal duration_filter_under As Integer = Integer.MaxValue, Optional ByVal duration_filter_over As Integer = Integer.MinValue, Optional ByVal rating_filter_under As Integer = Integer.MaxValue, Optional ByVal rating_filter_over As Integer = Integer.MinValue, Optional ByVal sortBy As String = "relevance", Optional ByVal sortOrder As Boolean = True) As List(Of Integer)
        Dim result_list As New List(Of Integer)

        'don't search anything less than 2 characters
        If (searchStr.Length < 2) Then
            'add all, but order randomly
            For Each i In active_indexes
                result_list.Add(i)
            Next
        Else
            If (vids.Count > 0) Then searchForString(result_list, vids, searchStr)
        End If

        'add other filters and sorting if active
        If (result_list.Count > 0) Then

            Dim vidsCopy As List(Of VideoFile) = New List(Of VideoFile)(vids)

            'remove videos that are over limit
            result_list = result_list.Where(Function(x) vidsCopy(x).getLength <= duration_filter_under).ToList()
            'remove videos that are under limit
            result_list = result_list.Where(Function(x) vidsCopy(x).getLength >= duration_filter_over).ToList()

            'remove videos that are over limit
            result_list = result_list.Where(Function(x) vidsCopy(x).getRating <= rating_filter_under).ToList()
            'remove videos that are under limit
            result_list = result_list.Where(Function(x) vidsCopy(x).getRating >= rating_filter_over).ToList()

            'additional sorting
            If (sortBy.Length > 0 And sortBy <> "relevance") Then
                'create a holder for comparisonlist, could be filled with long or integer but they both convert to long without loss
                'Dim comparisonList As New List(Of Long)
                Dim ordered_result_List As IOrderedEnumerable(Of Integer) = Nothing

                Dim videoSorter As New VideoSorter()
                ordered_result_List = CallByName(videoSorter, sortBy, CallType.Get, {result_list, vidsCopy})

                'Failsafe
                'If (comparisonList.Count > 0) Then
                If (ordered_result_List IsNot Nothing) Then
                    'return sorted list in ascending or descending order
                    If (sortOrder) Then
                        Return ordered_result_List.Reverse().ToList()
                    Else
                        Return ordered_result_List.ToList()
                    End If

                End If

            End If

        End If

        Return result_list
    End Function

    ''' <summary>
    ''' Sub-routine for finding string matches from a list of VideoFile
    ''' </summary>
    ''' <param name="result_list">A reference of a result list that will be populated</param>
    ''' <param name="vids">List of VideoFile that is searched</param>
    ''' <param name="searchStr">Search string</param>
    Private Sub searchForString(ByRef result_list As List(Of Integer), ByRef vids As List(Of VideoFile), ByRef searchStr As String)
        'Separate lists for different tags for priorizing
        Dim user_tag_list As New List(Of Integer)
        Dim path_tag_list As New List(Of Integer)
        Dim ai_tag_list As New List(Of Integer)

        'split search string if it has multiple words
        Dim search_parts As String() = searchStr.ToLower.Split(New Char() {" "c}, StringSplitOptions.RemoveEmptyEntries)
        'add full search string to first
        Dim search_list As List(Of String) = search_parts.ToList()
        If (search_list.Count > 1) Then search_list.Insert(0, searchStr.ToLower)

        For Each searchWord In search_list
            'add all videos to result_list if some 
            For i As Integer = 0 To vids.Count - 1
                If (vids(i).getTags.Contains(searchWord, StringComparer.InvariantCultureIgnoreCase)) Then user_tag_list.Add(i)
                If (vids(i).getPathTags.Contains(searchWord, StringComparer.InvariantCultureIgnoreCase)) Then path_tag_list.Add(i)
                If (vids(i).getAITags.Contains(searchWord, StringComparer.InvariantCultureIgnoreCase)) Then ai_tag_list.Add(i)
            Next
        Next

        'partial matches
        For Each searchword In search_parts
            'add all videos to result_list if some 
            For i As Integer = 0 To vids.Count - 1
                If (vids(i).getTags.Where(Function(x) x.ToLower.Contains(searchword)).Count > 0) Then user_tag_list.Add(i)
                If (vids(i).getPathTags.Where(Function(x) x.ToLower.Contains(searchword)).Count > 0) Then user_tag_list.Add(i)
                If (vids(i).getAITags.Where(Function(x) x.ToLower.Contains(searchword)).Count > 0) Then user_tag_list.Add(i)
            Next
        Next

        'combine in order and remove duplicates
        result_list.AddRange(user_tag_list)
        result_list.AddRange(path_tag_list)
        result_list.AddRange(ai_tag_list)
        result_list = result_list.Distinct().ToList

    End Sub

End Module
