﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetRoots
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SetRoots))
        Me.rootListBox = New System.Windows.Forms.ListBox()
        Me.AddBtn = New System.Windows.Forms.Button()
        Me.RemoveBtn = New System.Windows.Forms.Button()
        Me.okbtn = New System.Windows.Forms.Button()
        Me.cancelbtn = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.SuspendLayout()
        '
        'rootListBox
        '
        Me.rootListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rootListBox.FormattingEnabled = True
        Me.rootListBox.HorizontalScrollbar = True
        Me.rootListBox.Location = New System.Drawing.Point(12, 12)
        Me.rootListBox.Name = "rootListBox"
        Me.rootListBox.ScrollAlwaysVisible = True
        Me.rootListBox.Size = New System.Drawing.Size(701, 160)
        Me.rootListBox.TabIndex = 0
        '
        'AddBtn
        '
        Me.AddBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AddBtn.Location = New System.Drawing.Point(12, 175)
        Me.AddBtn.Name = "AddBtn"
        Me.AddBtn.Size = New System.Drawing.Size(92, 40)
        Me.AddBtn.TabIndex = 1
        Me.AddBtn.Text = "Add"
        Me.AddBtn.UseVisualStyleBackColor = True
        '
        'RemoveBtn
        '
        Me.RemoveBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.RemoveBtn.Location = New System.Drawing.Point(110, 175)
        Me.RemoveBtn.Name = "RemoveBtn"
        Me.RemoveBtn.Size = New System.Drawing.Size(92, 40)
        Me.RemoveBtn.TabIndex = 2
        Me.RemoveBtn.Text = "Remove"
        Me.RemoveBtn.UseVisualStyleBackColor = True
        '
        'okbtn
        '
        Me.okbtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.okbtn.Location = New System.Drawing.Point(621, 178)
        Me.okbtn.Name = "okbtn"
        Me.okbtn.Size = New System.Drawing.Size(92, 40)
        Me.okbtn.TabIndex = 3
        Me.okbtn.Text = "OK"
        Me.okbtn.UseVisualStyleBackColor = True
        '
        'cancelbtn
        '
        Me.cancelbtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cancelbtn.Location = New System.Drawing.Point(523, 178)
        Me.cancelbtn.Name = "cancelbtn"
        Me.cancelbtn.Size = New System.Drawing.Size(92, 40)
        Me.cancelbtn.TabIndex = 4
        Me.cancelbtn.Text = "Cancel"
        Me.cancelbtn.UseVisualStyleBackColor = True
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.Description = "Select root folder that contains videos"
        '
        'SetRoots
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 227)
        Me.Controls.Add(Me.cancelbtn)
        Me.Controls.Add(Me.okbtn)
        Me.Controls.Add(Me.RemoveBtn)
        Me.Controls.Add(Me.AddBtn)
        Me.Controls.Add(Me.rootListBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SetRoots"
        Me.ShowInTaskbar = False
        Me.Text = "Video library folders"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents rootListBox As ListBox
    Friend WithEvents AddBtn As Button
    Friend WithEvents RemoveBtn As Button
    Friend WithEvents okbtn As Button
    Friend WithEvents cancelbtn As Button
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
End Class
