﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TagMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CloseTimer = New System.Windows.Forms.Timer(Me.components)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TagBox = New System.Windows.Forms.TextBox()
        Me.tagList = New System.Windows.Forms.ListBox()
        Me.ai_tags_txt = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CloseTimer
        '
        Me.CloseTimer.Interval = 300
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.White
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Controls.Add(Me.TagBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.tagList)
        Me.FlowLayoutPanel1.Controls.Add(Me.ai_tags_txt)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(162, 189)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'TagBox
        '
        Me.TagBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TagBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.TagBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.TagBox.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TagBox.Location = New System.Drawing.Point(3, 3)
        Me.TagBox.MinimumSize = New System.Drawing.Size(150, 4)
        Me.TagBox.Name = "TagBox"
        Me.TagBox.Size = New System.Drawing.Size(150, 27)
        Me.TagBox.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.TagBox, "Press enter to add a new tag")
        '
        'tagList
        '
        Me.tagList.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.tagList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tagList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tagList.ForeColor = System.Drawing.Color.Navy
        Me.tagList.FormattingEnabled = True
        Me.tagList.ItemHeight = 20
        Me.tagList.Location = New System.Drawing.Point(3, 36)
        Me.tagList.MinimumSize = New System.Drawing.Size(150, 90)
        Me.tagList.Name = "tagList"
        Me.tagList.Size = New System.Drawing.Size(150, 96)
        Me.tagList.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.tagList, "Video search tags. Remove tags with delete button.")
        '
        'ai_tags_txt
        '
        Me.ai_tags_txt.AutoSize = True
        Me.ai_tags_txt.BackColor = System.Drawing.Color.Lavender
        Me.ai_tags_txt.ForeColor = System.Drawing.Color.Black
        Me.ai_tags_txt.Location = New System.Drawing.Point(3, 135)
        Me.ai_tags_txt.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.ai_tags_txt.MaximumSize = New System.Drawing.Size(150, 0)
        Me.ai_tags_txt.MinimumSize = New System.Drawing.Size(150, 0)
        Me.ai_tags_txt.Name = "ai_tags_txt"
        Me.ai_tags_txt.Size = New System.Drawing.Size(150, 20)
        Me.ai_tags_txt.TabIndex = 2
        Me.ai_tags_txt.Text = "[No AI tags]"
        Me.ToolTip1.SetToolTip(Me.ai_tags_txt, "AI generated tags based on the screenshot contents")
        '
        'TagMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(162, 189)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "TagMenu"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "TagMenu"
        Me.TopMost = True
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CloseTimer As Timer
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents TagBox As TextBox
    Friend WithEvents tagList As ListBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ai_tags_txt As Label
End Class
