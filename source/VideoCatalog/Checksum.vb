﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This program Is free software: you can redistribute it And/Or modify
'it under the terms Of the GNU General Public License As published by
'the Free Software Foundation, either version 3 Of the License, Or
'(at your option) any later version.

'This program Is distributed In the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty Of
'MERCHANTABILITY Or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License For more details.

'You should have received a copy Of the GNU General Public License
'along with this program.  If Not, see < http: //www.gnu.org/licenses/>.

Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Module Checksum

    Public Function getFingerprint(ByVal path As String) As String
        Try
            Dim hash As Byte()
            Using md5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider

                'read only first 1,000,000 bytes to save time, increases collision risk but saves a ton of time with large video files
                Dim _totalsize As Long = New FileInfo(path).Length
                If (_totalsize > 1000000) Then _totalsize = 1000000
                Dim buffer() As Byte = New Byte(_totalsize - 1) {}
                Using fs As New FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None)
                    fs.Read(buffer, 0, buffer.Length)
                End Using
                md5.ComputeHash(buffer)
                hash = md5.Hash
            End Using
            Return ByteArrayToString(hash)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function ByteArrayToString(ByVal arrInput() As Byte) As String
        Dim i As Integer
        Dim sOutput As New StringBuilder(arrInput.Length)
        For i = 0 To arrInput.Length - 1
            sOutput.Append(arrInput(i).ToString("X2"))
        Next
        Return sOutput.ToString()
    End Function

End Module
