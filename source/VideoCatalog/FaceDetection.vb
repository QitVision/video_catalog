﻿Imports System.IO
Imports OpenCV.Net

Public Class FaceDetection

    Dim cascadeFileFace As String = "haarcascade_frontalface_default.xml"
    Dim cascadeFileBody As String = "haarcascade_upperbody.xml"
    Dim haarCascadeFace As HaarClassifierCascade
    Dim haarCascadeBody As HaarClassifierCascade

    Public Sub New(ByVal modelDir As String)

        'write face cascade file if it doesn't exist already
        If (Not File.Exists(modelDir + cascadeFileFace)) Then
            File.WriteAllBytes(modelDir + cascadeFileFace, My.Resources.haarcascade_frontalface_default)
        End If

        'write full body cascade file if it doesn't exist already
        If (Not File.Exists(modelDir + cascadeFileBody)) Then
            File.WriteAllBytes(modelDir + cascadeFileBody, My.Resources.haarcascade_upperbody)
        End If

        'initialize haar cascades
        haarCascadeFace = HaarClassifierCascade.Load(modelDir + cascadeFileFace)
        haarCascadeBody = HaarClassifierCascade.Load(modelDir + cascadeFileBody)

    End Sub

    Public Function DetectFaceValue(ByVal imagePath As String) As Single
        Try
            'load image as a stream
            Dim imgM As Mat = OpenCV.Net.CV.LoadImageM(imagePath, LoadImageFlags.AnyColor)
            Dim storage As MemStorage = New MemStorage()
            Dim faces = haarCascadeFace.DetectObjects(imgM, storage, 1.1, 3, HaarDetectObjectFlags.None, Nothing, Nothing)
            Dim bodies = haarCascadeBody.DetectObjects(imgM, storage, 1.1, 3, HaarDetectObjectFlags.None, Nothing, Nothing)

            Return faces.Count + bodies.Count
        Catch ex As Exception
            Return 0
        End Try
    End Function

End Class
