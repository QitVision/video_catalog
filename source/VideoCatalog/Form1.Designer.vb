﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MainTabControl = New System.Windows.Forms.TabControl()
        Me.MainTabVideos = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanelVideos = New System.Windows.Forms.TableLayoutPanel()
        Me.VideoFlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
        Me.NavigatorTable = New System.Windows.Forms.TableLayoutPanel()
        Me.prev_btn = New System.Windows.Forms.PictureBox()
        Me.navigator_lbl = New System.Windows.Forms.Label()
        Me.next_btn = New System.Windows.Forms.PictureBox()
        Me.videoCountlbl = New System.Windows.Forms.Label()
        Me.NavigatorTextBox = New System.Windows.Forms.TextBox()
        Me.SearchTableLayout = New System.Windows.Forms.TableLayoutPanel()
        Me.AdvancedSearchPanel = New System.Windows.Forms.FlowLayoutPanel()
        Me.DurationFilterGroup = New System.Windows.Forms.GroupBox()
        Me.durationFilterLbl = New System.Windows.Forms.Label()
        Me.DurationFilterTrackbar = New System.Windows.Forms.TrackBar()
        Me.DurationFilterRadio_under = New System.Windows.Forms.RadioButton()
        Me.DurationFilterRadio_over = New System.Windows.Forms.RadioButton()
        Me.DurationFilterRadio_none = New System.Windows.Forms.RadioButton()
        Me.RatingFilterGroup = New System.Windows.Forms.GroupBox()
        Me.RatingFilterRadio_notrated = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ratingFilterNumeric = New System.Windows.Forms.NumericUpDown()
        Me.RatingFilterRadio_under = New System.Windows.Forms.RadioButton()
        Me.RatingFilterRadio_over = New System.Windows.Forms.RadioButton()
        Me.RatingFilterRadio_none = New System.Windows.Forms.RadioButton()
        Me.SortBox = New System.Windows.Forms.GroupBox()
        Me.SortByRandomCb = New System.Windows.Forms.RadioButton()
        Me.SortByRatingCb = New System.Windows.Forms.RadioButton()
        Me.SortOrderCb = New System.Windows.Forms.CheckBox()
        Me.SortByLengthCb = New System.Windows.Forms.RadioButton()
        Me.SortByDateCb = New System.Windows.Forms.RadioButton()
        Me.SortByRelevanceCb = New System.Windows.Forms.RadioButton()
        Me.SearchTextBox = New System.Windows.Forms.TextBox()
        Me.AdvancedSearchBtn = New System.Windows.Forms.Button()
        Me.MainTabSettings = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanelSettings = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBoxDirectories = New System.Windows.Forms.GroupBox()
        Me.Fix_previews_btn = New System.Windows.Forms.Button()
        Me.Regenerate_AI_tags_btn = New System.Windows.Forms.Button()
        Me.preview_screen_lbl = New System.Windows.Forms.Label()
        Me.btn_refresh = New System.Windows.Forms.Button()
        Me.btn_select_folders = New System.Windows.Forms.Button()
        Me.ElementHost1 = New System.Windows.Forms.Integration.ElementHost()
        Me.GroupBoxGeneral = New System.Windows.Forms.GroupBox()
        Me.TagMenuCb = New System.Windows.Forms.CheckBox()
        Me.GroupBoxPreviewSpeed = New System.Windows.Forms.GroupBox()
        Me.PreviewSpeedRadio_veryfast = New System.Windows.Forms.RadioButton()
        Me.PreviewSpeedRadio_fast = New System.Windows.Forms.RadioButton()
        Me.PreviewSpeedRadio_default = New System.Windows.Forms.RadioButton()
        Me.PreviewSpeedRadio_slow = New System.Windows.Forms.RadioButton()
        Me.ShowRatingCb = New System.Windows.Forms.CheckBox()
        Me.GeneralSettingsApplyBtn = New System.Windows.Forms.Button()
        Me.GroupBoxVideoSize = New System.Windows.Forms.GroupBox()
        Me.PreviewSizeRadio_Verylarge = New System.Windows.Forms.RadioButton()
        Me.PreviewSizeRadio_Large = New System.Windows.Forms.RadioButton()
        Me.PreviewSizeRadio_Medium = New System.Windows.Forms.RadioButton()
        Me.PreviewSizeRadio_Small = New System.Windows.Forms.RadioButton()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MaxVisibleVideosNumeric = New System.Windows.Forms.NumericUpDown()
        Me.AboutBox = New System.Windows.Forms.GroupBox()
        Me.Licenselbl = New System.Windows.Forms.LinkLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Versionlbl = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.statusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.SpringStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.CancelLoadingBtn = New System.Windows.Forms.ToolStripDropDownButton()
        Me.BottomProgressBar = New System.Windows.Forms.ToolStripProgressBar()
        Me.ErrorFlashTimer = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainTabControl.SuspendLayout()
        Me.MainTabVideos.SuspendLayout()
        Me.TableLayoutPanelVideos.SuspendLayout()
        Me.NavigatorTable.SuspendLayout()
        CType(Me.prev_btn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.next_btn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SearchTableLayout.SuspendLayout()
        Me.AdvancedSearchPanel.SuspendLayout()
        Me.DurationFilterGroup.SuspendLayout()
        CType(Me.DurationFilterTrackbar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RatingFilterGroup.SuspendLayout()
        CType(Me.ratingFilterNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SortBox.SuspendLayout()
        Me.MainTabSettings.SuspendLayout()
        Me.FlowLayoutPanelSettings.SuspendLayout()
        Me.GroupBoxDirectories.SuspendLayout()
        Me.GroupBoxGeneral.SuspendLayout()
        Me.GroupBoxPreviewSpeed.SuspendLayout()
        Me.GroupBoxVideoSize.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.MaxVisibleVideosNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.AboutBox.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainTabControl
        '
        Me.MainTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainTabControl.Controls.Add(Me.MainTabVideos)
        Me.MainTabControl.Controls.Add(Me.MainTabSettings)
        Me.MainTabControl.Location = New System.Drawing.Point(0, 2)
        Me.MainTabControl.Name = "MainTabControl"
        Me.MainTabControl.Padding = New System.Drawing.Point(0, 0)
        Me.MainTabControl.SelectedIndex = 0
        Me.MainTabControl.Size = New System.Drawing.Size(1084, 734)
        Me.MainTabControl.TabIndex = 3
        '
        'MainTabVideos
        '
        Me.MainTabVideos.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.MainTabVideos.Controls.Add(Me.TableLayoutPanelVideos)
        Me.MainTabVideos.Location = New System.Drawing.Point(4, 29)
        Me.MainTabVideos.Name = "MainTabVideos"
        Me.MainTabVideos.Padding = New System.Windows.Forms.Padding(3)
        Me.MainTabVideos.Size = New System.Drawing.Size(1076, 701)
        Me.MainTabVideos.TabIndex = 0
        Me.MainTabVideos.Text = "Library"
        '
        'TableLayoutPanelVideos
        '
        Me.TableLayoutPanelVideos.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TableLayoutPanelVideos.ColumnCount = 1
        Me.TableLayoutPanelVideos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanelVideos.Controls.Add(Me.VideoFlowLayoutPanel, 0, 1)
        Me.TableLayoutPanelVideos.Controls.Add(Me.NavigatorTable, 0, 2)
        Me.TableLayoutPanelVideos.Controls.Add(Me.SearchTableLayout, 0, 0)
        Me.TableLayoutPanelVideos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanelVideos.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanelVideos.Name = "TableLayoutPanelVideos"
        Me.TableLayoutPanelVideos.RowCount = 3
        Me.TableLayoutPanelVideos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanelVideos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanelVideos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanelVideos.Size = New System.Drawing.Size(1070, 695)
        Me.TableLayoutPanelVideos.TabIndex = 1
        '
        'VideoFlowLayoutPanel
        '
        Me.VideoFlowLayoutPanel.AutoScroll = True
        Me.VideoFlowLayoutPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.VideoFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VideoFlowLayoutPanel.Location = New System.Drawing.Point(3, 208)
        Me.VideoFlowLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.VideoFlowLayoutPanel.Name = "VideoFlowLayoutPanel"
        Me.VideoFlowLayoutPanel.Size = New System.Drawing.Size(1064, 437)
        Me.VideoFlowLayoutPanel.TabIndex = 0
        '
        'NavigatorTable
        '
        Me.NavigatorTable.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.NavigatorTable.ColumnCount = 5
        Me.NavigatorTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.NavigatorTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.NavigatorTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.NavigatorTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.NavigatorTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.NavigatorTable.Controls.Add(Me.prev_btn, 0, 0)
        Me.NavigatorTable.Controls.Add(Me.navigator_lbl, 2, 0)
        Me.NavigatorTable.Controls.Add(Me.next_btn, 3, 0)
        Me.NavigatorTable.Controls.Add(Me.videoCountlbl, 4, 0)
        Me.NavigatorTable.Controls.Add(Me.NavigatorTextBox, 1, 0)
        Me.NavigatorTable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigatorTable.ForeColor = System.Drawing.Color.Black
        Me.NavigatorTable.Location = New System.Drawing.Point(3, 645)
        Me.NavigatorTable.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.NavigatorTable.Name = "NavigatorTable"
        Me.NavigatorTable.RowCount = 1
        Me.NavigatorTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.NavigatorTable.Size = New System.Drawing.Size(1064, 47)
        Me.NavigatorTable.TabIndex = 1
        '
        'prev_btn
        '
        Me.prev_btn.Dock = System.Windows.Forms.DockStyle.Right
        Me.prev_btn.Image = Global.VideoCatalog.My.Resources.Resources.prev_n
        Me.prev_btn.Location = New System.Drawing.Point(448, 5)
        Me.prev_btn.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.prev_btn.Name = "prev_btn"
        Me.prev_btn.Size = New System.Drawing.Size(34, 37)
        Me.prev_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.prev_btn.TabIndex = 2
        Me.prev_btn.TabStop = False
        '
        'navigator_lbl
        '
        Me.navigator_lbl.AutoSize = True
        Me.navigator_lbl.Dock = System.Windows.Forms.DockStyle.Left
        Me.navigator_lbl.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.navigator_lbl.Location = New System.Drawing.Point(525, 0)
        Me.navigator_lbl.Name = "navigator_lbl"
        Me.navigator_lbl.Size = New System.Drawing.Size(0, 47)
        Me.navigator_lbl.TabIndex = 0
        Me.navigator_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'next_btn
        '
        Me.next_btn.Dock = System.Windows.Forms.DockStyle.Left
        Me.next_btn.Image = Global.VideoCatalog.My.Resources.Resources.next_n
        Me.next_btn.Location = New System.Drawing.Point(582, 5)
        Me.next_btn.Margin = New System.Windows.Forms.Padding(0, 5, 0, 5)
        Me.next_btn.Name = "next_btn"
        Me.next_btn.Size = New System.Drawing.Size(34, 37)
        Me.next_btn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.next_btn.TabIndex = 1
        Me.next_btn.TabStop = False
        '
        'videoCountlbl
        '
        Me.videoCountlbl.AutoSize = True
        Me.videoCountlbl.Dock = System.Windows.Forms.DockStyle.Right
        Me.videoCountlbl.Location = New System.Drawing.Point(1061, 0)
        Me.videoCountlbl.Name = "videoCountlbl"
        Me.videoCountlbl.Size = New System.Drawing.Size(0, 47)
        Me.videoCountlbl.TabIndex = 3
        Me.videoCountlbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NavigatorTextBox
        '
        Me.NavigatorTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.NavigatorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NavigatorTextBox.Dock = System.Windows.Forms.DockStyle.Right
        Me.NavigatorTextBox.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NavigatorTextBox.ForeColor = System.Drawing.Color.Black
        Me.NavigatorTextBox.Location = New System.Drawing.Point(488, 12)
        Me.NavigatorTextBox.Margin = New System.Windows.Forms.Padding(3, 12, 0, 3)
        Me.NavigatorTextBox.Name = "NavigatorTextBox"
        Me.NavigatorTextBox.Size = New System.Drawing.Size(34, 20)
        Me.NavigatorTextBox.TabIndex = 4
        Me.NavigatorTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.NavigatorTextBox, "Current page. Change pages with arrows or enter page number here.")
        '
        'SearchTableLayout
        '
        Me.SearchTableLayout.AutoSize = True
        Me.SearchTableLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SearchTableLayout.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.SearchTableLayout.ColumnCount = 2
        Me.SearchTableLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.SearchTableLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.SearchTableLayout.Controls.Add(Me.AdvancedSearchPanel, 0, 1)
        Me.SearchTableLayout.Controls.Add(Me.SearchTextBox, 0, 0)
        Me.SearchTableLayout.Controls.Add(Me.AdvancedSearchBtn, 1, 0)
        Me.SearchTableLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SearchTableLayout.Location = New System.Drawing.Point(3, 3)
        Me.SearchTableLayout.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.SearchTableLayout.Name = "SearchTableLayout"
        Me.SearchTableLayout.RowCount = 2
        Me.SearchTableLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.SearchTableLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.SearchTableLayout.Size = New System.Drawing.Size(1064, 205)
        Me.SearchTableLayout.TabIndex = 2
        '
        'AdvancedSearchPanel
        '
        Me.AdvancedSearchPanel.AutoScroll = True
        Me.AdvancedSearchPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.AdvancedSearchPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.SearchTableLayout.SetColumnSpan(Me.AdvancedSearchPanel, 2)
        Me.AdvancedSearchPanel.Controls.Add(Me.DurationFilterGroup)
        Me.AdvancedSearchPanel.Controls.Add(Me.RatingFilterGroup)
        Me.AdvancedSearchPanel.Controls.Add(Me.SortBox)
        Me.AdvancedSearchPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AdvancedSearchPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.AdvancedSearchPanel.Location = New System.Drawing.Point(3, 50)
        Me.AdvancedSearchPanel.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.AdvancedSearchPanel.Name = "AdvancedSearchPanel"
        Me.AdvancedSearchPanel.Size = New System.Drawing.Size(1058, 155)
        Me.AdvancedSearchPanel.TabIndex = 0
        '
        'DurationFilterGroup
        '
        Me.DurationFilterGroup.Controls.Add(Me.durationFilterLbl)
        Me.DurationFilterGroup.Controls.Add(Me.DurationFilterTrackbar)
        Me.DurationFilterGroup.Controls.Add(Me.DurationFilterRadio_under)
        Me.DurationFilterGroup.Controls.Add(Me.DurationFilterRadio_over)
        Me.DurationFilterGroup.Controls.Add(Me.DurationFilterRadio_none)
        Me.DurationFilterGroup.Location = New System.Drawing.Point(3, 3)
        Me.DurationFilterGroup.Name = "DurationFilterGroup"
        Me.DurationFilterGroup.Size = New System.Drawing.Size(682, 69)
        Me.DurationFilterGroup.TabIndex = 0
        Me.DurationFilterGroup.TabStop = False
        Me.DurationFilterGroup.Text = "Filter based on duration"
        '
        'durationFilterLbl
        '
        Me.durationFilterLbl.AutoSize = True
        Me.durationFilterLbl.Location = New System.Drawing.Point(534, 28)
        Me.durationFilterLbl.Name = "durationFilterLbl"
        Me.durationFilterLbl.Size = New System.Drawing.Size(128, 20)
        Me.durationFilterLbl.TabIndex = 4
        Me.durationFilterLbl.Text = "Duration: 00:01:00"
        '
        'DurationFilterTrackbar
        '
        Me.DurationFilterTrackbar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.DurationFilterTrackbar.Enabled = False
        Me.DurationFilterTrackbar.LargeChange = 30
        Me.DurationFilterTrackbar.Location = New System.Drawing.Point(212, 21)
        Me.DurationFilterTrackbar.Maximum = 7200
        Me.DurationFilterTrackbar.Name = "DurationFilterTrackbar"
        Me.DurationFilterTrackbar.Size = New System.Drawing.Size(316, 45)
        Me.DurationFilterTrackbar.SmallChange = 5
        Me.DurationFilterTrackbar.TabIndex = 3
        Me.DurationFilterTrackbar.TickFrequency = 600
        Me.DurationFilterTrackbar.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.ToolTip1.SetToolTip(Me.DurationFilterTrackbar, "Change duration")
        Me.DurationFilterTrackbar.Value = 60
        '
        'DurationFilterRadio_under
        '
        Me.DurationFilterRadio_under.AutoSize = True
        Me.DurationFilterRadio_under.Location = New System.Drawing.Point(139, 26)
        Me.DurationFilterRadio_under.Name = "DurationFilterRadio_under"
        Me.DurationFilterRadio_under.Size = New System.Drawing.Size(67, 24)
        Me.DurationFilterRadio_under.TabIndex = 2
        Me.DurationFilterRadio_under.Text = "Under"
        Me.DurationFilterRadio_under.UseVisualStyleBackColor = True
        '
        'DurationFilterRadio_over
        '
        Me.DurationFilterRadio_over.AutoSize = True
        Me.DurationFilterRadio_over.Location = New System.Drawing.Point(75, 26)
        Me.DurationFilterRadio_over.Name = "DurationFilterRadio_over"
        Me.DurationFilterRadio_over.Size = New System.Drawing.Size(58, 24)
        Me.DurationFilterRadio_over.TabIndex = 1
        Me.DurationFilterRadio_over.Text = "Over"
        Me.DurationFilterRadio_over.UseVisualStyleBackColor = True
        '
        'DurationFilterRadio_none
        '
        Me.DurationFilterRadio_none.AutoSize = True
        Me.DurationFilterRadio_none.Checked = True
        Me.DurationFilterRadio_none.Location = New System.Drawing.Point(6, 26)
        Me.DurationFilterRadio_none.Name = "DurationFilterRadio_none"
        Me.DurationFilterRadio_none.Size = New System.Drawing.Size(63, 24)
        Me.DurationFilterRadio_none.TabIndex = 0
        Me.DurationFilterRadio_none.TabStop = True
        Me.DurationFilterRadio_none.Text = "None"
        Me.DurationFilterRadio_none.UseVisualStyleBackColor = True
        '
        'RatingFilterGroup
        '
        Me.RatingFilterGroup.Controls.Add(Me.RatingFilterRadio_notrated)
        Me.RatingFilterGroup.Controls.Add(Me.Label3)
        Me.RatingFilterGroup.Controls.Add(Me.ratingFilterNumeric)
        Me.RatingFilterGroup.Controls.Add(Me.RatingFilterRadio_under)
        Me.RatingFilterGroup.Controls.Add(Me.RatingFilterRadio_over)
        Me.RatingFilterGroup.Controls.Add(Me.RatingFilterRadio_none)
        Me.RatingFilterGroup.Location = New System.Drawing.Point(3, 78)
        Me.RatingFilterGroup.Name = "RatingFilterGroup"
        Me.RatingFilterGroup.Size = New System.Drawing.Size(682, 69)
        Me.RatingFilterGroup.TabIndex = 2
        Me.RatingFilterGroup.TabStop = False
        Me.RatingFilterGroup.Text = "Filter based on rating"
        '
        'RatingFilterRadio_notrated
        '
        Me.RatingFilterRadio_notrated.AutoSize = True
        Me.RatingFilterRadio_notrated.Location = New System.Drawing.Point(571, 26)
        Me.RatingFilterRadio_notrated.Name = "RatingFilterRadio_notrated"
        Me.RatingFilterRadio_notrated.Size = New System.Drawing.Size(91, 24)
        Me.RatingFilterRadio_notrated.TabIndex = 6
        Me.RatingFilterRadio_notrated.Text = "Not rated"
        Me.RatingFilterRadio_notrated.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(273, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Stars"
        '
        'ratingFilterNumeric
        '
        Me.ratingFilterNumeric.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ratingFilterNumeric.DecimalPlaces = 1
        Me.ratingFilterNumeric.Enabled = False
        Me.ratingFilterNumeric.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.ratingFilterNumeric.Location = New System.Drawing.Point(212, 26)
        Me.ratingFilterNumeric.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.ratingFilterNumeric.Name = "ratingFilterNumeric"
        Me.ratingFilterNumeric.Size = New System.Drawing.Size(55, 27)
        Me.ratingFilterNumeric.TabIndex = 3
        '
        'RatingFilterRadio_under
        '
        Me.RatingFilterRadio_under.AutoSize = True
        Me.RatingFilterRadio_under.Location = New System.Drawing.Point(139, 26)
        Me.RatingFilterRadio_under.Name = "RatingFilterRadio_under"
        Me.RatingFilterRadio_under.Size = New System.Drawing.Size(67, 24)
        Me.RatingFilterRadio_under.TabIndex = 2
        Me.RatingFilterRadio_under.Text = "Under"
        Me.RatingFilterRadio_under.UseVisualStyleBackColor = True
        '
        'RatingFilterRadio_over
        '
        Me.RatingFilterRadio_over.AutoSize = True
        Me.RatingFilterRadio_over.Location = New System.Drawing.Point(75, 26)
        Me.RatingFilterRadio_over.Name = "RatingFilterRadio_over"
        Me.RatingFilterRadio_over.Size = New System.Drawing.Size(58, 24)
        Me.RatingFilterRadio_over.TabIndex = 1
        Me.RatingFilterRadio_over.Text = "Over"
        Me.RatingFilterRadio_over.UseVisualStyleBackColor = True
        '
        'RatingFilterRadio_none
        '
        Me.RatingFilterRadio_none.AutoSize = True
        Me.RatingFilterRadio_none.Checked = True
        Me.RatingFilterRadio_none.Location = New System.Drawing.Point(6, 26)
        Me.RatingFilterRadio_none.Name = "RatingFilterRadio_none"
        Me.RatingFilterRadio_none.Size = New System.Drawing.Size(63, 24)
        Me.RatingFilterRadio_none.TabIndex = 0
        Me.RatingFilterRadio_none.TabStop = True
        Me.RatingFilterRadio_none.Text = "None"
        Me.RatingFilterRadio_none.UseVisualStyleBackColor = True
        '
        'SortBox
        '
        Me.SortBox.Controls.Add(Me.SortByRandomCb)
        Me.SortBox.Controls.Add(Me.SortByRatingCb)
        Me.SortBox.Controls.Add(Me.SortOrderCb)
        Me.SortBox.Controls.Add(Me.SortByLengthCb)
        Me.SortBox.Controls.Add(Me.SortByDateCb)
        Me.SortBox.Controls.Add(Me.SortByRelevanceCb)
        Me.SortBox.Location = New System.Drawing.Point(691, 3)
        Me.SortBox.Name = "SortBox"
        Me.SortBox.Size = New System.Drawing.Size(306, 120)
        Me.SortBox.TabIndex = 1
        Me.SortBox.TabStop = False
        Me.SortBox.Text = "Sort by"
        '
        'SortByRandomCb
        '
        Me.SortByRandomCb.AutoSize = True
        Me.SortByRandomCb.Location = New System.Drawing.Point(82, 56)
        Me.SortByRandomCb.Name = "SortByRandomCb"
        Me.SortByRandomCb.Size = New System.Drawing.Size(83, 24)
        Me.SortByRandomCb.TabIndex = 5
        Me.SortByRandomCb.Text = "Random"
        Me.SortByRandomCb.UseVisualStyleBackColor = True
        '
        'SortByRatingCb
        '
        Me.SortByRatingCb.AutoSize = True
        Me.SortByRatingCb.Location = New System.Drawing.Point(6, 56)
        Me.SortByRatingCb.Name = "SortByRatingCb"
        Me.SortByRatingCb.Size = New System.Drawing.Size(70, 24)
        Me.SortByRatingCb.TabIndex = 4
        Me.SortByRatingCb.Text = "Rating"
        Me.SortByRatingCb.UseVisualStyleBackColor = True
        '
        'SortOrderCb
        '
        Me.SortOrderCb.AutoSize = True
        Me.SortOrderCb.Checked = True
        Me.SortOrderCb.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SortOrderCb.Location = New System.Drawing.Point(6, 86)
        Me.SortOrderCb.Name = "SortOrderCb"
        Me.SortOrderCb.Size = New System.Drawing.Size(164, 24)
        Me.SortOrderCb.TabIndex = 3
        Me.SortOrderCb.Text = "Toggle sorting order"
        Me.ToolTip1.SetToolTip(Me.SortOrderCb, "Flips the order." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Does nothing if sorted by relevance or random.")
        Me.SortOrderCb.UseVisualStyleBackColor = True
        '
        'SortByLengthCb
        '
        Me.SortByLengthCb.AutoSize = True
        Me.SortByLengthCb.Location = New System.Drawing.Point(225, 26)
        Me.SortByLengthCb.Name = "SortByLengthCb"
        Me.SortByLengthCb.Size = New System.Drawing.Size(72, 24)
        Me.SortByLengthCb.TabIndex = 2
        Me.SortByLengthCb.Text = "Length"
        Me.SortByLengthCb.UseVisualStyleBackColor = True
        '
        'SortByDateCb
        '
        Me.SortByDateCb.AutoSize = True
        Me.SortByDateCb.Location = New System.Drawing.Point(106, 26)
        Me.SortByDateCb.Name = "SortByDateCb"
        Me.SortByDateCb.Size = New System.Drawing.Size(113, 24)
        Me.SortByDateCb.TabIndex = 1
        Me.SortByDateCb.Text = "Date created"
        Me.SortByDateCb.UseVisualStyleBackColor = True
        '
        'SortByRelevanceCb
        '
        Me.SortByRelevanceCb.AutoSize = True
        Me.SortByRelevanceCb.Checked = True
        Me.SortByRelevanceCb.Location = New System.Drawing.Point(6, 26)
        Me.SortByRelevanceCb.Name = "SortByRelevanceCb"
        Me.SortByRelevanceCb.Size = New System.Drawing.Size(94, 24)
        Me.SortByRelevanceCb.TabIndex = 0
        Me.SortByRelevanceCb.TabStop = True
        Me.SortByRelevanceCb.Text = "Relevance"
        Me.SortByRelevanceCb.UseVisualStyleBackColor = True
        '
        'SearchTextBox
        '
        Me.SearchTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.SearchTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.SearchTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SearchTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SearchTextBox.Location = New System.Drawing.Point(10, 10)
        Me.SearchTextBox.Margin = New System.Windows.Forms.Padding(10)
        Me.SearchTextBox.Name = "SearchTextBox"
        Me.SearchTextBox.Size = New System.Drawing.Size(724, 27)
        Me.SearchTextBox.TabIndex = 2
        '
        'AdvancedSearchBtn
        '
        Me.AdvancedSearchBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AdvancedSearchBtn.BackColor = System.Drawing.Color.AliceBlue
        Me.AdvancedSearchBtn.Image = Global.VideoCatalog.My.Resources.Resources.menu
        Me.AdvancedSearchBtn.Location = New System.Drawing.Point(1025, 6)
        Me.AdvancedSearchBtn.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.AdvancedSearchBtn.Name = "AdvancedSearchBtn"
        Me.AdvancedSearchBtn.Padding = New System.Windows.Forms.Padding(3)
        Me.AdvancedSearchBtn.Size = New System.Drawing.Size(36, 36)
        Me.AdvancedSearchBtn.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.AdvancedSearchBtn, "Advanced search controls")
        Me.AdvancedSearchBtn.UseVisualStyleBackColor = False
        '
        'MainTabSettings
        '
        Me.MainTabSettings.Controls.Add(Me.FlowLayoutPanelSettings)
        Me.MainTabSettings.Location = New System.Drawing.Point(4, 29)
        Me.MainTabSettings.Name = "MainTabSettings"
        Me.MainTabSettings.Padding = New System.Windows.Forms.Padding(3)
        Me.MainTabSettings.Size = New System.Drawing.Size(1076, 701)
        Me.MainTabSettings.TabIndex = 1
        Me.MainTabSettings.Text = "Settings"
        Me.MainTabSettings.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanelSettings
        '
        Me.FlowLayoutPanelSettings.AutoScroll = True
        Me.FlowLayoutPanelSettings.Controls.Add(Me.GroupBoxDirectories)
        Me.FlowLayoutPanelSettings.Controls.Add(Me.GroupBoxGeneral)
        Me.FlowLayoutPanelSettings.Controls.Add(Me.AboutBox)
        Me.FlowLayoutPanelSettings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanelSettings.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanelSettings.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanelSettings.Name = "FlowLayoutPanelSettings"
        Me.FlowLayoutPanelSettings.Size = New System.Drawing.Size(1070, 695)
        Me.FlowLayoutPanelSettings.TabIndex = 2
        '
        'GroupBoxDirectories
        '
        Me.GroupBoxDirectories.AutoSize = True
        Me.GroupBoxDirectories.Controls.Add(Me.Fix_previews_btn)
        Me.GroupBoxDirectories.Controls.Add(Me.Regenerate_AI_tags_btn)
        Me.GroupBoxDirectories.Controls.Add(Me.preview_screen_lbl)
        Me.GroupBoxDirectories.Controls.Add(Me.btn_refresh)
        Me.GroupBoxDirectories.Controls.Add(Me.btn_select_folders)
        Me.GroupBoxDirectories.Controls.Add(Me.ElementHost1)
        Me.GroupBoxDirectories.Location = New System.Drawing.Point(3, 3)
        Me.GroupBoxDirectories.Name = "GroupBoxDirectories"
        Me.GroupBoxDirectories.Size = New System.Drawing.Size(539, 244)
        Me.GroupBoxDirectories.TabIndex = 3
        Me.GroupBoxDirectories.TabStop = False
        Me.GroupBoxDirectories.Text = "Video library"
        '
        'Fix_previews_btn
        '
        Me.Fix_previews_btn.BackColor = System.Drawing.Color.Thistle
        Me.Fix_previews_btn.Image = Global.VideoCatalog.My.Resources.Resources.support
        Me.Fix_previews_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Fix_previews_btn.Location = New System.Drawing.Point(6, 173)
        Me.Fix_previews_btn.Name = "Fix_previews_btn"
        Me.Fix_previews_btn.Padding = New System.Windows.Forms.Padding(5)
        Me.Fix_previews_btn.Size = New System.Drawing.Size(199, 43)
        Me.Fix_previews_btn.TabIndex = 4
        Me.Fix_previews_btn.Text = "Fix missing previews"
        Me.Fix_previews_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.Fix_previews_btn, "Try to generate previews again if they are missing")
        Me.Fix_previews_btn.UseVisualStyleBackColor = False
        '
        'Regenerate_AI_tags_btn
        '
        Me.Regenerate_AI_tags_btn.BackColor = System.Drawing.Color.Lavender
        Me.Regenerate_AI_tags_btn.Image = Global.VideoCatalog.My.Resources.Resources.tag_small
        Me.Regenerate_AI_tags_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Regenerate_AI_tags_btn.Location = New System.Drawing.Point(6, 124)
        Me.Regenerate_AI_tags_btn.Name = "Regenerate_AI_tags_btn"
        Me.Regenerate_AI_tags_btn.Padding = New System.Windows.Forms.Padding(5)
        Me.Regenerate_AI_tags_btn.Size = New System.Drawing.Size(199, 43)
        Me.Regenerate_AI_tags_btn.TabIndex = 3
        Me.Regenerate_AI_tags_btn.Text = "Regenerate AI tags"
        Me.Regenerate_AI_tags_btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.Regenerate_AI_tags_btn, "Regenerate AI tags for videos")
        Me.Regenerate_AI_tags_btn.UseVisualStyleBackColor = False
        '
        'preview_screen_lbl
        '
        Me.preview_screen_lbl.AutoSize = True
        Me.preview_screen_lbl.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.preview_screen_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.preview_screen_lbl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.preview_screen_lbl.Location = New System.Drawing.Point(211, 26)
        Me.preview_screen_lbl.Name = "preview_screen_lbl"
        Me.preview_screen_lbl.Size = New System.Drawing.Size(201, 22)
        Me.preview_screen_lbl.TabIndex = 2
        Me.preview_screen_lbl.Text = "Generating preview screens..."
        Me.preview_screen_lbl.Visible = False
        '
        'btn_refresh
        '
        Me.btn_refresh.BackColor = System.Drawing.Color.AliceBlue
        Me.btn_refresh.Image = Global.VideoCatalog.My.Resources.Resources.refresh
        Me.btn_refresh.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_refresh.Location = New System.Drawing.Point(6, 75)
        Me.btn_refresh.Name = "btn_refresh"
        Me.btn_refresh.Padding = New System.Windows.Forms.Padding(5)
        Me.btn_refresh.Size = New System.Drawing.Size(199, 43)
        Me.btn_refresh.TabIndex = 1
        Me.btn_refresh.Text = "Refresh library"
        Me.btn_refresh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btn_refresh, "The library should be refreshed if the contents of the video folders have changed" &
        "")
        Me.btn_refresh.UseVisualStyleBackColor = False
        '
        'btn_select_folders
        '
        Me.btn_select_folders.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.btn_select_folders.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btn_select_folders.Image = Global.VideoCatalog.My.Resources.Resources.folder
        Me.btn_select_folders.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_select_folders.Location = New System.Drawing.Point(6, 26)
        Me.btn_select_folders.Name = "btn_select_folders"
        Me.btn_select_folders.Padding = New System.Windows.Forms.Padding(5)
        Me.btn_select_folders.Size = New System.Drawing.Size(199, 43)
        Me.btn_select_folders.TabIndex = 0
        Me.btn_select_folders.Text = "Select folders"
        Me.btn_select_folders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btn_select_folders, "Select your video folders to include them in the library")
        Me.btn_select_folders.UseVisualStyleBackColor = False
        '
        'ElementHost1
        '
        Me.ElementHost1.Location = New System.Drawing.Point(212, 26)
        Me.ElementHost1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ElementHost1.Name = "ElementHost1"
        Me.ElementHost1.Size = New System.Drawing.Size(320, 190)
        Me.ElementHost1.TabIndex = 1
        Me.ElementHost1.Text = "ElementHost1"
        Me.ElementHost1.Child = Nothing
        '
        'GroupBoxGeneral
        '
        Me.GroupBoxGeneral.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBoxGeneral.AutoSize = True
        Me.GroupBoxGeneral.Controls.Add(Me.TagMenuCb)
        Me.GroupBoxGeneral.Controls.Add(Me.GroupBoxPreviewSpeed)
        Me.GroupBoxGeneral.Controls.Add(Me.ShowRatingCb)
        Me.GroupBoxGeneral.Controls.Add(Me.GeneralSettingsApplyBtn)
        Me.GroupBoxGeneral.Controls.Add(Me.GroupBoxVideoSize)
        Me.GroupBoxGeneral.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBoxGeneral.Location = New System.Drawing.Point(3, 253)
        Me.GroupBoxGeneral.Name = "GroupBoxGeneral"
        Me.GroupBoxGeneral.Size = New System.Drawing.Size(539, 315)
        Me.GroupBoxGeneral.TabIndex = 4
        Me.GroupBoxGeneral.TabStop = False
        Me.GroupBoxGeneral.Text = "General"
        '
        'TagMenuCb
        '
        Me.TagMenuCb.AutoSize = True
        Me.TagMenuCb.Checked = True
        Me.TagMenuCb.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TagMenuCb.Location = New System.Drawing.Point(108, 217)
        Me.TagMenuCb.Name = "TagMenuCb"
        Me.TagMenuCb.Size = New System.Drawing.Size(198, 24)
        Me.TagMenuCb.TabIndex = 5
        Me.TagMenuCb.Text = "Tag menu for editing tags"
        Me.TagMenuCb.UseVisualStyleBackColor = True
        '
        'GroupBoxPreviewSpeed
        '
        Me.GroupBoxPreviewSpeed.Controls.Add(Me.PreviewSpeedRadio_veryfast)
        Me.GroupBoxPreviewSpeed.Controls.Add(Me.PreviewSpeedRadio_fast)
        Me.GroupBoxPreviewSpeed.Controls.Add(Me.PreviewSpeedRadio_default)
        Me.GroupBoxPreviewSpeed.Controls.Add(Me.PreviewSpeedRadio_slow)
        Me.GroupBoxPreviewSpeed.Location = New System.Drawing.Point(6, 142)
        Me.GroupBoxPreviewSpeed.Name = "GroupBoxPreviewSpeed"
        Me.GroupBoxPreviewSpeed.Size = New System.Drawing.Size(352, 69)
        Me.GroupBoxPreviewSpeed.TabIndex = 4
        Me.GroupBoxPreviewSpeed.TabStop = False
        Me.GroupBoxPreviewSpeed.Text = "Preview change speed"
        '
        'PreviewSpeedRadio_veryfast
        '
        Me.PreviewSpeedRadio_veryfast.AutoSize = True
        Me.PreviewSpeedRadio_veryfast.Location = New System.Drawing.Point(217, 26)
        Me.PreviewSpeedRadio_veryfast.Name = "PreviewSpeedRadio_veryfast"
        Me.PreviewSpeedRadio_veryfast.Size = New System.Drawing.Size(83, 24)
        Me.PreviewSpeedRadio_veryfast.TabIndex = 3
        Me.PreviewSpeedRadio_veryfast.Text = "Very fast"
        Me.PreviewSpeedRadio_veryfast.UseVisualStyleBackColor = True
        '
        'PreviewSpeedRadio_fast
        '
        Me.PreviewSpeedRadio_fast.AutoSize = True
        Me.PreviewSpeedRadio_fast.Location = New System.Drawing.Point(159, 26)
        Me.PreviewSpeedRadio_fast.Name = "PreviewSpeedRadio_fast"
        Me.PreviewSpeedRadio_fast.Size = New System.Drawing.Size(52, 24)
        Me.PreviewSpeedRadio_fast.TabIndex = 2
        Me.PreviewSpeedRadio_fast.Text = "Fast"
        Me.PreviewSpeedRadio_fast.UseVisualStyleBackColor = True
        '
        'PreviewSpeedRadio_default
        '
        Me.PreviewSpeedRadio_default.AutoSize = True
        Me.PreviewSpeedRadio_default.Checked = True
        Me.PreviewSpeedRadio_default.Location = New System.Drawing.Point(71, 26)
        Me.PreviewSpeedRadio_default.Name = "PreviewSpeedRadio_default"
        Me.PreviewSpeedRadio_default.Size = New System.Drawing.Size(76, 24)
        Me.PreviewSpeedRadio_default.TabIndex = 1
        Me.PreviewSpeedRadio_default.TabStop = True
        Me.PreviewSpeedRadio_default.Text = "Default"
        Me.PreviewSpeedRadio_default.UseVisualStyleBackColor = True
        '
        'PreviewSpeedRadio_slow
        '
        Me.PreviewSpeedRadio_slow.AutoSize = True
        Me.PreviewSpeedRadio_slow.Location = New System.Drawing.Point(6, 26)
        Me.PreviewSpeedRadio_slow.Name = "PreviewSpeedRadio_slow"
        Me.PreviewSpeedRadio_slow.Size = New System.Drawing.Size(59, 24)
        Me.PreviewSpeedRadio_slow.TabIndex = 0
        Me.PreviewSpeedRadio_slow.Text = "Slow"
        Me.PreviewSpeedRadio_slow.UseVisualStyleBackColor = True
        '
        'ShowRatingCb
        '
        Me.ShowRatingCb.AutoSize = True
        Me.ShowRatingCb.Checked = True
        Me.ShowRatingCb.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ShowRatingCb.Location = New System.Drawing.Point(6, 217)
        Me.ShowRatingCb.Name = "ShowRatingCb"
        Me.ShowRatingCb.Size = New System.Drawing.Size(97, 24)
        Me.ShowRatingCb.TabIndex = 3
        Me.ShowRatingCb.Text = "Star rating"
        Me.ShowRatingCb.UseVisualStyleBackColor = True
        '
        'GeneralSettingsApplyBtn
        '
        Me.GeneralSettingsApplyBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(225, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.GeneralSettingsApplyBtn.Image = Global.VideoCatalog.My.Resources.Resources.save
        Me.GeneralSettingsApplyBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.GeneralSettingsApplyBtn.Location = New System.Drawing.Point(6, 247)
        Me.GeneralSettingsApplyBtn.Name = "GeneralSettingsApplyBtn"
        Me.GeneralSettingsApplyBtn.Padding = New System.Windows.Forms.Padding(5)
        Me.GeneralSettingsApplyBtn.Size = New System.Drawing.Size(199, 42)
        Me.GeneralSettingsApplyBtn.TabIndex = 2
        Me.GeneralSettingsApplyBtn.Text = "Apply"
        Me.GeneralSettingsApplyBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.GeneralSettingsApplyBtn.UseVisualStyleBackColor = False
        '
        'GroupBoxVideoSize
        '
        Me.GroupBoxVideoSize.Controls.Add(Me.PreviewSizeRadio_Verylarge)
        Me.GroupBoxVideoSize.Controls.Add(Me.PreviewSizeRadio_Large)
        Me.GroupBoxVideoSize.Controls.Add(Me.PreviewSizeRadio_Medium)
        Me.GroupBoxVideoSize.Controls.Add(Me.PreviewSizeRadio_Small)
        Me.GroupBoxVideoSize.Location = New System.Drawing.Point(6, 65)
        Me.GroupBoxVideoSize.Name = "GroupBoxVideoSize"
        Me.GroupBoxVideoSize.Size = New System.Drawing.Size(352, 69)
        Me.GroupBoxVideoSize.TabIndex = 1
        Me.GroupBoxVideoSize.TabStop = False
        Me.GroupBoxVideoSize.Text = "Video preview size"
        '
        'PreviewSizeRadio_Verylarge
        '
        Me.PreviewSizeRadio_Verylarge.AutoSize = True
        Me.PreviewSizeRadio_Verylarge.Location = New System.Drawing.Point(234, 27)
        Me.PreviewSizeRadio_Verylarge.Name = "PreviewSizeRadio_Verylarge"
        Me.PreviewSizeRadio_Verylarge.Size = New System.Drawing.Size(93, 24)
        Me.PreviewSizeRadio_Verylarge.TabIndex = 4
        Me.PreviewSizeRadio_Verylarge.Text = "Very large"
        Me.PreviewSizeRadio_Verylarge.UseVisualStyleBackColor = True
        '
        'PreviewSizeRadio_Large
        '
        Me.PreviewSizeRadio_Large.AutoSize = True
        Me.PreviewSizeRadio_Large.Checked = True
        Me.PreviewSizeRadio_Large.Location = New System.Drawing.Point(164, 26)
        Me.PreviewSizeRadio_Large.Name = "PreviewSizeRadio_Large"
        Me.PreviewSizeRadio_Large.Size = New System.Drawing.Size(64, 24)
        Me.PreviewSizeRadio_Large.TabIndex = 2
        Me.PreviewSizeRadio_Large.TabStop = True
        Me.PreviewSizeRadio_Large.Text = "Large"
        Me.PreviewSizeRadio_Large.UseVisualStyleBackColor = True
        '
        'PreviewSizeRadio_Medium
        '
        Me.PreviewSizeRadio_Medium.AutoSize = True
        Me.PreviewSizeRadio_Medium.Location = New System.Drawing.Point(76, 26)
        Me.PreviewSizeRadio_Medium.Name = "PreviewSizeRadio_Medium"
        Me.PreviewSizeRadio_Medium.Size = New System.Drawing.Size(82, 24)
        Me.PreviewSizeRadio_Medium.TabIndex = 1
        Me.PreviewSizeRadio_Medium.Text = "Medium"
        Me.PreviewSizeRadio_Medium.UseVisualStyleBackColor = True
        '
        'PreviewSizeRadio_Small
        '
        Me.PreviewSizeRadio_Small.AutoSize = True
        Me.PreviewSizeRadio_Small.Location = New System.Drawing.Point(6, 26)
        Me.PreviewSizeRadio_Small.Name = "PreviewSizeRadio_Small"
        Me.PreviewSizeRadio_Small.Size = New System.Drawing.Size(64, 24)
        Me.PreviewSizeRadio_Small.TabIndex = 0
        Me.PreviewSizeRadio_Small.Text = "Small"
        Me.PreviewSizeRadio_Small.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.MaxVisibleVideosNumeric, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 26)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(258, 33)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(187, 33)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Number of videos on page"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MaxVisibleVideosNumeric
        '
        Me.MaxVisibleVideosNumeric.Location = New System.Drawing.Point(196, 3)
        Me.MaxVisibleVideosNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.MaxVisibleVideosNumeric.Name = "MaxVisibleVideosNumeric"
        Me.MaxVisibleVideosNumeric.Size = New System.Drawing.Size(59, 27)
        Me.MaxVisibleVideosNumeric.TabIndex = 1
        Me.MaxVisibleVideosNumeric.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'AboutBox
        '
        Me.AboutBox.AutoSize = True
        Me.AboutBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.AboutBox.Controls.Add(Me.Licenselbl)
        Me.AboutBox.Controls.Add(Me.Label2)
        Me.AboutBox.Controls.Add(Me.Versionlbl)
        Me.AboutBox.Location = New System.Drawing.Point(3, 574)
        Me.AboutBox.Name = "AboutBox"
        Me.AboutBox.Size = New System.Drawing.Size(232, 106)
        Me.AboutBox.TabIndex = 5
        Me.AboutBox.TabStop = False
        Me.AboutBox.Text = "About"
        '
        'Licenselbl
        '
        Me.Licenselbl.AutoSize = True
        Me.Licenselbl.Location = New System.Drawing.Point(6, 43)
        Me.Licenselbl.Name = "Licenselbl"
        Me.Licenselbl.Size = New System.Drawing.Size(86, 20)
        Me.Licenselbl.TabIndex = 6
        Me.Licenselbl.TabStop = True
        Me.Licenselbl.Text = "MIT License"
        Me.ToolTip1.SetToolTip(Me.Licenselbl, "https://www.gnu.org/licenses/gpl-3.0.en.html")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(220, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Copyright (C) 2018 Joni Juvonen"
        '
        'Versionlbl
        '
        Me.Versionlbl.AutoSize = True
        Me.Versionlbl.Location = New System.Drawing.Point(6, 23)
        Me.Versionlbl.Name = "Versionlbl"
        Me.Versionlbl.Size = New System.Drawing.Size(60, 20)
        Me.Versionlbl.TabIndex = 0
        Me.Versionlbl.Text = "Version:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.statusLabel, Me.SpringStatusLabel, Me.CancelLoadingBtn, Me.BottomProgressBar})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 739)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1084, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'statusLabel
        '
        Me.statusLabel.Name = "statusLabel"
        Me.statusLabel.Size = New System.Drawing.Size(0, 17)
        '
        'SpringStatusLabel
        '
        Me.SpringStatusLabel.Name = "SpringStatusLabel"
        Me.SpringStatusLabel.Size = New System.Drawing.Size(917, 17)
        Me.SpringStatusLabel.Spring = True
        '
        'CancelLoadingBtn
        '
        Me.CancelLoadingBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CancelLoadingBtn.Image = Global.VideoCatalog.My.Resources.Resources.cancel
        Me.CancelLoadingBtn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CancelLoadingBtn.Name = "CancelLoadingBtn"
        Me.CancelLoadingBtn.ShowDropDownArrow = False
        Me.CancelLoadingBtn.Size = New System.Drawing.Size(20, 20)
        Me.CancelLoadingBtn.Text = "ToolStripDropDownButton1"
        Me.CancelLoadingBtn.ToolTipText = "Cancel"
        Me.CancelLoadingBtn.Visible = False
        '
        'BottomProgressBar
        '
        Me.BottomProgressBar.Name = "BottomProgressBar"
        Me.BottomProgressBar.Size = New System.Drawing.Size(150, 16)
        '
        'ErrorFlashTimer
        '
        Me.ErrorFlashTimer.Interval = 500
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1084, 761)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MainTabControl)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "Form1"
        Me.Text = "Juniper Video Library"
        Me.MainTabControl.ResumeLayout(False)
        Me.MainTabVideos.ResumeLayout(False)
        Me.TableLayoutPanelVideos.ResumeLayout(False)
        Me.TableLayoutPanelVideos.PerformLayout()
        Me.NavigatorTable.ResumeLayout(False)
        Me.NavigatorTable.PerformLayout()
        CType(Me.prev_btn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.next_btn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SearchTableLayout.ResumeLayout(False)
        Me.SearchTableLayout.PerformLayout()
        Me.AdvancedSearchPanel.ResumeLayout(False)
        Me.DurationFilterGroup.ResumeLayout(False)
        Me.DurationFilterGroup.PerformLayout()
        CType(Me.DurationFilterTrackbar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RatingFilterGroup.ResumeLayout(False)
        Me.RatingFilterGroup.PerformLayout()
        CType(Me.ratingFilterNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SortBox.ResumeLayout(False)
        Me.SortBox.PerformLayout()
        Me.MainTabSettings.ResumeLayout(False)
        Me.FlowLayoutPanelSettings.ResumeLayout(False)
        Me.FlowLayoutPanelSettings.PerformLayout()
        Me.GroupBoxDirectories.ResumeLayout(False)
        Me.GroupBoxDirectories.PerformLayout()
        Me.GroupBoxGeneral.ResumeLayout(False)
        Me.GroupBoxGeneral.PerformLayout()
        Me.GroupBoxPreviewSpeed.ResumeLayout(False)
        Me.GroupBoxPreviewSpeed.PerformLayout()
        Me.GroupBoxVideoSize.ResumeLayout(False)
        Me.GroupBoxVideoSize.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.MaxVisibleVideosNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.AboutBox.ResumeLayout(False)
        Me.AboutBox.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ElementHost1 As Integration.ElementHost
    Friend WithEvents MainTabControl As TabControl
    Friend WithEvents MainTabVideos As TabPage
    Friend WithEvents MainTabSettings As TabPage
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ErrorFlashTimer As Timer
    Friend WithEvents VideoFlowLayoutPanel As FlowLayoutPanel
    Friend WithEvents TableLayoutPanelVideos As TableLayoutPanel
    Friend WithEvents FlowLayoutPanelSettings As FlowLayoutPanel
    Friend WithEvents GroupBoxDirectories As GroupBox
    Friend WithEvents btn_refresh As Button
    Friend WithEvents btn_select_folders As Button
    Friend WithEvents NavigatorTable As TableLayoutPanel
    Friend WithEvents navigator_lbl As Label
    Friend WithEvents prev_btn As PictureBox
    Friend WithEvents next_btn As PictureBox
    Friend WithEvents SpringStatusLabel As ToolStripStatusLabel
    Friend WithEvents BottomProgressBar As ToolStripProgressBar
    Friend WithEvents statusLabel As ToolStripStatusLabel
    Friend WithEvents SearchTableLayout As TableLayoutPanel
    Friend WithEvents AdvancedSearchPanel As FlowLayoutPanel
    Friend WithEvents SearchTextBox As TextBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents GroupBoxGeneral As GroupBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents MaxVisibleVideosNumeric As NumericUpDown
    Friend WithEvents GroupBoxVideoSize As GroupBox
    Friend WithEvents PreviewSizeRadio_Large As RadioButton
    Friend WithEvents PreviewSizeRadio_Medium As RadioButton
    Friend WithEvents PreviewSizeRadio_Small As RadioButton
    Friend WithEvents PreviewSizeRadio_Verylarge As RadioButton
    Friend WithEvents ShowRatingCb As CheckBox
    Friend WithEvents GeneralSettingsApplyBtn As Button
    Friend WithEvents GroupBoxPreviewSpeed As GroupBox
    Friend WithEvents PreviewSpeedRadio_fast As RadioButton
    Friend WithEvents PreviewSpeedRadio_default As RadioButton
    Friend WithEvents PreviewSpeedRadio_slow As RadioButton
    Friend WithEvents TagMenuCb As CheckBox
    Friend WithEvents DurationFilterGroup As GroupBox
    Friend WithEvents DurationFilterTrackbar As TrackBar
    Friend WithEvents DurationFilterRadio_under As RadioButton
    Friend WithEvents DurationFilterRadio_over As RadioButton
    Friend WithEvents DurationFilterRadio_none As RadioButton
    Friend WithEvents AboutBox As GroupBox
    Friend WithEvents Licenselbl As LinkLabel
    Friend WithEvents Label2 As Label
    Friend WithEvents Versionlbl As Label
    Friend WithEvents SortBox As GroupBox
    Friend WithEvents SortByDateCb As RadioButton
    Friend WithEvents SortByRelevanceCb As RadioButton
    Friend WithEvents videoCountlbl As Label
    Friend WithEvents SortByLengthCb As RadioButton
    Friend WithEvents SortOrderCb As CheckBox
    Friend WithEvents NavigatorTextBox As TextBox
    Friend WithEvents SortByRandomCb As RadioButton
    Friend WithEvents SortByRatingCb As RadioButton
    Friend WithEvents RatingFilterGroup As GroupBox
    Friend WithEvents RatingFilterRadio_under As RadioButton
    Friend WithEvents RatingFilterRadio_over As RadioButton
    Friend WithEvents RatingFilterRadio_none As RadioButton
    Friend WithEvents durationFilterLbl As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents ratingFilterNumeric As NumericUpDown
    Friend WithEvents RatingFilterRadio_notrated As RadioButton
    Friend WithEvents preview_screen_lbl As Label
    Friend WithEvents PreviewSpeedRadio_veryfast As RadioButton
    Friend WithEvents Regenerate_AI_tags_btn As Button
    Friend WithEvents Fix_previews_btn As Button
    Friend WithEvents AdvancedSearchBtn As Button
    Friend WithEvents CancelLoadingBtn As ToolStripDropDownButton
End Class
