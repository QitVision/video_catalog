﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

'Videofile data class that holds meta data

Public Class VideoFile

    Private path As String  'file path
    Private id As String    'fingerprint of the file
    Private extension As String 'File type extension e.g. mp4
    Private tags As String() = {}
    Private length As Long 'seconds
    Private resolution As Size
    Private size As Long 'bytes
    Private created As String 'Date string, format yyyy.MM.dd-HH.mm
    Private edited As String 'Date string, format yyyy.MM.dd-HH.mm
    Private fps As String 'Frames per second as string, this value may also contain localized text e.g. '24.0 frames per second'
    Private path_tags As String() = {}  'tags that are parsed from filename and folder paths. For example if the file path is '~Documents\Spain\swimmingpool.avi', path_tags are 'spain' and 'swimmingpool'
    Private AI_tags As String() = {}    'reserved for later, these are AI classified tags. TODO: use TensorFlow and ImageNet to classify videos from their screenshot previews
    Private rating As Integer           'range from -1 to 50, -1 = unrated, 0 = 0 stars, 50 = 5 stars. This value is divided by 10 to get one decimal star rating
    Private preview_index As Integer = -1   'this is a 0-based index of preview files that is used as a default pic

    Dim DateFormat As String = "yyyy.MM.dd-HH.mm"

    Dim db_changes As Boolean = False            'flag that is used to make decision about saving the database

    Public Sub New(ByVal _path As String, ByVal _id As String, ByVal _extension As String, ByVal _tags As String(), ByVal _length As Long, ByVal _resolution As Size, ByVal _size As Long, ByVal _created As String, ByVal _edited As String, ByVal _fps As String, ByVal _path_tags As String(), ByVal _AI_tags As String(), ByVal _rating As Integer, ByVal _preview_index As Integer)
        'initialize file info
        path = _path
        id = _id
        extension = _extension
        tags = _tags
        length = _length
        resolution = _resolution
        size = _size
        created = _created
        edited = _edited
        fps = _fps
        path_tags = _path_tags
        AI_tags = _AI_tags
        rating = _rating
        preview_index = _preview_index

        'fill extension if it is missing
        If (extension.Equals("")) Then
            Try
                extension = path.Substring(path.LastIndexOf(".") + 1, path.Length - path.LastIndexOf(".") - 1)
                db_changes = True   'mark changes
            Catch ex As Exception
                Console.WriteLine("extension parse error: " + ex.ToString)
            End Try
        End If

        'fill path tags if missing
        If (path_tags.Length = 0) Then
            ParsePathTags()
            db_changes = True   'mark changes
        End If

    End Sub

    Private Sub ParsePathTags()

        'Skip default Windows directory words like Documents or Videos
        Dim myDocuments As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        Dim myPictures As String = My.Computer.FileSystem.SpecialDirectories.MyPictures
        Dim myPrograms As String = My.Computer.FileSystem.SpecialDirectories.ProgramFiles
        Dim myMusic As String = My.Computer.FileSystem.SpecialDirectories.MyMusic
        Dim myDesktop As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        Dim myFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
        Dim TruePath As String = path.Replace(myDocuments, "").Replace(myPictures, "").Replace(myMusic, "").Replace(myDesktop, "").Replace(myFolder, "")
        TruePath = TruePath.ToLower()

        Dim path_parts As String() = TruePath.Substring(0, TruePath.LastIndexOf(".")).Split(New Char() {"\"c}, StringSplitOptions.RemoveEmptyEntries)
        Dim good_parts As New List(Of String)
        For Each part In path_parts
            'skip if it is a drive letter
            If (part.Contains(":")) Then Continue For

            'split words if they contain whitespace _ or -
            Dim parts2 As String() = part.Split(New Char() {" "c}, StringSplitOptions.RemoveEmptyEntries)
            For Each part2 In parts2
                Dim parts3 As String() = part2.Split(New Char() {"_"c}, StringSplitOptions.RemoveEmptyEntries)
                For Each part3 In parts3
                    Dim parts4 As String() = part3.Split(New Char() {"-"c}, StringSplitOptions.RemoveEmptyEntries)
                    For Each part4 In parts4
                        Dim part5 As String = part4.Replace("videos", "").Replace("video", "")
                        If (part5.Length > 1) Then
                            'remove the word video
                            good_parts.Add(part5)
                        End If
                    Next
                Next
            Next
        Next

        path_tags = good_parts.ToArray
    End Sub

#Region "Changes to videofile"

    Public Function hasChanges() As Boolean
        Return db_changes
    End Function

    'called after db save
    Public Sub clearDBChanges()
        db_changes = False
    End Sub

#End Region

#Region "Getters"

    Public Function getName() As String
        Try
            Return path.Substring(path.LastIndexOf("\") + 1, path.Length - path.LastIndexOf("\") - extension.Length - 2)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Return path
        End Try
    End Function

    Public Function getPath() As String
        Return path
    End Function

    Public Function getId() As String
        Return id
    End Function

    Public Function getExtension() As String
        Return extension
    End Function

    Public Function getTags() As String()
        Return tags
    End Function

    Public Function getLength() As Long
        Return length
    End Function

    Public Function getLengthStr() As String
        'get hours minutes and seconds
        Dim hours As Integer = Math.Floor(length / 3600)
        Dim minutes As Integer = Math.Floor((length - (3600 * hours)) / 60)
        Dim seconds As Integer = length - (3600 * hours) - (60 * minutes)
        'string format
        Dim hourStr As String = hours.ToString
        If (hourStr.Length = 1) Then hourStr = "0" + hourStr
        Dim minutesStr As String = minutes.ToString
        If (minutesStr.Length = 1) Then minutesStr = "0" + minutesStr
        Dim secondsStr As String = seconds.ToString
        If (secondsStr.Length = 1) Then secondsStr = "0" + secondsStr

        Return hourStr + ":" + minutesStr + ":" + secondsStr
    End Function

    Public Function getresolution() As Size
        Return resolution
    End Function

    Public Function getSize() As Long
        Return size
    End Function

    Public Function getCreated() As String
        Return created
    End Function

    Public Function getEdited() As String
        Return edited
    End Function

    Public Function getFPS() As String
        Return fps
    End Function

    Public Function getPathTags() As String()
        Return path_tags
    End Function

    Public Function getAITags() As String()
        Return AI_tags
    End Function

    Public Function getRating() As Integer
        Return rating
    End Function

    Public Function getCreatedLong() As Long
        'returns the created date as comparable date long that is useful in sorting by created time
        Dim result_date As Date
        If (Date.TryParseExact(created, DateFormat, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None, result_date)) Then
            Return result_date.ToFileTimeUtc
        End If
        Return 0
    End Function

    Public Function getPreviewIndex() As Integer
        Return preview_index
    End Function

#End Region

#Region "Setters"

#Region "regular tags"

    Public Sub addTag(ByVal tag As String)
        If (tag Is Nothing) Then
            'first tag
            setTag({tag})
        Else
            'add Tag 
            Dim newTags(tags.Count + 1) As String
            If (tags.Count = 0) Then    'first tag
                setTag({tag})
                Return
            End If

            For i As Integer = 0 To tags.Count - 1
                newTags(i) = tags(i)
            Next
            newTags(newTags.Count - 1) = tag
            tags = newTags 'replace old tags
        End If
        db_changes = True  'mark changes
    End Sub

    Public Sub setTag(ByVal _tags As String())
        tags = _tags
        db_changes = True   'mark changes
    End Sub

    Public Sub clearTags()
        tags = {}
        db_changes = True  'mark changes
    End Sub

#End Region

#Region "path tags"

    Public Sub addPathTag(ByVal pathTag As String)
        If (pathTag Is Nothing) Then
            'first tag
            setPathTag({pathTag})
        Else
            'add Tag 
            Dim newPathTags(path_tags.Count + 1) As String
            If (path_tags.Count = 0) Then    'first tag
                setPathTag({pathTag})
                Return
            End If

            For i As Integer = 0 To path_tags.Count - 1
                newPathTags(i) = path_tags(i)
            Next
            newPathTags(newPathTags.Count - 1) = pathTag
            path_tags = newPathTags 'replace old tags
        End If
        db_changes = True   'mark changes
    End Sub

    Public Sub setPathTag(ByVal _PathTags As String())
        path_tags = _PathTags
        db_changes = True   'mark changes
    End Sub

    Public Sub clearPathTags()
        path_tags = {}
        db_changes = True   'mark changes
    End Sub

#End Region

#Region "AI tags"

    Public Sub addAITag(ByVal AITag As String)
        If (AITag Is Nothing) Then
            'first tag
            setAITag({AITag})
        Else
            'add Tag 
            Dim newAITags(AI_tags.Count + 1) As String
            If (AI_tags.Count = 0) Then    'first tag
                setAITag({AITag})
                Return
            End If

            For i As Integer = 0 To AI_tags.Count - 1
                newAITags(i) = AI_tags(i)
            Next
            newAITags(newAITags.Count - 1) = AITag
            AI_tags = newAITags 'replace old tags
        End If
        db_changes = True   'mark changes
    End Sub

    Public Sub setAITag(ByVal _AITags As String())
        AI_tags = _AITags
        db_changes = True   'mark changes
    End Sub

    Public Sub clearAITags()
        AI_tags = {}
        db_changes = True   'mark changes
    End Sub

#End Region

    Public Sub setPath(ByVal _path As String)
        path = _path
        db_changes = True   'mark changes
    End Sub

    Public Sub setCreated(ByVal _created As String)
        created = _created
        db_changes = True   'mark changes
    End Sub

    Public Sub setEdited(ByVal _edited As String)
        edited = _edited
        db_changes = True   'mark changes
    End Sub

    Public Sub setRating(ByVal _rating As Integer)
        _rating = Math.Min(_rating, 50)
        _rating = Math.Max(_rating, -1)
        rating = _rating
        db_changes = True   'mark changes
    End Sub

    Public Sub setResolution(ByVal _resolution As Size)
        resolution = _resolution
        db_changes = True   'mark changes
    End Sub

    Public Sub setLength(ByVal _length As Long)
        length = _length
        db_changes = True   'mark changes
    End Sub

    Public Sub setFps(ByVal _fps As String)
        fps = _fps
        db_changes = True   'mark changes
    End Sub

    Public Sub setPreviewIndex(ByVal _preview_index As Integer)
        preview_index = _preview_index
        db_changes = True   'mark changes
    End Sub

#End Region

End Class
