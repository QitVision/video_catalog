﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.IO

'This dialog window returns DialogResult.OK if OK was pressed and the list is not empty
'Returns DialogResult.No if the list is empty and OK was pressed
'Otherwise returns DialogResult.Cancel

Public Class SetRoots

    Dim ROOT_DIRECTORIES As List(Of String)

    Public Sub New(ByRef _ROOT_DIRECTORIES As List(Of String))

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ROOT_DIRECTORIES = _ROOT_DIRECTORIES
    End Sub

    Private Sub SetRoots_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'load dirs
        For Each _Dir As String In ROOT_DIRECTORIES
            rootListBox.Items.Add(_Dir)
        Next
    End Sub

    Private Sub cancelbtn_Click(sender As Object, e As EventArgs) Handles cancelbtn.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub okbtn_Click(sender As Object, e As EventArgs) Handles okbtn.Click
        'Load dirs back to list
        ROOT_DIRECTORIES.Clear()
        For Each _dir As String In rootListBox.Items
            Try
                If (Directory.Exists(_dir)) Then
                    ROOT_DIRECTORIES.Add(_dir)
                End If
            Catch ex As Exception
                MsgBox("Directory " + _dir + " does not exist.")
            End Try
        Next

        'dont return ok if the list is empty
        If (ROOT_DIRECTORIES.Count = 0) Then
            Me.DialogResult = DialogResult.No
            Me.Close()
        End If

        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub RemoveBtn_Click(sender As Object, e As EventArgs) Handles RemoveBtn.Click
        'remove selected item
        If (rootListBox.Items.Count > 0) Then
            rootListBox.Items.RemoveAt(rootListBox.SelectedIndex)
        End If
    End Sub

    Private Sub AddBtn_Click(sender As Object, e As EventArgs) Handles AddBtn.Click
        If (FolderBrowserDialog1.ShowDialog = DialogResult.OK) Then
            If Not (rootListBox.Items.Contains(FolderBrowserDialog1.SelectedPath)) Then
                rootListBox.Items.Add(FolderBrowserDialog1.SelectedPath)
            End If
        End If
    End Sub
End Class