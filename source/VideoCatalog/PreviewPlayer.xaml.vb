﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.Windows.Media
Imports System.Windows.Media.Imaging
Imports System.IO
Imports OpenCV
Imports System.ComponentModel


'WPF Media Player class for capturing screenshots
'Joni Juvonen 2017

Public Class PreviewPlayer

    Private output_width As Integer                 'Screenshot width. This is always 640
    Private output_height As Integer                'Screenshot height. This is either 480 or 380 depending on the original video's aspect ratio
    Private PAUSE_WAIT_COUNT As Integer = 2         '[1-20] one count = 100 ms. UI-draw waiting period before pausing after seek. Increase this if there are multiple same screenshots
    Private UI_WAIT_COUNT As Integer = 2            '[1-20] one count = 100 ms. UI-draw waiting period before each screenshot. Increase this if the screenshot are black

    Private mTimeline As MediaTimeline
    Private mClock As MediaClock

    'Check preview file checksum against previous file and black image checksum to determine if the preview was successful
    Private lastpreview_checksum As String = ""
    Private blackpreview_checksum As String = "2945c1264bff7709351955d8dee1bc07"    '640x380 black jpeg
    Private blackpreview_checksum2 As String = "8d3b890dcc57fe38585817082c895af4"   '640x480 black jpeg

    Private EMD_Threshold As Single = 1.0
    Private lastpreview_histogram As Net.Arr

    Public Sub captureScreenshots(ByRef vid As VideoFile, ByVal dirPath As String, ByVal screenshot_count As Integer, ByRef bw_handle As BackgroundWorker)

        'Check if video extended properties are missing and fill them
        Dim extended_properties_missing As Boolean = False
        If (vid.getLength = 0) Then
            extended_properties_missing = True
        End If

        'Load video to mediaplayer
        mTimeline = New MediaTimeline(New Uri(vid.getPath, UriKind.Absolute))
        'add timing control
        mClock = mTimeline.CreateClock
        Dim _VideoDrawingPlayer As MediaPlayer = New MediaPlayer
        _VideoDrawingPlayer.Clock = mClock
        Dim _VideoDrawing As VideoDrawing = New VideoDrawing
        _VideoDrawing.Rect = New System.Windows.Rect(0, 0, 100, 100)  'doesn't matter. needs to be bigger than 0x0
        _VideoDrawing.Player = _VideoDrawingPlayer
        _VideoDrawing.Player.Volume = 0
        _VideoDrawing.Player.IsMuted = True
        'make a brush out of the player and paint this control's background with it
        Dim db As DrawingBrush = New DrawingBrush(_VideoDrawing)
        Me.Background = db

        If (extended_properties_missing) Then
            'update UI event queue to take effect
            Dim wait_counter As Integer = 0
            While (Not mClock.NaturalDuration.HasTimeSpan)
                System.Windows.Forms.Application.DoEvents()
                wait_counter += 1
                System.Threading.Thread.Sleep(100)
                If (wait_counter > 50) Then
                    Console.WriteLine("Timespan not loading")
                    'mark the folder
                    saveErrorScreen(dirPath + "no_preview" + ".jpg")

                    'generate version file
                    Using outputFile As New StreamWriter(dirPath + "preview_version.txt", False, System.Text.Encoding.UTF8)
                        outputFile.WriteLine("version:2")
                    End Using

                    Return    'this is an error case
                End If
            End While

            vid.setLength(mClock.NaturalDuration.TimeSpan.TotalSeconds)

            Dim _width As Integer = _VideoDrawingPlayer.NaturalVideoWidth
            Dim _height As Integer = _VideoDrawingPlayer.NaturalVideoHeight
            Dim _resolution As System.Drawing.Size = New System.Drawing.Size(_width, _height)

            vid.setResolution(_resolution)
        End If

        'set output frame size
        'Only 640x480 (4:3) or 640x360 (16:9) preview shots are supported, depending which fits better
        output_width = 640
        output_height = 360
        If vid.getresolution().Height > 0 Then
            Dim aspect_ratio As Double = vid.getresolution().Width / vid.getresolution().Height
            'aspect ratio could be closer to 4:3 than 16:9
            If (aspect_ratio < 1.5) Then
                output_height = 480
            End If
        End If

        'calculate screenshot intervals
        'do not save the start or end of the video
        'Divide the video to n parts and capture a frame from the middle of each part
        Dim interval As Double = vid.getLength / screenshot_count  'how long is the difference in seconds between two screenshots
        Dim small_interval As Double = interval / 8                'for seeking forward if the screenshot is too similar than the previous one
        Dim shift_from_start As Double = interval / 2              'what is the offset of the first screenshot
        Dim intervalSpan As TimeSpan = New TimeSpan(0, 0, interval)
        Dim shift_from_start_Span As TimeSpan = New TimeSpan(0, 0, shift_from_start)
        Dim endBoundTimeSpan As TimeSpan = New TimeSpan(0, 0, vid.getLength - 1)

        'silence
        _VideoDrawing.Player.Volume = 0

        'if every screenshot fails, don't take all of them
        Dim failed_screenshot_counter As Integer = 0

        'Cancel waiting
        If (bw_handle.CancellationPending) Then
            mClock.Controller.Stop()
            GC.Collect()
            Return
        End If

        If (screenshot_count > 0) Then
            'take screenshots
            For i As Integer = 1 To screenshot_count
                'calculate screenshot position
                Dim seek_Span As TimeSpan = shift_from_start_Span + (New TimeSpan(0, 0, interval * (i - 1)))

                'resume clock after pause (seek does not work on pause)
                mClock.Controller.Resume()

                'update UI event queue to take effect
                System.Windows.Forms.Application.DoEvents()

                'seek to screenshot section
                'Dispatcher.Invoke(New SeekToSpan_delegate(AddressOf SeekToSpan), Threading.DispatcherPriority.ContextIdle, New Object() {seek_Span})
                mClock.Controller.Seek(seek_Span, Animation.TimeSeekOrigin.BeginTime)

                If (mClock.CurrentTime Is Nothing) Then
                    While mClock.CurrentTime Is Nothing
                        Console.WriteLine("waiting for the clock to activate")
                        System.Threading.Thread.Sleep(100)
                        System.Windows.Forms.Application.DoEvents()

                        'Cancel waiting
                        If (bw_handle.CancellationPending) Then
                            mClock.Controller.Stop()
                            GC.Collect()
                            Return
                        End If

                    End While
                End If

                Dim seek_counter As Integer = 0
                While (mClock.CurrentTime < seek_Span)
                    'don't loop if the video is alreade at the end
                    If (seek_Span > endBoundTimeSpan) Then Exit While
                    'sometimes the seek invokation is ignored, so repeat the request
                    mClock.Controller.Seek(seek_Span, Animation.TimeSeekOrigin.BeginTime)

                    'Dispatcher.Invoke(New SeekToSpan_delegate(AddressOf SeekToSpan), Threading.DispatcherPriority.ContextIdle, New Object() {seek_Span})

                    'Console.WriteLine("seeking position")
                    System.Windows.Forms.Application.DoEvents()

                    'Cancel waiting
                    If (bw_handle.CancellationPending) Then
                        mClock.Controller.Stop()
                        GC.Collect()
                        Return
                    End If


                    seek_counter += 1
                    If (seek_counter > 10000) Then
                        seek_counter = 0
                        Exit While
                    End If
                End While

                'wait for the view to load before pausing
                For wait_counter As Integer = 1 To UI_WAIT_COUNT
                    System.Threading.Thread.Sleep(100)
                    System.Windows.Forms.Application.DoEvents()
                Next

                'Dispatcher.Invoke(New Pause_delegate(AddressOf Pause), Threading.DispatcherPriority.ContextIdle, New Object() {})

                'Pause after the media clock is in position for the screenshot
                mClock.Controller.Pause()

                'update UI queue to take effect
                System.Windows.Forms.Application.DoEvents()

                'UI drawing lags behind even though the clock position is set
                'the screenshot is taken directly from the UI view so it is important to wait for it
                For wait_counter As Integer = 1 To UI_WAIT_COUNT
                    System.Threading.Thread.Sleep(100)
                    System.Windows.Forms.Application.DoEvents()
                Next

                Console.WriteLine("Screenshot " + i.ToString + " at " + mClock.CurrentTime.Value.TotalSeconds.ToString + " seconds")
                Dim retry_counter As Integer = 0
                While (Not screenshot(_VideoDrawingPlayer, dirPath + "screenshot_" + i.ToString + ".jpg", retry_counter > 5) And retry_counter < 15)
                    retry_counter += 1
                    Console.WriteLine("same screenshot checksum. Resume, wait and try again.")

                    'increment seeking position
                    seek_Span += New TimeSpan(0, 0, small_interval)

                    'seek and wait
                    Dim seek_and_wait_counter As Integer = 0
                    While (mClock.CurrentTime < seek_Span)
                        'don't loop if already at the end of the video
                        If (seek_Span > endBoundTimeSpan) Then Exit While
                        mClock.Controller.Seek(seek_Span, Animation.TimeSeekOrigin.BeginTime)
                        System.Windows.Forms.Application.DoEvents()

                        'Cancel waiting
                        If (bw_handle.CancellationPending) Then
                            mClock.Controller.Stop()
                            GC.Collect()
                            Return
                        End If

                        seek_and_wait_counter += 1
                        If (seek_and_wait_counter > 10000) Then
                            seek_and_wait_counter = 0
                            Exit While
                        End If
                    End While

                    mClock.Controller.Resume()
                    System.Windows.Forms.Application.DoEvents()

                    'wait for the view to load before pausing
                    For wait_counter As Integer = 1 To UI_WAIT_COUNT
                        System.Threading.Thread.Sleep(100)
                        System.Windows.Forms.Application.DoEvents()
                    Next

                    mClock.Controller.Pause()
                    System.Windows.Forms.Application.DoEvents()

                    'wait for the view to load before pausing
                    For wait_counter As Integer = 1 To UI_WAIT_COUNT
                        System.Threading.Thread.Sleep(100)
                        System.Windows.Forms.Application.DoEvents()
                    Next
                End While

                If (retry_counter = 15) Then
                    failed_screenshot_counter += 1
                    If (failed_screenshot_counter > 2) Then
                        'all screenshot are failing so skip others
                        Console.WriteLine("Screenshots are failing. Skipping this video.")
                        'mark the folder
                        saveErrorScreen(dirPath + "no_preview" + ".jpg")

                        'generate version file
                        Using outputFile As New StreamWriter(dirPath + "preview_version.txt", False, System.Text.Encoding.UTF8)
                            outputFile.WriteLine("version:2")
                        End Using
                        Return
                    End If
                Else
                    failed_screenshot_counter = 0
                End If

            Next
        End If

        'generate version file
        Using outputFile As New StreamWriter(dirPath + "preview_version.txt", False, System.Text.Encoding.UTF8)
            outputFile.WriteLine("version:2")
        End Using

        mClock.Controller.Stop()

        GC.Collect()

    End Sub

    Private Delegate Sub SeekToSpan_delegate(<[ParamArray]()> ByVal seek_Span As TimeSpan)

    Private Sub SeekToSpan(ByVal seek_Span As TimeSpan)
        mClock.Controller.Seek(seek_Span, Animation.TimeSeekOrigin.BeginTime)
    End Sub

    Private Delegate Sub Pause_delegate()

    Private Sub Pause()
        mClock.Controller.Pause()
    End Sub

    Private Function screenshot(ByRef _VideoDrawingPLayer As MediaPlayer, ByVal filepath As String, ByVal passSimilar As Boolean) As Boolean
        Try
            'Render screenshot directly from the UI control
            Dim rtb As RenderTargetBitmap = New RenderTargetBitmap(output_width, output_height, 96, 96, PixelFormats.Default)
            Dim dv As DrawingVisual = New DrawingVisual()
            Dim dc As DrawingContext = dv.RenderOpen()
            dc.DrawVideo(_VideoDrawingPLayer, New System.Windows.Rect(0, 0, output_width, output_height))
            dc.Close()
            rtb.Render(dv)

            'use jpg format to compress the image
            Dim jpg As JpegBitmapEncoder = New JpegBitmapEncoder
            Dim bmp_enc As BitmapEncoder = New BmpBitmapEncoder()
            jpg.Frames.Add(BitmapFrame.Create(rtb))
            bmp_enc.Frames.Add(BitmapFrame.Create(rtb))

            Using str As Stream = File.Create(filepath)
                jpg.Save(str)
            End Using

            Dim bmp As Bitmap
            Using outstream As MemoryStream = New MemoryStream()
                bmp_enc.Save(outstream)
                bmp = New System.Drawing.Bitmap(outstream)
            End Using

            Dim thispreview_checksum As String = Checksum.getFingerprint(filepath)
            Dim thispreview_histogram As Net.Arr = BitmapToHistogram(bmp)
            Dim comparison As Single = EMD_Threshold
            If (lastpreview_histogram IsNot Nothing) Then
                comparison = CompareTwoImages(thispreview_histogram, lastpreview_histogram)
                Console.WriteLine("Comparison to previous img. = " + comparison.ToString)
            End If

            If (thispreview_checksum.Equals(lastpreview_checksum) Or thispreview_checksum.Equals(blackpreview_checksum) Or thispreview_checksum.Equals(blackpreview_checksum2) Or (comparison < EMD_Threshold And Not passSimilar)) Then
                'Same checksum means same image
                Return False
            Else
                lastpreview_checksum = thispreview_checksum
                lastpreview_histogram = thispreview_histogram
            End If

            Return True
        Catch ex As Exception
            'IO operations can fail for multiple reasons, e.g. directory access denied
            Console.WriteLine(ex.ToString)
            Return False
        End Try
    End Function

    Private Function saveErrorScreen(ByVal filepath As String) As Boolean
        Try
            'write the file
            Dim img As Image = My.Resources.no_preview
            img.Save(filepath)
            Return True
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
        Return False
    End Function

    Public Sub unload()
        'Dispose objects
        Me.Background = Nothing
        mTimeline = Nothing
        mClock = Nothing
        GC.Collect()
    End Sub

    Protected Overrides Sub OnContentChanged(oldContent As Object, newContent As Object)
        MyBase.OnContentChanged(oldContent, newContent)
    End Sub

    Private Function CompareTwoImages(ByRef histogram1 As Net.Arr, ByRef histogram2 As Net.Arr) As Single
        Try
            Return OpenCV.Net.CV.CalcEMD2(histogram1, histogram2, Net.DistanceType.L2, Nothing, Nothing, Nothing)
        Catch ex As Exception
            Return EMD_Threshold
        End Try
    End Function

    Private Function BitmapToHistogram(ByRef bmp As Bitmap) As Net.Arr

        Dim rect As New Rectangle(0, 0, bmp.Width, bmp.Height)
        Dim bmpData As System.Drawing.Imaging.BitmapData = bmp.LockBits(rect,
            System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat)

        ' Get the address of the first line. 
        Dim ptr As IntPtr = bmpData.Scan0

        ' Declare an array to hold the bytes of the bitmap. 
        ' This code is specific to a bitmap with 24 bits per pixels. 
        Dim bytes As Integer = Math.Abs(bmpData.Stride) * bmp.Height
        Dim rgbValues(bytes - 1) As Byte

        ' Copy the RGB values into the array.
        System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes)
        Dim paddingbytes As Integer = bmpData.Stride - (bmp.Width * 3)

        '8 bins per channel

        Dim mat = New Net.Mat(New Net.Size(2, 8), Net.Depth.F32, 1)
        Dim histogram As Net.Arr = mat
        histogram.Set(New Net.Scalar(0))

        Dim R, G, B As Integer
        Dim Rbins(7) As Integer
        Dim Gbins(7) As Integer
        Dim Bbins(7) As Integer
        For x As Integer = 0 To bmp.Width - 1
            For y As Integer = 0 To bmp.Height - 1
                R = rgbValues((bmp.Width * (y * 3) + (y * paddingbytes) + (x * 3)))
                G = rgbValues((bmp.Width * (y * 3) + (y * paddingbytes) + (x * 3)) + 1)
                B = rgbValues((bmp.Width * (y * 3) + (y * paddingbytes) + (x * 3)) + 2)
                Rbins(R \ 32) += 1
                Gbins(G \ 32) += 1
                Bbins(B \ 32) += 1
            Next
        Next

        Dim totalPixels As Integer = bmp.Width * bmp.Height

        For i As Integer = 0 To 7
            Dim intR As Single = (256 * (Rbins(i) / totalPixels))
            Dim intG As Single = (256 * (Gbins(i) / totalPixels))
            Dim intB As Single = (256 * (Bbins(i) / totalPixels))
            Dim avg As Single = (intR + intG + intB) / 3.0 'Math.Round((intR + intG + intB) / 3.0)

            histogram.SetReal(i, avg)
            'histogram(i) = New Net.Scalar(avg)
        Next

        ' Copy the RGB values back to the bitmap
        'System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes)

        ' Unlock the bits.
        bmp.UnlockBits(bmpData)

        Return histogram
    End Function

End Class

