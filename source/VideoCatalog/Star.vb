﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.Drawing.Drawing2D

Public Class Star

    Private vid As VideoFile = Nothing       'reference to videoFile

    Private rating As Double = -1   '-1 is not rated, range = [0,5]
    Private shadow_rating As Double = -1 'mouse is hovering over the selector
    Private isHovering As Boolean = False   'mouse hover over

    Private star_fill_color As Color = Color.FromArgb(239, 239, 24) ' Color.FromArgb(241, 221, 17)
    Private star_empty_color As Color = Color.FromArgb(100, 100, 105) ' Color.FromArgb(240, 240, 240)
    Private empty_color As Color = Color.FromArgb(80, 80, 85) 'SystemColors.ControlLightLight 'transparent

    Private star_fill_brush As Brush = New SolidBrush(star_fill_color)
    Private star_empty_brush As Brush = New SolidBrush(star_empty_color)
    Private empty_brush As Brush = New SolidBrush(empty_color)
    Private boundaryPen As Pen = New Pen(New SolidBrush(Color.FromArgb(20, 20, 20)), 1.0F)   ' New Pen(New SolidBrush(Color.FromArgb(80, 80, 80)), 1.0F)
    Private boundaryPenEmpty As Pen = New Pen(New SolidBrush(Color.FromArgb(40, 40, 40)), 1.0F) 'New Pen(New SolidBrush(Color.FromArgb(220, 220, 220)), 1.0F)
    Private boundaryPenStar As Pen = New Pen(New SolidBrush(Color.FromArgb(220, 220, 10)), 1.0F) ' New Pen(New SolidBrush(Color.FromArgb(220, 220, 10)), 1.0F)

    Private isHidden As Boolean = False


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub Star_Load(sender As Object, e As EventArgs) Handles Me.Load
        'draw first rating
    End Sub

    Public Sub setRating(ByRef _vid As VideoFile)
        If (_vid IsNot Nothing) Then
            vid = _vid
            rating = vid.getRating / 10.0
            isHidden = False
            StartPic.Enabled = True
        Else
            isHidden = True
            rating = -1
            StartPic.Enabled = False
        End If

        StartPic.Invalidate()
    End Sub

    Private Sub StartPic_Paint(sender As Object, e As PaintEventArgs) Handles StartPic.Paint
        If (Not isHidden) Then
            Dim g As Graphics = e.Graphics
            g.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

            Dim _width As Single = StartPic.Width
            Dim _height As Single = StartPic.Height

            'one star width
            Dim star_width As Single = _width / 5.0F

            'draw 5 stars
            For i As Integer = 0 To 4
                Dim fill_percentage As Single = rating - i
                If (isHovering) Then
                    fill_percentage = shadow_rating - i
                End If
                fill_percentage = Math.Min(1, fill_percentage)
                fill_percentage = Math.Max(0, fill_percentage)
                drawStar(g, i * star_width, 0, star_width, fill_percentage)
            Next
        End If
    End Sub

    Private Sub drawStar(ByRef g As Graphics, ByVal x As Single, ByVal y As Single, ByVal width As Single, ByVal fill_percentage As Double)
        'draws on width x width area starting from x,y

        'get required positions
        Dim radians As Single = 1.2566  '72 degrees
        Dim offset_radians As Single = 1.57 '90 degrees
        Dim offset_radians2 As Single = 2.199 '90 + 36 degrees
        Dim offset_radians3 As Single = 3.14 '180 degrees
        Dim star_long As Single = (6.0F / 16.0F) * width
        Dim star_short As Single = (3.0F / 16.0F) * width
        Dim boundary_width As Single = (2.0 / 16.0) * width
        Dim star_center As PointF = New PointF(x + (width / 2.0F), y + (width / 2.0F))
        Dim star_points(4) As PointF
        Dim star_points_inner(4) As PointF

        Dim leftTop As PointF = New PointF(x, y)
        Dim leftBottom As PointF = New PointF(x, y + width)
        Dim rightTop As PointF = New PointF(x + width, y)
        Dim rightBottom As PointF = New PointF(x + width, y + width)
        Dim topCenter As PointF = New PointF(x + width / 2.0F, y)
        Dim bottomCenter As PointF = New PointF(x + width / 2.0F, y + width)

        'anti-clocwise
        Dim _x1, _y1, _x2, _y2 As Single
        For i As Integer = 0 To star_points.Length - 1
            _x1 = star_center.X + star_long * Math.Cos(offset_radians + i * radians)
            _y1 = star_center.Y + star_long * Math.Sin(offset_radians + offset_radians3 + i * radians)
            star_points(i) = New PointF(_x1, _y1)
            _x2 = star_center.X + star_short * Math.Cos(offset_radians2 + i * radians)
            _y2 = star_center.Y + star_short * Math.Sin(offset_radians2 + offset_radians3 + i * radians)
            star_points_inner(i) = New PointF(_x2, _y2)
        Next

        'fill with background
        g.FillRectangle(star_empty_brush, New Rectangle(x + boundary_width, y + boundary_width, width - 2 * boundary_width, width - 2 * boundary_width))

        'fill with selection percentage
        g.FillRectangle(star_fill_brush, New Rectangle(x + boundary_width, y + boundary_width, fill_percentage * (width - 2 * boundary_width), width - 2 * boundary_width))
        'draw sharp boundary on the star
        'g.DrawLine(boundaryPenStar, New PointF(x + boundary_width + fill_percentage * (width - 2 * boundary_width), y + boundary_width), New PointF(x + boundary_width + fill_percentage * (width - 2 * boundary_width), width - 2 * boundary_width))

        'Draw line to cover up some graphics
        g.DrawLine(New Pen(New SolidBrush(empty_color), 1), star_points_inner(2), bottomCenter)

        'draw star outline path
        'left outline
        Dim star_left_outside As GraphicsPath = New GraphicsPath()
        star_left_outside.AddLine(leftTop, topCenter)
        star_left_outside.AddLine(topCenter, star_points(0))
        star_left_outside.AddLine(star_points(0), star_points_inner(0))
        star_left_outside.AddLine(star_points_inner(0), star_points(1))
        star_left_outside.AddLine(star_points(1), star_points_inner(1))
        star_left_outside.AddLine(star_points_inner(1), star_points(2))
        star_left_outside.AddLine(star_points(2), star_points_inner(2))
        star_left_outside.AddLine(star_points_inner(2), bottomCenter)
        star_left_outside.AddLine(bottomCenter, leftBottom)
        star_left_outside.AddLine(leftBottom, leftTop)
        g.FillPath(empty_brush, star_left_outside)

        'right outline
        Dim star_right_outside As GraphicsPath = New GraphicsPath()
        star_right_outside.AddLine(rightTop, topCenter)
        star_right_outside.AddLine(topCenter, star_points(0))
        star_right_outside.AddLine(star_points(0), star_points_inner(4))
        star_right_outside.AddLine(star_points_inner(4), star_points(4))
        star_right_outside.AddLine(star_points(4), star_points_inner(3))
        star_right_outside.AddLine(star_points_inner(3), star_points(3))
        star_right_outside.AddLine(star_points(3), star_points_inner(2))
        star_right_outside.AddLine(star_points_inner(2), bottomCenter)
        star_right_outside.AddLine(bottomCenter, rightBottom)
        star_right_outside.AddLine(rightBottom, rightTop)
        g.FillPath(empty_brush, star_right_outside)

        'boundary
        Dim star_boundary_left As GraphicsPath = New GraphicsPath()
        star_boundary_left.AddLine(star_points(0), star_points_inner(0))
        star_boundary_left.AddLine(star_points_inner(0), star_points(1))
        star_boundary_left.AddLine(star_points(1), star_points_inner(1))
        star_boundary_left.AddLine(star_points_inner(1), star_points(2))
        star_boundary_left.AddLine(star_points(2), star_points_inner(2))
        Dim star_boundary_right As GraphicsPath = New GraphicsPath()
        star_boundary_right.AddLine(star_points_inner(2), star_points(3))
        star_boundary_right.AddLine(star_points(3), star_points_inner(3))
        star_boundary_right.AddLine(star_points_inner(3), star_points(4))
        star_boundary_right.AddLine(star_points(4), star_points_inner(4))
        star_boundary_right.AddLine(star_points_inner(4), star_points(0))
        If (fill_percentage = 0) Then
            g.DrawPath(boundaryPenEmpty, star_boundary_left)
            g.DrawPath(boundaryPenEmpty, star_boundary_right)
        ElseIf (fill_percentage <= 0.5F) Then
            star_boundary_left.AddLine(star_points_inner(2), star_points(0))
            g.DrawPath(boundaryPen, star_boundary_left)
            g.DrawPath(boundaryPenEmpty, star_boundary_right)
        Else
            g.DrawPath(boundaryPen, star_boundary_left)
            g.DrawPath(boundaryPen, star_boundary_right)
        End If




    End Sub

    Private Sub StartPic_MouseEnter(sender As Object, e As EventArgs) Handles StartPic.MouseEnter
        If Not (isHidden) Then isHovering = True
    End Sub

    Private Sub StartPic_MouseMove(sender As Object, e As MouseEventArgs) Handles StartPic.MouseMove
        If (isHovering) Then
            'get width position
            shadow_rating = Math.Round((e.X / StartPic.Width) * 50.0F) / 10.0F
            Dim decimals As Single = shadow_rating - Math.Floor(shadow_rating)
            If (decimals > 0.5F) Then
                decimals = 1.0F
            ElseIf (decimals > 0) Then
                decimals = 0.5F
            End If
            shadow_rating = Math.Floor(shadow_rating) + decimals
            StartPic.Invalidate()
        End If
    End Sub

    Private Sub StartPic_MouseLeave(sender As Object, e As EventArgs) Handles StartPic.MouseLeave
        isHovering = False
        StartPic.Invalidate()
    End Sub

    Private Sub StartPic_MouseClick(sender As Object, e As MouseEventArgs) Handles StartPic.MouseClick
        If (e.Button = MouseButtons.Left) Then
            'apply
            rating = shadow_rating
            isHovering = False
            If (vid IsNot Nothing) Then
                vid.setRating(Math.Round(rating * 10))
            End If
            StartPic.Invalidate()
        End If

    End Sub

    Private Sub StarRatingMenuItemClear_Click(sender As Object, e As EventArgs) Handles StarRatingMenuItemClear.Click
        rating = -1
        isHovering = False
        If (vid IsNot Nothing) Then
            vid.setRating(-1)
        End If
        StartPic.Invalidate()
    End Sub

End Class
