﻿'Juniper Video Library. Organize search and play your local videos.
'Copyright(C) 2018  Joni Juvonen

'This code is under MIT licence, https://opensource.org/licenses/MIT

Imports System.IO
Imports System.ComponentModel
Imports TensorFlow

Public Class TF_Inception

    'This is used to stop bg process from the parent form
    Public intercept As Boolean = False

    Private _ModelFolder As String
    Private _PreviewFolder As String

    Private threshold_for_classifier As Single = 0.3       '30 percent limit for a valid classification

    Private _TFSession As TFSession
    Private _TFGraph As TFGraph
    Private _TFLabels As String()

    Private modelVersionFile As String = "model_version.txt"

    Public Sub AITagVideos(ByRef vids As List(Of VideoFile), ByVal modelFolder As String, ByVal previewFolder As String, ByRef activeList As List(Of Integer), ByRef bw_AI As BackgroundWorker)
        _ModelFolder = modelFolder
        _PreviewFolder = previewFolder

        'Start by loading the model
        If Not loadModel() Then
            'Loading failed
            Console.WriteLine("Could not load model")
            Return
        End If

        'load haar face detector
        Dim _FaceDetector = New FaceDetection(modelFolder)

        'Check the version of the AI tags and regenerate if there is a better version
        Dim regenerate As Boolean = False
        If (Not File.Exists(modelFolder + modelVersionFile)) Then
            regenerate = True
        Else
            If (File.ReadAllLines(modelFolder + modelVersionFile)(0) <> "version:2") Then
                regenerate = True
            End If
        End If

        If (regenerate) Then
            're-generate AI tags
            'clear previous tags
            For Each index In activeList
                vids(index).setAITag({})
            Next
            'generate version file
            Using outputFile As New StreamWriter(modelFolder + modelVersionFile, False, System.Text.Encoding.UTF8)
                outputFile.WriteLine("version:2")
                outputFile.WriteLine("model:mobilenet_v2_1.0_224")
            End Using
        End If

        Console.WriteLine("generating AI tags")

        For Each index In activeList
            'exit if intercepted
            If (intercept) Then Exit For
            If bw_AI.CancellationPending Then Exit For


            'skip if AI-tags have already been generated
            If vids(index).getAITags.Count > 0 Then
                If (vids(index).getPreviewIndex = -1) Then
                    'generate tags again and at the same time, set the most relevant picture as the default pic
                    vids(index).setAITag({})
                Else
                    'uncomment this to remake tags
                    'vids(index).setAITag({})
                    Continue For
                End If
            End If

            'get preview directory
            Dim prevDir As String = previewFolder + vids(index).getId

            'Check that the preview directory exists
            If (Directory.Exists(prevDir)) Then

                'get all screenshots
                Dim previewFiles As String() = IO.Directory.GetFiles(prevDir, "*.jpg*", SearchOption.TopDirectoryOnly)

                'holder for tags
                Dim allTags As New List(Of String)

                Dim mostRelevantPic As Integer = 0
                Dim highestProb As Single = 0
                Dim maxFaceCount As Single = 0
                Dim previewIndex = 0

                Dim allNewTags As New List(Of label_prob)

                For Each previewFile In previewFiles
                    'do not process no_preview.jpg
                    If previewFile.Contains("no_preview") Then Continue For

                    Dim newTags As List(Of label_prob) = Recognize(previewFile, modelFolder)

                    Dim faceCount As Single = _FaceDetector.DetectFaceValue(previewFile)
                    Console.WriteLine("FaceCount at " + previewFile + " : " + faceCount.ToString)

                    For Each newTag In newTags

                        'Select thumbnail pic. Logic:  Value(FaceCount) >> Value(Detected object probability)
                        If (faceCount > maxFaceCount) Then
                            maxFaceCount = faceCount
                            highestProb = newTag.prob
                            mostRelevantPic = previewIndex
                        ElseIf (faceCount = maxFaceCount) Then
                            If (newTag.prob > highestProb) Then
                                highestProb = newTag.prob
                                mostRelevantPic = previewIndex
                            End If
                        End If

                        'not an empty tag
                        If (newTag.label <> "") Then

                            'check that allNewTags does not contain the same label
                            Dim hasLabel As Boolean = False
                            If (allNewTags.Count > 0) Then
                                For i As Integer = 0 To allNewTags.Count - 1
                                    If (allNewTags(i).label = newTag.label) Then
                                        hasLabel = True

                                        'update probability to highest value
                                        If (allNewTags(i).prob < newTag.prob) Then
                                            hasLabel = False
                                            allNewTags.RemoveAt(i)
                                            Exit For
                                        End If
                                    End If
                                Next
                            End If

                            'add new one
                            If Not hasLabel Then
                                allNewTags.Add(newTag)
                            End If
                        End If

                    Next

                    previewIndex += 1
                Next

                'Filter out tags that have low probability
                'set probability threshold based on highest found prob.
                Dim probTh As Single = Math.Min(threshold_for_classifier, highestProb)

                For Each _tag In allNewTags
                    If Not (_tag.prob < probTh) Then
                        allTags.Add(_tag.label)
                    End If
                Next

                'add tags to the list
                allTags.Add("*")    'this is an indicator that AI tags have been generated
                vids(index).setAITag(allTags.ToArray())

                vids(index).setPreviewIndex(mostRelevantPic)

                GC.Collect()

            End If

        Next

        'unload model
        unloadModel()

        Console.WriteLine("finished generating AI tags")

    End Sub

    Private Function loadModel() As Boolean
        Try
            'load model from resources
            _TFGraph = New TFGraph()
            Dim model As Byte() = My.Resources.mobilenet_v2_1_0_224_frozen
            _TFLabels = My.Resources.imagenet_slim_labels.Split(New String() {Environment.NewLine},
                                       StringSplitOptions.RemoveEmptyEntries)
            _TFGraph.Import(model)
            _TFSession = New TFSession(_TFGraph)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub unloadModel()
        Try
            _TFSession.CloseSession()
            _TFSession.DeleteSession()
            _TFSession = Nothing
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    Private Function Recognize(ByVal fileName As String, ByVal modelFolder As String) As List(Of label_prob)

        Dim returnList As New List(Of label_prob)
        Try

            Dim tensor = CreateTensorFromImageFile(fileName)
            Dim runner = _TFSession.GetRunner()

            runner.AddInput(_TFGraph("input")(0), tensor)
            runner.Fetch(_TFGraph("MobilenetV2/Predictions/Reshape_1")(0))

            Dim output = runner.Run()
            Dim result = output(0)
            Dim probabilities = TryCast(result.GetValue(True), Single()())(0)

            tensor.Dispose()

            If (probabilities.Length > 0) Then
                Dim maxVal As Single = 0
                Dim maxIdx As Integer = 0
                For i As Integer = 0 To probabilities.Length - 1
                    If probabilities(i) > maxVal Then
                        maxVal = probabilities(i)
                        maxIdx = i
                    End If
                Next
                'return label with the highest prob
                returnList.Add(New label_prob(_TFLabels(maxIdx), maxVal))

                Console.WriteLine((_TFLabels(maxIdx) + " with prob. " + (maxVal * 100.0).ToString + " %"))
                Return returnList
            End If

        Catch ex As Exception
            Console.WriteLine("Could not classify an image, possibly in use")
        End Try
        Return returnList
    End Function

    Public Shared Function CreateTensorFromImageFile(ByVal path As String, ByVal Optional destinationDataType As TFDataType = TFDataType.Float) As TFTensor
        Dim contents = SafeFileFromPath(path) 'File.ReadAllBytes(path)
        Dim tensor = TFTensor.CreateString(contents)
        Dim input, output As TFOutput

        Using graph = ConstructGraphToNormalizeImage(input, output, destinationDataType)

            Using session = New TFSession(graph)
                Dim normalized = session.Run(inputs:={input}, inputValues:={tensor}, outputs:={output})
                Return normalized(0)
            End Using
        End Using
    End Function

    Private Shared Function SafeFileFromPath(path As String) As Byte()
        Using fs As New FileStream(path, FileMode.Open, FileAccess.Read)
            ' Read the source file into a byte array.
            Dim bytes() As Byte = New Byte((fs.Length) - 1) {}
            Dim numBytesToRead As Integer = CType(fs.Length, Integer)
            Dim numBytesRead As Integer = 0

            While (numBytesToRead > 0)
                ' Read may return anything from 0 to numBytesToRead.
                Dim n As Integer = fs.Read(bytes, numBytesRead,
                        numBytesToRead)
                ' Break when the end of the file is reached.
                If (n = 0) Then
                    Exit While
                End If
                numBytesRead = (numBytesRead + n)
                numBytesToRead = (numBytesToRead - n)

            End While
            Return bytes
        End Using
    End Function

    Private Shared Function ConstructGraphToNormalizeImage(ByRef input As TFOutput, ByRef output As TFOutput, ByVal Optional destinationDataType As TFDataType = TFDataType.Float) As TFGraph
        Const W As Integer = 224
        Const H As Integer = 224
        Const Mean As Single = 128
        Const Scale As Single = 128 '1 for inception and 128 for mobilenet
        Dim graph = New TFGraph()
        input = graph.Placeholder(TFDataType.String)
        output = graph.Cast(graph.Div(x:=graph.[Sub](x:=graph.ResizeBilinear(images:=graph.ExpandDims(input:=graph.Cast(graph.DecodeJpeg(contents:=input, channels:=3), DstT:=TFDataType.Float), [dim]:=graph.[Const](0, "make_batch")), size:=graph.[Const](New Integer() {W, H}, "size")), y:=graph.[Const](Mean, "mean")), y:=graph.[Const](Scale, "scale")), destinationDataType)
        Return graph
    End Function

    Private Structure label_prob
        Public label As String
        Public prob As Single

        Public Sub New(_l As String, _p As Single)
            label = _l
            prob = _p
        End Sub

        Public Sub UpdateProb(_p As Single)
            prob = _p
        End Sub

    End Structure

End Class
