﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VideoPreviewControl
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Name_lbl = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.PreviewVideo = New System.Windows.Forms.PictureBox()
        Me.VideoContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PlayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewInFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tagMenuTimer = New System.Windows.Forms.Timer(Me.components)
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.PreviewVideo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.VideoContextMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'Name_lbl
        '
        Me.Name_lbl.AutoSize = True
        Me.Name_lbl.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name_lbl.ForeColor = System.Drawing.Color.Gainsboro
        Me.Name_lbl.Location = New System.Drawing.Point(0, 132)
        Me.Name_lbl.Margin = New System.Windows.Forms.Padding(0, 0, 4, 0)
        Me.Name_lbl.MaximumSize = New System.Drawing.Size(200, 40)
        Me.Name_lbl.MinimumSize = New System.Drawing.Size(0, 40)
        Me.Name_lbl.Name = "Name_lbl"
        Me.Name_lbl.Size = New System.Drawing.Size(51, 40)
        Me.Name_lbl.TabIndex = 1
        Me.Name_lbl.Text = "Name"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.FlowLayoutPanel1.Controls.Add(Me.PreviewVideo)
        Me.FlowLayoutPanel1.Controls.Add(Me.Name_lbl)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(220, 172)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'PreviewVideo
        '
        Me.PreviewVideo.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.PreviewVideo.ContextMenuStrip = Me.VideoContextMenu
        Me.PreviewVideo.Location = New System.Drawing.Point(3, 3)
        Me.PreviewVideo.Name = "PreviewVideo"
        Me.PreviewVideo.Size = New System.Drawing.Size(214, 126)
        Me.PreviewVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PreviewVideo.TabIndex = 4
        Me.PreviewVideo.TabStop = False
        '
        'VideoContextMenu
        '
        Me.VideoContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PlayToolStripMenuItem, Me.ViewInFolderToolStripMenuItem})
        Me.VideoContextMenu.Name = "VideoContextMenu"
        Me.VideoContextMenu.Size = New System.Drawing.Size(167, 48)
        '
        'PlayToolStripMenuItem
        '
        Me.PlayToolStripMenuItem.Name = "PlayToolStripMenuItem"
        Me.PlayToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.PlayToolStripMenuItem.Text = "Play"
        '
        'ViewInFolderToolStripMenuItem
        '
        Me.ViewInFolderToolStripMenuItem.Name = "ViewInFolderToolStripMenuItem"
        Me.ViewInFolderToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ViewInFolderToolStripMenuItem.Text = "Show in directory"
        '
        'Timer1
        '
        Me.Timer1.Interval = 700
        '
        'tagMenuTimer
        '
        Me.tagMenuTimer.Interval = 200
        '
        'VideoPreviewControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "VideoPreviewControl"
        Me.Size = New System.Drawing.Size(226, 178)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.PreviewVideo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.VideoContextMenu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Name_lbl As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents PreviewVideo As PictureBox
    Friend WithEvents Timer1 As Timer
    Friend WithEvents VideoContextMenu As ContextMenuStrip
    Friend WithEvents PlayToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewInFolderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents tagMenuTimer As Timer
End Class
