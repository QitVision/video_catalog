## Compiling

In order to compile Juniper Video Library you need a version of Visual Studio 2015 or later. Use x64 framework (included OpenCV binaries are built with x64)!

## Reporting bugs

If you are sure you've found a bug. 
Please check latest entries in Juniper Video Library's error log which you find at `%APPDATA%\VideoCatalog\ErrorLog.txt` and file a new issue.