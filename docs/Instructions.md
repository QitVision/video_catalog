## Installation

Juniper Video Library is distributed as a zip package that you can download [here](../latest_build/juniper.zip).

1. Unzip **juniper.zip** to a folder. For example: `C:\Juniper`
2. The unzipped folder has the **Juniper Video Library.exe** binary. Run this *.exe to open Juniper Video Library
3. (Optional) Create a shortcut of the **Juniper Video Library.exe** and place it on your desktop.

Note that the executable has to be kept in the same folder with the other *.dll dependencies of the zip folder.

### Requirements

- Windows 7/8/10
- .NET framework 4.6.1 or higher

## Selecting video folders

When opened for the first time, you are prompted to select all your local video folders that you want to include in the library.
The locations can be edited later from the **Settings** tab page part **Select folders**.

![Select folders](/docs/select_folders.png)

The video library needs to be refreshed after adding or deleting folders or videos but Juniper will notice this automatically.

Note that the folders are searched for videos recursively so all the subfolders are included.

## Customizing library browsing

Options such as preview size, number of videos per page and preview change speed can be customized from the **Settings** tab page. Remember to press **Apply** after changes for them to take effect.

![Customize video browsing](/docs/general_settings.PNG)

## Search

Videos can be searched from the top search bar by their folder path and name by default but you can also add custom search tags to videos from the search icon in the video preview's bottom right corner.

![Tag menu](/docs/tag_menu.png)

Press the **Advanced search** to see additional filtering and sorting options.

![Advanced search](/docs/advanced_search.png)

## Playing videos

Double click a video to play or right click and select *Play*.

## Supported video formats

Previews are created from all Windows Media Player natively supported formats:

.wma, .wmv, .asf, .wm, .dvr-ms, .avi, .mpg, .mpeg, .m1v, .mp2, .mpe, .mov, .mp4, .m4v, .m2ts, .flv

Note that .mkv files are indexed but the previews might not work for them.
To get previews from .mkv files, make sure you have the codecs installed. For example, [K-Lite](https://www.codecguide.com/about_kl.htm) codec pack comes with matroska codecs.

