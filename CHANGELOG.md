## Version 0.5.0
#### 30th June 2018
- improved preview screenshot generation, prefer higher variance in a batch of screenshots
- improved thumbnail selection logic: hasFaces > hasUpperBodies > hasAiRecognizedObjects
- increased preview screenshot count from 8 to 14
- icons and colors to UI buttons


## Version 0.4.0
#### 6th June 2018
- Lighter TensorFlow model mobilenetV2 for AI video tagging
- Improved logic for AI tagging
- Changed TensorFlow library from Emgu.TF to TensorFlowSharp
- Re-released with more permissive MIT license
- .NET framework to 4.6.1

## Version 0.3.1
#### 30th March 2018
- Bug fixes, handled AI image classification exceptions

## Version 0.3.0
#### 23rd March 2018
- AI selects the most descriptive thumbnail
- .flv files are indexed

## Version 0.2.0
#### 22th February 2018
- dark UI theme
- AI generated search tags
- show 'screenshot missing' image for videos without screenshots
- autorefresh library on every open

## Version 0.1.2
#### 30th January 2018
- search filtering by rating
- sorting by rating
- random sorting

## Version 0.1.1
#### 25th January 2018
- First beta release