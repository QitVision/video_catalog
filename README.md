# Juniper Video Library
Juniper Video Library lets you browse and search your local videos easily and it tags them automatically using a trained deep convolutional neural network.

All video content processing, including AI video tagging, is done offline in Juniper Video Library without any 3rd party services.

![Juniper screenshot](/docs/juniper_main.gif)

## Shortcuts

#### [Latest build](/latest_build/)
To run this software, download the latest 
[build](/latest_build/juniper.zip)
.

#### [Installation and instructions](/docs/Instructions.md)
How to install and use.

#### [Repository info](/docs/Repository.md)
How to compile or report bugs.

#### [Source](/source/)
Get the VB.NET source.

#### [Old binaries](/old_builds/)
Older versions

#### [License](/LICENSE.md)
MIT LICENSE
Copyright (c) 2018 Joni Juvonen

#### [CHANGELOG](/CHANGELOG.md)
Version history

***
